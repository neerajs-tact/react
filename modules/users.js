import { createSelector } from 'reselect'
import { CALL_API, HTTP_METHODS } from '../lib/api-middleware'
import { signOut as authSignOut } from '../lib/auth'

// API Actions
const ACCEPT_INVITATION_REQUEST = 'users/ACCEPT_INVITATION_REQUEST'
const ACCEPT_INVITATION_SUCCESS = 'users/ACCEPT_INVITATION_SUCCESS'
const ACCEPT_INVITATION_FAILURE = 'users/ACCEPT_INVITATION_FAILURE'

const SIGN_UP_REQUEST = 'users/SIGN_UP_REQUEST'
const SIGN_UP_SUCCESS = 'users/SIGN_UP_SUCCESS'
const SIGN_UP_FAILURE = 'users/SIGN_UP_FAILURE'

const SIGN_IN_SUCCESS = 'users/SIGN_IN_SUCCESS'

const SIGN_OUT_REQUEST = 'users/SIGN_OUT_REQUEST'
const SIGN_OUT_SUCCESS = 'users/SIGN_OUT_SUCCESS'
const SIGN_OUT_FAILURE = 'users/SIGN_OUT_FAILURE'

const RETRIEVE_USER_REQUEST = 'users/RETRIEVE_USER_REQUEST'
const RETRIEVE_USER_SUCCESS = 'users/RETRIEVE_USER_SUCCESS'
const RETRIEVE_USER_FAILURE = 'users/RETRIEVE_USER_FAILURE'

const UPDATE_USER_REQUEST = 'users/UPDATE_USER_REQUEST'
const UPDATE_USER_SUCCESS = 'users/UPDATE_USER_SUCCESS'
const UPDATE_USER_FAILURE = 'users/UPDATE_USER_FAILURE'

const CHANGE_PASSWORD_REQUEST = 'users/CHANGE_PASSWORD_REQUEST'
const CHANGE_PASSWORD_SUCCESS = 'users/CHANGE_PASSWORD_SUCCESS'
const CHANGE_PASSWORD_FAILURE = 'users/CHANGE_PASSWORD_FAILURE'

const RESET_PASSWORD_REQUEST = 'users/RESET_PASSWORD_REQUEST'
const RESET_PASSWORD_SUCCESS = 'users/RESET_PASSWORD_SUCCESS'
const RESET_PASSWORD_FAILURE = 'users/RESET_PASSWORD_FAILURE'

const RESET_ALL_STATE = 'RESET_ALL_STATE'

export { RESET_ALL_STATE }

// Initial State
const initialState = {
  currentUser: {},
}

// Reducer
export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_UP_SUCCESS:
    case SIGN_IN_SUCCESS:
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        currentUser: action.payload.user,
      }
    case RETRIEVE_USER_SUCCESS:
      return {
        ...state,
        currentUser: action.payload,
      }
    default:
      return state
  }
}

// Selectors

/*
  A hook, useAuthUser, can be used instead but note it is only updated during
  sign in and sign out. If you are working with data that might change between
  those moments then use this module.
 */

export const selectCurrentUser = state => state.users.currentUser

export const selectCurrentUserEmail = createSelector(
  selectCurrentUser,
  user => user.email
)

export const selectCurrentUserFirstName = createSelector(
  selectCurrentUser,
  user => user.firstName
)

export const selectCurrentUserLastName = createSelector(
  selectCurrentUser,
  user => user.lastName
)

export const selectCurrentUserPhoneNumber = createSelector(
  selectCurrentUser,
  user => user.phoneNumber
)

// Action Creators

export const acceptInvitation = user => dispatch => {
  const acceptInvitationAction = {
    [CALL_API]: {
      types: [
        ACCEPT_INVITATION_REQUEST,
        ACCEPT_INVITATION_SUCCESS,
        ACCEPT_INVITATION_FAILURE,
      ],
      endpoint: 'users/invitation',
      method: HTTP_METHODS.PUT,
      body: { user },
    },
  }

  return dispatch(acceptInvitationAction)
}

export const signUp = (user, token) => dispatch => {
  const signUpAction = {
    [CALL_API]: {
      types: [SIGN_UP_REQUEST, SIGN_UP_SUCCESS, SIGN_UP_FAILURE],
      endpoint: 'users',
      method: HTTP_METHODS.POST,
      body: { user, token },
    },
  }

  return dispatch(signUpAction)
}

export const signIn = user => dispatch =>
  dispatch({
    type: SIGN_IN_SUCCESS,
    payload: user,
  })

export const signOut = () => async dispatch => {
  const signOutAction = {
    [CALL_API]: {
      types: [SIGN_OUT_REQUEST, SIGN_OUT_SUCCESS, SIGN_OUT_FAILURE],
      endpoint: 'users/sign_out',
      method: HTTP_METHODS.DELETE,
    },
  }

  await dispatch(signOutAction)
  await authSignOut()
  dispatch({ type: RESET_ALL_STATE })
}

export const retrieveCurrentUser = () => ({
  [CALL_API]: {
    types: [
      RETRIEVE_USER_REQUEST,
      RETRIEVE_USER_SUCCESS,
      RETRIEVE_USER_FAILURE,
    ],
    endpoint: 'users/current_user',
  },
})

export const updateCurrentUser = user => async dispatch => {
  const updateAction = {
    [CALL_API]: {
      types: [UPDATE_USER_REQUEST, UPDATE_USER_SUCCESS, UPDATE_USER_FAILURE],
      endpoint: `users/my`,
      method: HTTP_METHODS.PUT,
      body: { user },
    },
  }

  const result = await dispatch(updateAction)
  // Update user and authToken in NextAuth session.
  // await updateAuthUser(result)
  return result
}

export const updatePassword = user => ({
  [CALL_API]: {
    types: [
      CHANGE_PASSWORD_REQUEST,
      CHANGE_PASSWORD_SUCCESS,
      CHANGE_PASSWORD_FAILURE,
    ],
    endpoint: 'users/my',
    method: HTTP_METHODS.PUT,
    body: { user },
  },
})

export const resetPassword = user => ({
  [CALL_API]: {
    types: [
      RESET_PASSWORD_REQUEST,
      RESET_PASSWORD_SUCCESS,
      RESET_PASSWORD_FAILURE,
    ],
    endpoint: 'users/password',
    method: HTTP_METHODS.POST,
    body: { user },
  },
})

export const changePassword = user => ({
  [CALL_API]: {
    types: [
      RESET_PASSWORD_REQUEST,
      RESET_PASSWORD_SUCCESS,
      RESET_PASSWORD_FAILURE,
    ],
    endpoint: 'users/password',
    method: HTTP_METHODS.PUT,
    body: { user },
  },
})
