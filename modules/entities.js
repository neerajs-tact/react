import { isArray, mergeWith, omit } from 'lodash'

/** A mergeWith customizer that overwrites arrays instead of merging them. */
const overwriteArraysCustomizer = (objValue, srcValue) => {
  if (isArray(objValue)) {
    return srcValue
  }
  // Otherwise return undefined, which does a default merge
  return undefined
}

// Actions
const DELETE_ENTITIES = 'entities/DELETE_ENTITIES'

// Initial State
const initialState = {
  user: {},
}

// Reducer
export default (state = initialState, action) => {
  if (action.payload && action.payload.entities) {
    return mergeWith(
      {},
      state,
      action.payload.entities,
      overwriteArraysCustomizer
    )
  }
  if (action.type === DELETE_ENTITIES) {
    return omit(state, action.payload)
  }
  return state
}

// Selectors

export const selectEntities = state => state.entities

// Action Creators

/**
 * Delete the specified entities in the state.
 *
 * @param {string[]} paths The property paths of the entities to delete.
 * @return {{payload: string[], type: string}}
 */
export const deleteEntities = paths => ({
  type: DELETE_ENTITIES,
  payload: paths,
})
