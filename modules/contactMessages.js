import { CALL_API, HTTP_METHODS } from '../lib/api-middleware'

// Actions

const CREATE_CONTACT_MESSAGE_REQUEST =
  'contactMessages/CREATE_CONTACT_MESSAGE_REQUEST'
const CREATE_CONTACT_MESSAGE_SUCCESS =
  'contactMessages/CREATE_CONTACT_MESSAGE_SUCCESS'
const CREATE_CONTACT_MESSAGE_FAILURE =
  'contactMessages/CREATE_CONTACT_MESSAGE_FAILURE'

// Action Creators

// eslint-disable-next-line import/prefer-default-export
export const createContactMessage = contactMessage => ({
  [CALL_API]: {
    types: [
      CREATE_CONTACT_MESSAGE_REQUEST,
      CREATE_CONTACT_MESSAGE_SUCCESS,
      CREATE_CONTACT_MESSAGE_FAILURE,
    ],
    endpoint: 'contact_messages',
    method: HTTP_METHODS.POST,
    body: contactMessage,
  },
})
