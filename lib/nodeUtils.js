/**
 * CommonJS Utility Functions
 *
 * Note that this file uses the CommonJS require and module.exports syntax so it
 * can be used from both the React app and the Node server without transpiling.
 *
 * If you do not need this feature then create your new utility in /lib/utils
 */

// eslint-disable-next-line no-multi-assign
exports = module.exports = {}

/**
 * Load the value of an environment variable and throw an error if not defined
 *
 * @param {string} varName
 * @return {*}
 */
exports.requireEnvVar = function requireServerEnvVar(varName) {
  const envVar = process.env[varName]
  if (!envVar) {
    throw new Error(`${varName} environment variable must be defined!`)
  }
  return envVar
}
