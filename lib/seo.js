import url from 'url'

const defaultConfig = {
  titleTemplate: '%s | Starter',
  title: 'Starter',
  description: 'meta description goes here.',
}

const openGraphImage = '/static/images/open-graph.png'

/**
 * Return a config object for use with Next SEO.
 * @param {ServerRequest} [req]
 * @return {object}
 */
export default function makeSEO(req) {
  if (!req) {
    return defaultConfig
  }

  const baseURL = url.format({ protocol: req.protocol, host: req.get('host') })

  return {
    ...defaultConfig,
    openGraph: {
      site_name: defaultConfig.title,
      images: [{ url: url.resolve(baseURL, openGraphImage) }],
    },
  }
}
