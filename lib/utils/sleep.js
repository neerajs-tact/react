/**
 * Return a promise that resolves after `delay` milliseconds
 *
 * @description helpful tool to simulate loading data and other timings that affect UX
 *
 * @param {int} delay The time, in milliseconds, to sleep
 * @return {Promise}
 */
export default function sleep(delay) {
  return new Promise(resolve => setTimeout(resolve, delay))
}
