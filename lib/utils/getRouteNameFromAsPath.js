import routes from '../../routes'

/**
 * Returns route name if given asPath from ctx
 *
 * @param {string} asPath - Property of NextJS context; e.g. ctx from getInitialProps
 */
export default function getRouteNameFromAsPath(asPath) {
  const matchedRoute = routes.match(asPath).route
  if (matchedRoute) return matchedRoute.name
  return null
}
