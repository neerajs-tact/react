import routes, { Router } from '../../routes'

/**
 * Redirect on either server or client based on ctx.
 * @param {object} ctx     - Next.js context.
 * @param {string} route   - The route to redirect to.
 * @param {object} [query] - Optional query object to use with redirect.
 */
export default function redirect(ctx, route, query) {
  const resolvedQuery = query || ctx.query

  // If `ctx.res` is available it means it's being rendered on server.
  if (ctx.res) {
    const path = routes.findAndGetUrls(route, resolvedQuery).urls.as
    ctx.res.redirect(path)
  }

  // We already checked for server. This should only happen on client.
  Router.pushRoute(route, resolvedQuery)
}
