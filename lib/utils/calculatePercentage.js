/**
 * Calculate percentage and return 0 if divisor is 0.
 *
 * @param {number} dividend
 * @param {number} divisor
 * @return {number}
 */
export default function calculatePercentage(dividend, divisor) {
  if (divisor === 0) return 0
  return Math.round((dividend / divisor) * 100)
}
