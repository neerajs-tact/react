/**
 * Remove non-numbers from string
 *
 * @param {string} str     - String you wish to remove non-numbers from
 * @param {*} [noValue=''] - What returns if there is no string.
 * @returns {string|*}     - String without numbers or noValue.
 */
export default function onlyStringNums(str, noValue = '') {
  return (str && str.replace(/[^\d]/g, '')) || noValue
}
