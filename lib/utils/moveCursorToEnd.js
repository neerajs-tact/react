/**
 * Moves cursor to end of input/textarea.
 *
 * @see https://davidwalsh.name/caret-end
 *
 * @param el - DOM input or textarea element.
 */
export default function moveCursorToEnd(el) {
  if (typeof el.selectionStart === 'number') {
    // eslint-disable-next-line no-param-reassign,no-multi-assign
    el.selectionStart = el.selectionEnd = el.value.length
  } else if (typeof el.createTextRange !== 'undefined') {
    el.focus()
    const range = el.createTextRange()
    range.collapse(false)
    range.select()
  }
}
