const numberFormatUSD = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
})
/**
 * Format USD Currency
 *
 * @param {number|string} usd - Dollar amount in USD
 * @returns {string|null}     - Formatted USD or null if a number isn't provided.
 */
export default function formatCurrencyUSD(usd) {
  return Number.isNaN(parseFloat(usd)) ? null : numberFormatUSD.format(usd)
}
