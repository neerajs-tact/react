// Sample Data for Styleguide

const states = {
  AL: 'Alabama',
  AK: 'Alaska',
  AS: 'American Samoa',
  AZ: 'Arizona',
  AR: 'Arkansas',
  CA: 'California',
  CO: 'Colorado',
  CT: 'Connecticut',
  DE: 'Delaware',
  DC: 'District Of Columbia',
  FM: 'Federated States Of Micronesia',
  FL: 'Florida',
  GA: 'Georgia',
  GU: 'Guam',
  HI: 'Hawaii',
  ID: 'Idaho',
  IL: 'Illinois',
  IN: 'Indiana',
  IA: 'Iowa',
  KS: 'Kansas',
  KY: 'Kentucky',
  LA: 'Louisiana',
  ME: 'Maine',
  MH: 'Marshall Islands',
  MD: 'Maryland',
  MA: 'Massachusetts',
  MI: 'Michigan',
  MN: 'Minnesota',
  MS: 'Mississippi',
  MO: 'Missouri',
  MT: 'Montana',
  NE: 'Nebraska',
  NV: 'Nevada',
  NH: 'New Hampshire',
  NJ: 'New Jersey',
  NM: 'New Mexico',
  NY: 'New York',
  NC: 'North Carolina',
  ND: 'North Dakota',
  MP: 'Northern Mariana Islands',
  OH: 'Ohio',
  OK: 'Oklahoma',
  OR: 'Oregon',
  PW: 'Palau',
  PA: 'Pennsylvania',
  PR: 'Puerto Rico',
  RI: 'Rhode Island',
  SC: 'South Carolina',
  SD: 'South Dakota',
  TN: 'Tennessee',
  TX: 'Texas',
  UT: 'Utah',
  VT: 'Vermont',
  VI: 'Virgin Islands',
  VA: 'Virginia',
  WA: 'Washington',
  WV: 'West Virginia',
  WI: 'Wisconsin',
  WY: 'Wyoming',
}

export const selectOptions = Object.keys(states).map(value => ({
  label: states[value],
  value,
}))

export const groupedSelectOptions = [
  {
    label: 'Produce',
    groupOptions: [
      {
        label: 'Apples',
        value: 'apples',
      },
      {
        label: 'Oranges',
        value: 'oranges',
      },
    ],
  },
  {
    label: 'Dairy',
    groupOptions: [
      {
        label: 'Butter',
        value: 'butter',
      },
      {
        label: 'Milk',
        value: 'milk',
      },
    ],
  },
  {
    label: 'Frozen Food',
    groupOptions: [
      {
        label: 'Pizza',
        value: 'pizza',
      },
      {
        label: 'Ice Cream',
        value: 'icecream',
      },
    ],
  },
]

export const multiCheckboxOptions = [
  {
    label: 'Checkbox Unchecked',
    value: 'one',
  },
  {
    label: 'Checkbox Checked',
    value: 'two',
  },
  {
    label: 'Checkbox Disabled',
    value: 'disabled',
    disabled: true,
  },
]

export const multiRadioOptions = [
  {
    label: 'Radio Unselected',
    value: 'one',
  },
  {
    label: 'Radio Selected',
    value: 'two',
  },
  {
    label: 'Radio Disabled',
    value: 'disabled',
    disabled: true,
  },
]

export const multiCheckboxInlineOptions = [
  {
    label: 'Unchecked',
    value: 'one',
  },
  {
    label: 'Checked',
    value: 'two',
  },
  {
    label: 'Disabled',
    value: 'disabled',
    disabled: true,
  },
]

export const multiRadioInlineOptions = [
  {
    label: 'Unselected',
    value: 'one',
  },
  {
    label: 'Selected',
    value: 'two',
  },
  {
    label: 'Disabled',
    value: 'disabled',
    disabled: true,
  },
]

export const multiSwitchOptions = [
  {
    label: 'Toggle Unselected',
    value: 'one',
  },
  {
    label: 'Toggle Selected',
    value: 'two',
  },
  {
    label: 'Toggle Disabled',
    value: 'disabled',
    disabled: true,
  },
]

export const yesNoRadioOptions = [
  {
    label: 'Yes',
    value: 'Yes',
  },
  {
    label: 'No',
    value: 'No',
  },
]

export const idNumsAndContextOptions = [
  {
    label: '0: Success',
    value: 0,
    contextColor: 'success',
  },
  {
    label: '1: Danger',
    value: 1,
    contextColor: 'danger',
  },
  {
    label: '2: Info',
    value: 2,
    contextColor: 'info',
  },
  {
    label: '3: Warning',
    value: 3,
    contextColor: 'warning',
  },
]
