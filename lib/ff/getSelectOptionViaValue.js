/**
 * Get selected select option data from value
 *
 * @description Get the option data for a selected option of a select in Final-Form.
 *
 * @param {string} selectedValue                - Selected Select value from Final-Form.
 * @param {Object[]} selectOptions              - Select options data.
 * @typedef {Object} selectOptions[]            - Select option data.
 * @property {string|number} value              - Select option value.
 * @param {function} [valueConvertFunc]         - Optional function to process option data values if FF values are parsed differently.
 * @returns {Object|undefined} - Selected select option data if there is a match otherwise undefined.
 */
export default function getSelectOptionViaValue(
  selectedValue,
  selectOptions,
  valueConvertFunc = value => value // by default no parsing
) {
  return selectOptions.find(
    // Type coercion is wanted here since Final-Form will convert option number values to strings
    // eslint-disable-next-line eqeqeq
    selectOption => selectOption.value == valueConvertFunc(selectedValue)
  )
}
