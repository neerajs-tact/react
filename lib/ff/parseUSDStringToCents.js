import { onlyStringNums } from '../utils'

/**
 * Parse USD currency string into number of cents.
 *
 * @param {boolean} [zeroUndefined=true] - option to allow return of zero value otherwise undefined.
 * @returns {function(string)} - Function to convert USD string to cents.
 */
export default function parseUSDStringToCents(zeroUndefined = true) {
  /**
   * @param {string} usdString - USD currency string e.g. $0.00 must include 2 decimals.
   * @returns {number|undefined } Number of cents or undefined if amount is zero and allowZero is true.
   */
  return usdString => {
    const cents = parseInt(onlyStringNums(usdString), 10)
    if (cents === 0 && zeroUndefined) return undefined
    return cents
  }
}
