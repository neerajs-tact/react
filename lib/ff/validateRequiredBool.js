/**
 * Required Boolean
 *
 * @param {string} [msg='Required.']   - Error Message. Can include HTML.
 * @returns {function(string): string} - Function that returns an error message if value is not true or false.
 */
export default function validateRequiredBool(msg = 'Required.') {
  return value => (typeof value === 'boolean' ? undefined : msg)
}
