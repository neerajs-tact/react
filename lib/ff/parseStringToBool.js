/**
 * Yes/No String to Bool
 *
 * @description used in parse prop of yes/no Final-Form radios to store Yes/No as true/false.
 *
 * @param {string} [yesStr="Yes"]  - String value associated with true.
 * @param {string} [noStr="No"]    - String value associated with false.
 * @returns {null|boolean}         - Boolean associated with string value otherwise null.
 */
export default function parseStringToBool(yesStr = 'Yes', noStr = 'No') {
  return value => {
    switch (value) {
      case yesStr:
        return true
      case noStr:
        return false
      default:
        return null // null with our API ensures field is recorded as emptied.
    }
  }
}
