/**
 * Boolean to Yes/No String
 *
 * @description used in format prop of yes/no Final-Form radios to match true/false to Yes/No strings.
 *
 * @param {string} [yesStr="Yes"]        - String value to return if value is true.
 * @param {string} [noStr="No"]          - String value to return if value is false.
 * @returns {undefined|string}           - String for matching boolean otherwise empty string.
 */
export default function formatBoolToString(yesStr = 'Yes', noStr = 'No') {
  return value => {
    switch (value) {
      case true:
        return yesStr
      case false:
        return noStr
      default:
        return ''
    }
  }
}
