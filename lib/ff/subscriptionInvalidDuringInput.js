/**
 * Subscription to show error during user input.
 *
 * @description Put this object in the "subscription" prop of <FormFeedbackAdapter /> or the
 *              "errorSubscription" prop of <FFormGroup />. Should be paired with the use of
 *              invalidDuringInput in the invalid prop(s).
 */
const subscriptionInvalidDuringInput = {
  error: true,
  submitError: true,
  invalid: true,
  visited: true,
  pristine: true,
  touched: true,
}

export default subscriptionInvalidDuringInput
