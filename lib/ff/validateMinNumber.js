/**
 * Min Number
 *
 * @param {number} min                 - Number which value can't be less than.
 * @param {string} [msg='...']         - Error Message. Can include HTML.
 * @returns {function(string): string} - Function that returns an error message if value less than min.
 */
export default function validateMinNumber(
  min,
  msg = `Must be a minimum of ${min}`
) {
  return value => {
    return !Number.isNaN(parseFloat(value)) && value < min ? msg : undefined
  }
}
