import { FORM_ERROR } from 'final-form'
import mapValues from 'lodash/mapValues'
import startCase from 'lodash/startCase'

/**
 * Return a function that can accept an error returned by our API and returns
 * an error object to be used by Final Form.
 *
 * @see https://github.com/final-form/final-form#onsubmit-values-object-callback-errors-object--void--object--promiseobject--void
 * @param {string} [defaultErrorMessage] The error message to use if one is
 *   not provided by the API
 * @return {function}
 */
export default function mapFinalFormErrors(
  defaultErrorMessage = 'An error occurred.'
) {
  return serverError => {
    const { errors, message } = serverError

    const formErrors = mapValues(
      errors,
      (error, name) => `${startCase(name)} ${error}`
    )

    formErrors[FORM_ERROR] = message || defaultErrorMessage

    return formErrors
  }
}
