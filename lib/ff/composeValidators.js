/**
 * Compose Validators (Function for combining validators)
 *
 * @param {*} validators            - Validators as params will be composed together.
 * @returns {function(...[*]): (*)} - Returns first error message from validators.
 *
 * @example
 * // first validates with validateRequired() then validateMinLength(8)
 * composeValidators(
 *   validateRequired(),
 *   validateMinLength(8)
 * )
 *
 */
export default function composeValidators(...validators) {
  return (...validatorParams) =>
    validators.reduce(
      (error, validator) => error || validator(...validatorParams),
      undefined
    )
}
