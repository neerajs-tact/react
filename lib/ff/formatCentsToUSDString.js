import { formatCurrencyUSD } from '../utils'

/**
 * Format number of cents into USD currency string.
 *
 * @param {number|string} cents=0 - Cents of USD currency amount. Zero default allows moveCursorToEnd
 *                                 util to work properly for new entry.
 * @returns {string}             - USD currency formatted string; e.g. $47.47.
 */
export default function formatCentsToUSDString(cents = 0) {
  return formatCurrencyUSD(cents / 100)
}
