/**
 * Required
 *
 * @param {string} [msg='Required.']   - Error Message. Can include HTML.
 * @returns {function(string|number): string} - Function that returns an error message if value is empty.
 */
export default function validateRequired(msg = 'Required.') {
  // check against length because unfilled checkboxes produces an empty array in Final-Form
  return value =>
    (value && value.length !== 0) || value === 0 ? undefined : msg
}
