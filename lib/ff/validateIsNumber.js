/**
 * Is a Number
 *
 * @param {string} [msg='Must be a number.'] - Error message. Can include HTML.
 * @returns {function(string): string}       - Function that returns the error message if a provided value is not a number.
 */
export default function validateIsNumber(msg = 'Must be a number.') {
  return value => (value && !Number.isNaN(value) ? msg : undefined)
}
