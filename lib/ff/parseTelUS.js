import { onlyStringNums } from '../utils'

/**
 * Parse formatted US phone number to E.164.
 *
 * @param {string} telString - Formatted telephone string.
 * @returns {string|null}    - Telephone numbers only, limit 10 with +1 US code, or null if empty for Redux/API.
 */
export default function parseTelUS(telString) {
  return (telString && `+1${onlyStringNums(telString).slice(0, 10)}`) || null
}
