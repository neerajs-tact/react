/**
 * Min Length
 *
 * @param {number} minLength - Length (characters or array items) value must be less than or equal to.
 * @param {string} [msg=`Must be minimum of ${minLength}.`] - Error message. Can include HTML.
 * @returns {function(string): string} - Function that returns the error message if a provided value is less than minLength.
 */
export default function validateMinLength(
  minLength,
  msg = `Must be minimum of ${minLength}.`
) {
  return value => (value && value.length < minLength ? msg : undefined)
}
