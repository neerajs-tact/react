import { onlyStringNums } from '../utils'

/**
 * Format E.164 US phone number.
 *
 * @param {string} telString - US phone number string in E.164 format (has +1 country code)
 * @returns {string}         - Formatted US phone number.
 */
export default function formatTelUS(telString) {
  if (!telString) return ''
  const telNums = onlyStringNums(telString).substring(1) // removes +1 country code
  const numNums = telNums.length
  if (numNums <= 3) return `(${telNums}`
  if (numNums <= 6) return `(${telNums.slice(0, 3)}) ${telNums.slice(3, 6)}`
  return `(${telNums.slice(0, 3)}) ${telNums.slice(3, 6)}-${telNums.slice(
    6,
    10
  )}`
}
