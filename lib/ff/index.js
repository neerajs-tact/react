/**
 * Validators for Final-Form Synchronous Field-Level Validation
 *
 * @see https://github.com/final-form/react-final-form#synchronous-record-level-validation
 * @see Implementation /components/styleguide/FormElements/
 *
 * @example
 * // field validation of required input with default error message
 * validate={validateRequired()}
 *
 * @example
 * // field validation of required input with custom required error message
 * validate={validateRequired('This is really required.')}
 *
 * @example
 * // field validation of min length with custom error message with HTML (wrap with fragment)
 * validate={validateMinLength(6, <>Use at least 6 characters.</>)}
 *
 * @example
 * // field validation of required plus matching field named password with custom error message.
 * validate={composeValidators(
 *   validateRequired(),
 *   validateMatch('password', 'Passwords must match.')
 * )}
 *
 * @example
 * If you are using the <FFormGroup/> adapter the prop name is instead "validators" and you must
 * add your validation functions in an array. They will automatically be composed without needing to
 * use composeValidators.
 *
 * validators={[validateRequired(), validateMatch('password', 'Passwords must match.')]}
 *
 */

export composeValidators from './composeValidators'
export validateEmail from './validateEmail'
export validateIsNumber from './validateIsNumber'
export validateMatch from './validateMatch'
export validateMaxLength from './validateMaxLength'
export validateMinLength from './validateMinLength'
export validateMinNumber from './validateMinNumber'
export validateRequired from './validateRequired'
export validateRequiredBool from './validateRequiredBool'
export validateTelUS from './validateTelUS'

/**
 * Parse & Format
 *
 * Unlike the general utilities these are designed to use in Final-Form parse/format props. These
 * are often used in pairs to keep data stored one way in Final-Form but displayed in another way
 * for the user.
 *
 * Be mindful with how these affect the user's ability to edit the field; e.g. cursor position and
 * forced formatting preventing backspace.
 */

export parseStringToBool from './parseStringToBool'
export formatBoolToString from './formatBoolToString'

export parseTelUS from './parseTelUS'
export formatTelUS from './formatTelUS'

export parseUSDStringToCents from './parseUSDStringToCents'
export formatCentsToUSDString from './formatCentsToUSDString'

export parseStrToNum from './parseStrToNum'
export formatNumToStr from './formatNumToStr'

/**
 * Error display and highlighting conditions.
 *
 * These are used in invalid and subscription props to update the conditions when error highlighting
 * and messaging is shown.
 */

export invalidDuringInput from './invalidDuringInput'
export subscriptionInvalidDuringInput from './subscriptionInvalidDuringInput'

/**
 * Other
 */

export mapFinalFormErrors from './mapFinalFormErrors'
export getSelectOptionViaValue from './getSelectOptionViaValue'
