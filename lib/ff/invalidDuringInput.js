/**
 * Show error highlighting during user input.
 *
 * @description Put this function in the "invalid" prop of <Field/ >, <FormFeedbackAdapter />, or
 *              <FFormGroup /> to update the condition when the error highlighting is shown.
 *              <FormFeedbackAdapter /> or <FFormGroup />, whichever you are using, will also need
 *              subscriptionInvalidDuringInput to apply the matching subscription.
 *
 * @param {object} meta - Final-form's meta object. Automatically used.
 * @returns {boolean}   - Boolean that is true when user is typing in Final-Form field.
 */
export default function invalidDuringInput(meta) {
  return meta.invalid && ((meta.visited && !meta.pristine) || meta.touched)
}
