/**
 * Match
 *
 * @param {string} fieldName                          - The name of the field you want to validate is a match.
 * @param {string} [msg='Does not match.']            - Error message. Can include HTML.
 * @returns {function(string|number, object): string} - Function that returns an error message if a provide value does not match a given value from fieldName.
 */
export default function validateMatch(fieldName, msg = 'Does not match.') {
  return (value, allValues) =>
    (value || value === 0) && value !== allValues[fieldName] ? msg : undefined
}
