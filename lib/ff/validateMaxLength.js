/**
 * Max Length
 *
 * @param {number} maxLength - Length (characters or array items) value must be greater than or equal to.
 * @param {string} [msg=`Must be maximum of ${maxLength}.`] - Error message. Can include HTML.
 * @returns {function(string): string} - Function that returns the error message if value is greater than maxLength.
 */
export default function validateMaxLength(
  maxLength,
  msg = `Can not be more than ${maxLength}.`
) {
  return value => (value && value.length > maxLength ? msg : undefined)
}
