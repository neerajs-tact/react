import { isNumber } from 'lodash'

/**
 * Parse Number to String
 *
 * @description - Select fields convert selected number values to strings but it can mismatch when
 *                initialValues remain numbers. Apply this format numbers back into string so the
 *                matching option can be selected when paired with formatNumToStr.
 *
 * @param {*} nonNumberVal='' - Value returned if value is not a number. Default is empty string
 *                              so it properly maps to select placeholder option.
 * @returns {function(*=): *} - String from number or nonNumberVal if not a number.
 */
export default function formatNumToStr(nonNumberVal = '') {
  return value => {
    return isNumber(value) ? value.toString() : nonNumberVal
  }
}
