import { toNumber } from 'lodash'

/**
 * Parse String to Number
 *
 * @description - Select fields convert selected number values to strings but it can mismatch when
 *                initialValues remain numbers. Apply this parse to the select field to keep data
 *                as numbers but must be paired with formatNumToStr.
 *
 * @param {*} nonNumberVal=undefined - Value returned if value is not a number. Default is undefined
 *                                     as it will usually empty field when sent to backend API.
 * @returns {function(*=): *} - Number from string or nonNumberVal if not a number.
 */
export default function parseStrToNum(nonNumberVal = undefined) {
  return value => {
    const toNum = toNumber(value)
    return Number.isNaN(toNum) ? nonNumberVal : toNum
  }
}
