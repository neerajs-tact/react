import { onlyStringNums } from '../utils'

/**
 * Is a US Phone Number (1 + 10-digits)
 *
 * @param {string} [msg='...']         - Error message. Can include HTML.
 * @returns {function(string): string} - Function that returns an error message if value is not +1 with 10-digits.
 */
export default function validateTelUS(
  msg = 'Must be a 10-digit US phone number.'
) {
  return value =>
    value && onlyStringNums(value).length !== 11 ? msg : undefined
}
