import isEmail from 'validator/lib/isEmail'
/**
 * Email
 *
 * @param {string} [msg='Please enter a valid email address.']
 *    - Override default error message with your own. Can include HTML.
 * @returns {function(string): string} - Function that returns an error message if value is provided and it is not an email address.
 */
export default function validateEmail(
  msg = 'Please enter a valid email address.'
) {
  return value => (value && !isEmail(value) ? msg : undefined)
}
