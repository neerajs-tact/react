import { applyMiddleware, combineReducers, createStore } from 'redux'
import createDebounce from 'redux-debounced'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import thunk from 'redux-thunk'
import * as reducers from '../modules'
import makeAPI from './api-middleware'
import { RESET_ALL_STATE } from '../modules/users'

const appReducer = combineReducers(reducers)

const rootReducer = (state, action) => {
  // Reset state to initialState upon sign out
  // See https://stackoverflow.com/q/35622588/956688
  if (action.type === RESET_ALL_STATE) {
    return appReducer(undefined, action)
  }

  return appReducer(state, action)
}

export default (initialState, { req }) => {
  const middlewares = [createDebounce(), thunk, makeAPI(req)]

  return createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
  )
}
