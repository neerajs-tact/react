import { camelizeKeys, decamelizeKeys } from 'humps'
import fetch from 'isomorphic-unfetch'
import param from 'jquery-param'
import { isPlainObject, isUndefined, omitBy } from 'lodash'
import getConfig from 'next/config'
import { normalize } from 'normalizr'
import url from 'url'
import { getAuthHeaders, signOut } from './auth'

const { publicRuntimeConfig } = getConfig()
const apiRoot = url.resolve(publicRuntimeConfig.apiUrl, '/api/v1/')

// Available HTTP request methods
export const HTTP_METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
}

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = Symbol('Call API')

const handleResponse = schema => async response => {
  if (response.status === 204) {
    // No Content
    return response.statusText
  }
  if (response.status >= 500) {
    // Server Error
    throw response.statusText
  }

  // Assume JSON response
  const json = camelizeKeys(await response.json())

  if (!response.ok) {
    if (response.status === 401) {
      // Unauthorized
      await signOut({ error: json.message })
    }
    throw json
  }

  return schema ? normalize(json, schema) : json
}

/**
 * Return an API middleware for use with Redux.
 * A server request can be provided to access auth headers from the session.
 * @param {ServerRequest} [req]
 * @return {function}
 */
const makeAPI = (req = null) => {
  const callAPI = async (endpoint, method, body, schema) => {
    // Build full URL by prepending API server URL
    const url = endpoint.indexOf(apiRoot) === -1 ? apiRoot + endpoint : endpoint

    const authHeaders = await getAuthHeaders(req)

    const requestOptions = {
      method,
      body,
      headers: {
        Accept: 'application/json',
        ...authHeaders,
      },
    }

    if (isPlainObject(body)) {
      requestOptions.body = JSON.stringify(decamelizeKeys(body))
      requestOptions.headers['Content-Type'] = 'application/json'
    }

    return fetch(url, requestOptions).then(handleResponse(schema))
  }

  /**
   * A Redux middleware that interprets actions with CALL_API options specified.
   * Performs the call and promises when such actions are dispatched.
   */
  return () => next => async action => {
    const callAPIOptions = action[CALL_API]
    if (typeof callAPIOptions === 'undefined') {
      return next(action)
    }

    const { query, body, types, schema } = callAPIOptions
    let { endpoint, method } = callAPIOptions

    if (typeof endpoint !== 'string') {
      throw new Error('Specify a string endpoint URL.')
    }

    // If query params are specified, append them to endpoint
    if (typeof query !== 'undefined') {
      if (!isPlainObject(query)) {
        throw new Error('Expected query to be an object.')
      }
      endpoint += `?${param(decamelizeKeys(omitBy(query, isUndefined)))}`
    }

    if (typeof method === 'undefined') {
      method = HTTP_METHODS.GET
    } else if (!{}.hasOwnProperty.call(HTTP_METHODS, method)) {
      throw new Error('Invalid HTTP method.')
    }

    if (!Array.isArray(types) || types.length !== 3) {
      throw new Error('Expected an array of three action types.')
    }
    if (!types.every(type => typeof type === 'string')) {
      throw new Error('Expected action types to be strings.')
    }

    const actionWith = data => {
      const finalAction = Object.assign({}, action, data)
      delete finalAction[CALL_API]
      return finalAction
    }

    const [requestType, successType, failureType] = types
    next(actionWith({ type: requestType }))

    try {
      const response = await callAPI(endpoint, method, body, schema)
      const result = await next(
        actionWith({
          type: successType,
          payload: response,
        })
      )
      return result.payload || result
    } catch (error) {
      const result = await next(
        actionWith({
          type: failureType,
          error: error || 'Something bad happened',
        })
      )
      throw result.error || result
    }
  }
}

export default makeAPI
