/* eslint-disable react/no-multi-comp */
import { get } from 'lodash'
import { NextAuth } from 'next-auth/client'
import React from 'react'
import { wrapDisplayName } from 'recompose'
import { Router, UNAUTHORIZED_REDIRECT_ROUTE, USER_HOME_ROUTE } from '../routes'
import { redirect } from './utils'

/**
 * Sign out the user.
 * @param {string} [error] An error message to show after sign out
 */
export const signOut = async ({ error } = {}) => {
  const params = error
    ? {
        alertContext: 'danger',
        alertMsg: error,
      }
    : {
        alertContext: 'success',
        alertMsg: 'You have successfully signed out.',
      }
  try {
    await NextAuth.signout()
    await Router.pushRoute('auth-callback', params)
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error)
    Router.pushRoute('auth-error', { action: 'signout' })
  }
}

/**
 * Return headers that are used to access the API.
 * @param {ServerRequest} req
 * @return {Promise}
 */
export const getAuthHeaders = async req => {
  const session = await NextAuth.init({ req })
  return (
    session &&
    session.user && {
      Authorization: `Bearer ${session.user.authToken}`,
    }
  )
}

/**
 * Return the user object when authorized.
 * Note that the actual user object is nested within the NextAuth user object.
 * @param {object} session The NextAuth session object
 * @return {object|undefined}
 * @see the `serialize` function in next-auth.functions.js
 */
export const getAuthUser = session => get(session, 'user.user')

/**
 * Update the `user` and `authToken` in the NextAuth session.
 * The `userAndToken` param passed to this function should be the response from
 * the Users#update API endpoint. This works in conjunction with the `signIn`
 * function in `next-auth.functions.js`
 * @see `signIn` function in `next-auth.functions.js`
 * @param {object} userAndToken The user and authToken as returned by the API
 * @param {object} userAndToken.user
 * @param {string} userAndToken.authToken
 * @return {Promise<void>}
 */
export const updateAuthUser = async userAndToken => {
  await NextAuth.signin({ user: JSON.stringify(userAndToken) })
  await NextAuth.init({ force: true })
}

/**
 * A higher-order component used to wrap Next.js pages.
 * A page wrapped with this HOC will redirect to `USER_HOME_ROUTE` if
 * the user is signed in. Otherwise it will render the wrapped page.
 * @param {React.Component} WrappedComponent The Next.js page to wrap
 * @return {React.ComponentClass}
 */
export const forwardAuth = WrappedComponent =>
  class extends React.Component {
    static displayName = wrapDisplayName(WrappedComponent, 'forwardAuth')

    /**
     * If user is authenticated, redirect to user home.
     * @param {object} ctx Next.js context
     */
    static async getInitialProps(ctx) {
      // The NextAuth session is provided via ctx in pages/_app.jsx
      const authUser = getAuthUser(ctx.session)

      if (authUser) {
        return redirect(ctx, USER_HOME_ROUTE)
      }

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx))

      return { ...componentProps }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

/**
 * A higher-order component used to wrap Next.js pages.
 * A page wrapped with this HOC will redirect to `UNAUTHORIZED_REDIRECT_ROUTE`
 * if the user is not signed in. Otherwise it will render the wrapped page.
 * Additionally, an `authUser` prop is passed to the page that contains the
 * user data from the NextAuth session.
 * @param {React.Component} WrappedComponent The Next.js page to wrap
 * @return {React.ComponentClass}
 */
export const requireAuth = WrappedComponent =>
  class extends React.Component {
    static displayName = wrapDisplayName(WrappedComponent, 'requireAuth')

    /**
     * Load auth user from session if available. If not, redirect to login page.
     * @param {object} ctx Next.js context
     * @see https://github.com/zeit/next.js/blob/master/examples/with-cookie-auth/www/utils/auth.js
     */
    static async getInitialProps(ctx) {
      // The NextAuth session is provided via ctx in pages/_app.jsx
      const authUser = getAuthUser(ctx.session)

      if (!authUser) {
        return redirect(ctx, UNAUTHORIZED_REDIRECT_ROUTE)
      }

      const componentProps =
        WrappedComponent.getInitialProps &&
        (await WrappedComponent.getInitialProps(ctx))

      return { ...componentProps, authUser }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }
