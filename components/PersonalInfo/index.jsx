import React, { useCallback } from 'react'
import { func, string } from 'prop-types'
import { connect } from 'react-redux'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import PersonalInfoForm from './PersonalInfoForm'
import {
  selectCurrentUserEmail,
  selectCurrentUserFirstName,
  selectCurrentUserLastName,
  selectCurrentUserPhoneNumber,
  updateCurrentUser,
} from '../../modules/users'
import { mapFinalFormErrors } from '../../lib/ff'

const mapDispatch = { updateCurrentUser }

const PersonalInfo = connect(
  mapState,
  mapDispatch
)(PersonalInfoFormController)

function mapState(state) {
  return {
    email: selectCurrentUserEmail(state),
    firstName: selectCurrentUserFirstName(state),
    lastName: selectCurrentUserLastName(state),
    phoneNumber: selectCurrentUserPhoneNumber(state),
  }
}

PersonalInfoFormController.propTypes = {
  updateCurrentUser: func.isRequired,
  email: string.isRequired,
  firstName: string.isRequired,
  lastName: string.isRequired,
  phoneNumber: string,
}

PersonalInfoFormController.defaultProps = {
  phoneNumber: undefined,
}

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors('Failed to update Personal Info.')

function PersonalInfoFormController({
  email,
  firstName,
  lastName,
  phoneNumber,
  updateCurrentUser,
}) {
  const onSubmit = useCallback(
    async function onPersonalInfoFormSubmit(values) {
      try {
        const updatedUser = {
          ...values,
          profileAttributes: {},
        }
        await updateCurrentUser(updatedUser)
        return undefined
      } catch (error) {
        return mapErrors(error)
      }
    },
    [updateCurrentUser]
  )

  return (
    <Form
      component={PersonalInfoForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      initialValues={{
        email,
        firstName,
        lastName,
        phoneNumber,
      }}
      // restricts which parts of form state the form component needs to render
      subscription={{
        dirtySinceLastSubmit: true,
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default PersonalInfo
