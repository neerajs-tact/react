import React from 'react'
import { bool, func } from 'prop-types'
import { useUIDSeed } from 'react-uid'
import { Alert, Button, Col, Form, Row } from 'reactstrap'
import {
  formatTelUS,
  parseTelUS,
  validateEmail,
  validateRequired,
  validateTelUS,
} from '../../lib/ff'
import { FFormGroup, SubmitErrorAlerts } from '../ff'
import { moveCursorToEnd } from '../../lib/utils'

PersonalInfoForm.propTypes = {
  handleSubmit: func.isRequired,
  dirtySinceLastSubmit: bool.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function PersonalInfoForm({
  handleSubmit,
  dirtySinceLastSubmit,
  submitSucceeded,
  submitting,
}) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <FFormGroup
        label="Email Address"
        name="email"
        type="email"
        seed={seed}
        validators={[validateRequired(), validateEmail()]}
        autoComplete="username email"
        inputMode="email" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
        formText="You will use your email address to log in."
      />
      <Row>
        <Col xs={12} md={6}>
          <FFormGroup
            label="First Name"
            name="firstName"
            type="text"
            seed={seed}
            validators={[validateRequired()]}
            autoComplete="given-name"
          />
        </Col>
        <Col xs={12} md={6}>
          <FFormGroup
            label="Last Name"
            name="lastName"
            type="text"
            seed={seed}
            validators={[validateRequired()]}
            autoComplete="family-name"
          />
        </Col>
      </Row>
      <FFormGroup
        label="Phone Number (optional)"
        name="phoneNumber"
        type="tel"
        seed={seed}
        parse={parseTelUS}
        format={formatTelUS}
        validators={[validateTelUS()]}
        onClick={event => moveCursorToEnd(event.target)}
        onFocus={event => moveCursorToEnd(event.target)}
        inputMode="tel" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
      />

      <SubmitErrorAlerts />

      <Alert color="success" isOpen={submitSucceeded && !dirtySinceLastSubmit}>
        Your personal info has been successfully updated.
      </Alert>

      <Button
        block
        className="uaTrack-submit-personal-info"
        color="primary"
        disabled={submitting}
        size="lg"
        type="submit"
      >
        {submitting ? 'Updating Account' : 'Update Account'}
      </Button>
    </Form>
  )
}

export default PersonalInfoForm
