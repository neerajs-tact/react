import React, { useCallback } from 'react'
import { func, string } from 'prop-types'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import { connect } from 'react-redux'
import { mapFinalFormErrors } from '../../lib/ff'
import { createContactMessage } from '../../modules/contactMessages'
import ContactForm from './ContactForm'
import {
  selectCurrentUserEmail,
  selectCurrentUserFirstName,
  selectCurrentUserLastName,
  selectCurrentUserPhoneNumber,
} from '../../modules/users'

const mapDispatch = { createContactMessage }

function mapState(state) {
  return {
    email: selectCurrentUserEmail(state),
    firstName: selectCurrentUserFirstName(state),
    lastName: selectCurrentUserLastName(state),
    phoneNumber: selectCurrentUserPhoneNumber(state),
  }
}

const Contact = connect(
  mapState,
  mapDispatch
)(ContactFormController)

ContactFormController.propTypes = {
  createContactMessage: func.isRequired,
  email: string,
  firstName: string,
  lastName: string,
  phoneNumber: string,
}

ContactFormController.defaultProps = {
  email: undefined,
  firstName: undefined,
  lastName: undefined,
  phoneNumber: undefined,
}

const contactReasonOptions = [
  { label: 'Additional Information', value: 'Additional Information' },
  { label: 'Error Reporting', value: 'Error Reporting' },
  { label: 'Other', value: 'Other' },
]

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors('Failed to send message.')

function ContactFormController({
  email,
  firstName,
  lastName,
  phoneNumber,
  createContactMessage,
}) {
  const onSubmit = useCallback(
    async function onContactFormSubmit(values, form) {
      try {
        await createContactMessage(values)
        // clears focus if submit via enter
        document.activeElement.blur()
        // re-initialize form AFTER submitSuccess to prevent focus on first field
        setTimeout(() => form.initialize({ email, firstName, lastName }), 20) // resets form without losing submitSucceeded state
        return undefined
      } catch (error) {
        return mapErrors(error)
      }
    },
    [createContactMessage, email, firstName, lastName]
  )

  return (
    <Form
      component={ContactForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      initialValues={{
        email,
        firstName,
        lastName,
        phoneNumber,
      }}
      contactReasonOptions={contactReasonOptions}
      subscription={{
        pristine: true,
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default Contact
