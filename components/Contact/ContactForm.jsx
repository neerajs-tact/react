import React from 'react'
import { bool, func } from 'prop-types'
import { useUIDSeed } from 'react-uid'
import { Alert, Button, Form } from 'reactstrap'
import { validateEmail, validateRequired } from '../../lib/ff'
import { FFormGroup, SubmitErrorAlerts } from '../ff'

ContactForm.propTypes = {
  handleSubmit: func.isRequired,
  pristine: bool.isRequired,
  submitting: bool.isRequired,
  submitSucceeded: bool.isRequired,
}

const contactReasonOptions = [
  { label: 'Additional Information', value: 'Additional Information' },
  { label: 'Error Reporting', value: 'Error Reporting' },
  { label: 'Other', value: 'Other' },
]

function ContactForm({ handleSubmit, pristine, submitting, submitSucceeded }) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <FFormGroup
        label="First Name"
        name="firstName"
        type="text"
        seed={seed}
        validators={[validateRequired()]}
        autoComplete="given-name"
      />
      <FFormGroup
        label="Last Name"
        name="lastName"
        type="text"
        seed={seed}
        validators={[validateRequired()]}
        autoComplete="family-name"
      />
      <FFormGroup
        label="Email Address"
        name="email"
        type="email"
        seed={seed}
        validators={[validateRequired(), validateEmail()]}
        autoComplete="username email"
        inputMode="email" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
      />
      <FFormGroup
        label="Reason"
        name="reason"
        type="select"
        seed={seed}
        validators={[validateRequired()]}
        placeholder="Select reason for contacting"
        options={contactReasonOptions}
      />
      <FFormGroup
        label="Message"
        name="message"
        type="textarea"
        seed={seed}
        validators={[validateRequired()]}
        rows="5"
      />
      <SubmitErrorAlerts />
      <Alert
        color="success"
        className="text-center"
        isOpen={submitSucceeded && pristine}
      >
        Your message has been sent. Expect a response shortly!
      </Alert>
      <Button
        block
        className="uaTrack-submit-contact-message"
        color="primary"
        size="lg"
        type="submit"
        disabled={submitting}
      >
        {submitting ? 'Sending' : 'Send'} Message
      </Button>
    </Form>
  )
}

export default ContactForm
