import React from 'react'
import { oneOf, shape, string } from 'prop-types'
import { withRouter } from 'next/router'
import { UncontrolledAlert } from 'reactstrap'

UrlQueryAlert.propTypes = {
  className: string,
  // eslint-disable-next-line react/require-default-props
  router: shape({
    query: shape({
      alertContext: oneOf([
        'primary',
        'secondary',
        'success',
        'danger',
        'warning',
        'info',
        'light',
        'lighter',
        'dark',
      ]),
      alertMsg: string,
    }),
  }),
}

UrlQueryAlert.defaultProps = {
  className: undefined,
}

function UrlQueryAlert({ className, router }) {
  const { alertContext, alertMsg } = router.query

  return alertMsg && alertContext ? (
    <UncontrolledAlert className={className} color={alertContext}>
      {alertMsg}
    </UncontrolledAlert>
  ) : null
}

export default withRouter(UrlQueryAlert)
