import { bool, func } from 'prop-types'
import React from 'react'
import { Button, Col, Form, FormGroup, Row } from 'reactstrap'
import { useUIDSeed } from 'react-uid'
import {
  formatTelUS,
  parseTelUS,
  validateEmail,
  validateMinLength,
  validateRequired,
  validateTelUS,
} from '../../lib/ff'
import { FFormGroup, FormFeedbackAdapter, SubmitErrorAlerts } from '../ff'
import { moveCursorToEnd } from '../../lib/utils'

RegisterForm.propTypes = {
  handleSubmit: func.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function RegisterForm({ handleSubmit, submitSucceeded, submitting }) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <FFormGroup
        label="Email Address"
        name="email"
        type="email"
        seed={seed}
        validators={[validateRequired(), validateEmail()]}
        autoComplete="username email"
        inputMode="email" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
        formText="You will use your email address to log in."
      />
      <FFormGroup
        label="Password"
        name="password"
        type="password"
        seed={seed}
        validators={[
          validateRequired(),
          validateMinLength(6, 'Use at least 6 characters.'),
        ]}
        autoComplete="new-password"
      />
      <Row>
        <Col xs={12} md={6}>
          <FFormGroup
            label="First Name"
            name="firstName"
            type="text"
            seed={seed}
            validators={[validateRequired()]}
            autoComplete="given-name"
          />
        </Col>
        <Col xs={12} md={6}>
          <FFormGroup
            label="Last Name"
            name="lastName"
            type="text"
            seed={seed}
            validators={[validateRequired()]}
            autoComplete="family-name"
          />
        </Col>
      </Row>
      <FFormGroup
        label="Phone Number (optional)"
        name="phoneNumber"
        type="tel"
        seed={seed}
        parse={parseTelUS}
        format={formatTelUS}
        validators={[validateTelUS()]}
        onClick={event => moveCursorToEnd(event.target)}
        onFocus={event => moveCursorToEnd(event.target)}
        inputMode="tel" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
      />
      <FormGroup className="mt-10">
        <FFormGroup
          label={
            <>
              I agree to the{' '}
              <a href="#" target="_blank" className="uaTrack-a-register-terms">
                Terms
              </a>{' '}
              and{' '}
              <a
                href="#"
                target="_blank"
                className="uaTrack-a-register-privacy"
              >
                Privacy Policy
              </a>
              .
            </>
          }
          seed={seed}
          name="termsConditions"
          type="checkbox"
          custom
          validators={[validateRequired()]}
        >
          <FormFeedbackAdapter name="termsConditions" />
        </FFormGroup>
      </FormGroup>
      <SubmitErrorAlerts />
      <Button
        block
        color="primary"
        className="uaTrack-submit-register"
        disabled={submitting || submitSucceeded}
        size="lg"
        type="submit"
      >
        {submitting || submitSucceeded ? 'Signing Up' : 'Sign Up'}
      </Button>
    </Form>
  )
}

export default RegisterForm
