import React, { useCallback } from 'react'
import { func } from 'prop-types'
import { Form } from 'react-final-form'
import { connect } from 'react-redux'
import { NextAuth } from 'next-auth/client'
import createDecorator from 'final-form-focus'
import { Router } from '../../routes'
import RegisterForm from './RegisterForm'
import { mapFinalFormErrors } from '../../lib/ff'
import { signUp } from '../../modules/users'

const mapDispatch = { signUp }

const Register = connect(
  null,
  mapDispatch
)(RegisterFormController)

RegisterFormController.propTypes = {
  signUp: func.isRequired,
}

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors('Account creation failed.')

function RegisterFormController({ signUp }) {
  const onSubmit = useCallback(
    async function onRegisterFormSubmit(values) {
      try {
        await signUp(values)
        await NextAuth.signin(values)
        await Router.pushRoute('auth-callback')
        return undefined
      } catch (error) {
        return mapErrors(error)
      }
    },
    [signUp]
  )

  return (
    <Form
      component={RegisterForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      subscription={{
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default Register
