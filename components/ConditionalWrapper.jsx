import { func, node } from 'prop-types'

ConditionalWrapper.propTypes = {
  children: node.isRequired,
  wrapFunc: func,
}

ConditionalWrapper.defaultProps = {
  wrapFunc: undefined,
}

/**
 * Component that conditionally wraps children when a wrapper function is present.
 *
 * wrapFunc is a function that takes children as its argument and returns with markup of
 * your choosing around it.
 *
 * Example:
 *
 * <ConditionalWrapper
 *   wrapFunc={children => <div className="test">{children}</div>}
 * >
 *   <SomeComponent/>
 * </ConditionalWrapper>
 *
 * This would output:
 *
 * <div className="test"><SomeComponent/></div>
 */
function ConditionalWrapper({ children, wrapFunc }) {
  return wrapFunc ? wrapFunc(children) : children
}

export default ConditionalWrapper
