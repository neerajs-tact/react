import React from 'react'
import { node, objectOf, string } from 'prop-types'
import { useRouter } from 'next/router'
import { NavLink } from 'reactstrap'
import { Link } from '../routes'
import { getRouteNameFromAsPath } from '../lib/utils'

/**
 * <NavLink> Adapter for next-route
 *
 * Use instead of <NavLink> to add Bootstrap's active route state.
 *
 * This component will pass className to the link so you can use uaTrack- classes.
 */

LinkActiveNavLink.propTypes = {
  children: node.isRequired,
  params: objectOf(string),
  route: string.isRequired,
}

LinkActiveNavLink.defaultProps = {
  params: {},
}

function LinkActiveNavLink({ children, params, route, ...rest }) {
  const { asPath } = useRouter()
  const routeName = getRouteNameFromAsPath(asPath)
  const isActiveRoute = routeName === route

  return (
    <Link route={route} params={params} passHref>
      <NavLink active={isActiveRoute} {...rest}>
        {children}
      </NavLink>
    </Link>
  )
}

export default LinkActiveNavLink
