import React, { useCallback } from 'react'
import { func, string } from 'prop-types'
import { connect } from 'react-redux'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import { mapFinalFormErrors } from '../../lib/ff'
import { Router } from '../../routes'
import { changePassword } from '../../modules/users'
import ResetPasswordForm from './ResetPasswordForm'

const mapDispatch = { changePassword }

const ResetPassword = connect(
  null,
  mapDispatch
)(ResetPasswordFormController)

ResetPassword.propTypes = {
  resetToken: string.isRequired,
}

ResetPasswordFormController.propTypes = {
  changePassword: func.isRequired,
  resetToken: string.isRequired,
}

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors('Failed to update password.')

function ResetPasswordFormController({ changePassword, resetToken }) {
  const onSubmit = useCallback(
    async function onResetPasswordFormSubmit(values) {
      try {
        const user = {
          ...values,
          reset_password_token: resetToken,
        }
        await changePassword(user)
        Router.pushRoute('login', {
          alertContext: 'success',
          alertMsg:
            'Your password is updated. Please log in below with your new password.',
        })
        return undefined
      } catch (error) {
        return mapErrors(error)
      }
    },
    [changePassword, resetToken]
  )

  return (
    <Form
      component={ResetPasswordForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      subscription={{
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default ResetPassword
