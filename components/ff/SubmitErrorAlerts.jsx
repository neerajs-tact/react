import React from 'react'
import { string } from 'prop-types'
import { map, isString } from 'lodash'
import { Alert } from 'reactstrap'
import { useFormState } from 'react-final-form'
import { useUIDSeed } from 'react-uid'

/**
 * Produces error alerts from Final-Form submitError form state
 *
 * @see https://github.com/final-form/final-form#submiterror-any-1
 * @see https://github.com/final-form/react-final-form#submission-errors
 */

SubmitErrorAlerts.propTypes = {
  className: string,
}

SubmitErrorAlerts.defaultProps = {
  className: undefined,
}

function SubmitErrorAlerts({ className }) {
  const seed = useUIDSeed()
  const { submitError, submitting } = useFormState({
    subscription: { submitError: true, submitting: true },
  })
  if (!submitError || submitting) return null

  return isString(submitError) ? (
    // single error alert
    <Alert className={className} color="danger">
      {submitError}
    </Alert>
  ) : (
    // multiple error alerts
    map(submitError, errorMsg => (
      <Alert
        className={className}
        key={`SubmitError-${seed(errorMsg)}`}
        color="danger"
      >
        {errorMsg}
      </Alert>
    ))
  )
}

export default SubmitErrorAlerts
