import React from 'react'
import {
  arrayOf,
  bool,
  func,
  node,
  number,
  object,
  oneOfType,
  shape,
  string,
} from 'prop-types'
import { Input, CustomInput } from 'reactstrap'
import classNames from 'classnames'

/**
 * Final-Form to Reactstrap <Input> and <InputSelect> component adapter for selects
 *
 * ^ Use for select drop-downs and select[multiple], all others use <InputAdapter>.
 *
 * Add the 'custom' prop to use the custom input type.
 *
 * ## Synchronous Field-Level Validation
 * Success validation highlighting is off by default.
 * Invalid validation behavior requires touch (gained and lost focus) by default.
 * Provide your own success/invalid functions into the prop to override.
 *
 * ## Option Groups
 * Option groups must use key "groupOptions" in the following format.
 *
 * const groupedOptions = [
 *  {
 *    label: 'Group A',
 *    groupOptions: [
 *      {
 *        label: 'A One',
 *        value: 'a1'
 *      },
 *      {
 *        label: 'A Two',
 *        value: 'a2'
 *      }
 *    ]
 *  },
 *  {
 *    label: 'Group B',
 *    groupOptions: [
 *      {
 *        label: 'B One',
 *        value: 'b1'
 *      },
 *      {
 *        label: 'B Two',
 *        value: 'b2'
 *      }
 *    ]
 *  },
 * ]
 *
 * @see Implementation Examples https://github.com/Soundvessel/final-form-reactstrap
 * @see https://github.com/final-form/react-final-form#third-party-components
 * @see https://reactstrap.github.io/components/form/
 */

SelectAdapter.propTypes = {
  className: string,
  custom: bool,
  multiSelect: bool, // allows selection of multiple options
  options: arrayOf(
    shape({
      label: node.isRequired,
      value: oneOfType([number, string]),
      contextColor: string,
      groupOptions: arrayOf(
        shape({
          label: node.isRequired,
          value: oneOfType([number, string]).isRequired,
          contextColor: string,
        })
      ),
    })
  ).isRequired,
  selectablePlaceholder: bool,
  placeholder: string,
  invalid: func,
  valid: func,
  input: object.isRequired, // eslint-disable-line react/forbid-prop-types
  meta: object.isRequired, // eslint-disable-line react/forbid-prop-types
}

SelectAdapter.defaultProps = {
  className: undefined,
  custom: false,
  multiSelect: false,
  selectablePlaceholder: false,
  placeholder: 'Select...',
  invalid: meta => meta.touched && meta.invalid,
  valid: () => false,
}

function SelectAdapter({
  input,
  className,
  custom,
  multiSelect,
  options,
  meta,
  placeholder,
  invalid,
  selectablePlaceholder,
  valid,
  ...inputAttributes
}) {
  // use appropriate Reactstrap component
  const SelectElement = custom ? CustomInput : Input
  return (
    <SelectElement
      {...input}
      {...inputAttributes}
      invalid={invalid(meta)}
      valid={valid(meta)}
      // className to style placeholder when not multiple
      className={classNames(className, {
        'text-placeholder': !input.value && input.value !== 0 && !multiSelect,
      })}
      multiple={multiSelect}
      // empty multiple must be empty array for final-form
      value={!input.value && multiSelect ? [] : input.value}
    >
      {placeholder && (
        <option
          className="text-placeholder"
          disabled={!selectablePlaceholder}
          value=""
        >
          {placeholder}
        </option>
      )}
      {/* Generate options once available */}
      {options.length &&
      Object.prototype.hasOwnProperty.call(options[0], 'groupOptions')
        ? /* labeled groups of options */
          options.map(group => (
            <optgroup label={group.label} key={group.label}>
              {group.groupOptions.map(option => (
                <option
                  className={
                    option.contextColor
                      ? `text-${option.contextColor}`
                      : undefined
                  }
                  key={`${group.label}-${option.value}`}
                  value={option.value}
                >
                  {option.label}
                </option>
              ))}
            </optgroup>
          ))
        : /* options */
          options.map(option => (
            <option
              className={
                option.contextColor ? `text-${option.contextColor}` : undefined
              }
              key={option.value}
              value={option.value}
            >
              {option.label}
            </option>
          ))}
    </SelectElement>
  )
}

export default SelectAdapter
