import React from 'react'
import { func, object, string } from 'prop-types'
import { useField } from 'react-final-form'
import { FormFeedback } from 'reactstrap'

/**
 * Error message display for fields.
 *
 * @see https://github.com/final-form/react-final-form#independent-error-component
 */

FormFeedbackAdapter.propTypes = {
  errorMsgPriority: func,
  invalid: func,
  name: string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  subscription: object,
}

FormFeedbackAdapter.defaultProps = {
  errorMsgPriority: meta => meta.error || meta.submitError,
  // Match default field error highlight condition
  invalid: meta =>
    meta.touched && meta.invalid && (meta.error || meta.submitError),
  subscription: {
    error: true,
    invalid: true,
    submitError: true,
    touched: true,
  },
}

/**
 * Adapts Final-Form to Reactstrap Bootstrap 4 error component
 *
 * If you change the behavior of the error display (invalid func) be sure to update the subscription
 * prop if different field states are used. See Password field in /_styleguide for an example of
 * this type of override.
 *
 * @param errorMsgPriority - Determines field error message priority (client vs submit).
 * @param invalid          - Determines field state based condition to show error message.
 * @param name             - Name of field this error belongs to.
 * @param subscription     - Object of FF field states used for errorMsg and invalid functions.
 * @returns {node|null}    - Reactstrap Bootstrap 4 error component.
 */
function FormFeedbackAdapter({
  errorMsgPriority,
  invalid,
  name,
  subscription,
}) {
  const { meta } = useField(name, { subscription })
  return invalid(meta) ? (
    <FormFeedback className="d-block">{errorMsgPriority(meta)}</FormFeedback>
  ) : null
}

export default FormFeedbackAdapter
