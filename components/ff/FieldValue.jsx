import { string } from 'prop-types'
import { useField } from 'react-final-form'

FieldValue.propTypes = {
  name: string.isRequired,
  noValuePlaceholder: string, // when no value is available, you might want to set initialValue instead
}

FieldValue.defaultProps = {
  noValuePlaceholder: 'N/A',
}

/**
 * Outputs value of given Final-Form Field name. Must be used within Final-Form <Form/>
 *
 * Note that in the case of multi-select/checkboxes you'll get VALUES not labels.
 *
 * @return {string}
 */
function FieldValue({ name, noValuePlaceholder }) {
  const {
    input: { value },
  } = useField(name, { subscription: { value: true } })
  if (!value) return noValuePlaceholder
  if (Array.isArray(value)) return value.join(', ') // multi-select/checkbox values (not labels!)
  return value.toString() // toString prevents false when 0
}

export default FieldValue
