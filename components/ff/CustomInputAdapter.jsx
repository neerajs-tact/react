import React from 'react'
import { func, node, object, oneOf, shape } from 'prop-types'
import { CustomInput } from 'reactstrap'

/**
 * Final-Form to Reactstrap custom component adapter
 *
 * For select drop-downs and select[multiple], use <SelectAdapter>.
 *
 * ## Synchronous Field-Level Validation
 * Success validation highlighting is off by default.
 * Invalid validation behavior requires touch (gained and lost focus) by default.
 * Provide your own success/invalid functions into the prop to override.
 *
 * @see /components/styleguide/FormElementsForm.jsx
 * @see https://github.com/final-form/react-final-form#third-party-components
 * @see https://reactstrap.github.io/components/form/
 *
 * Feedback elements for vertical stack of radios/checkboxes/switches needs to passed into
 * last field as children.
 * @see https://github.com/reactstrap/reactstrap/issues/1242#issuecomment-426804926
 */

CustomInputAdapter.propTypes = {
  children: node,
  customType: oneOf(['switch']), // because parent <Field/> needs type="checkbox" to work properly
  input: shape({
    type: oneOf(['checkbox', 'file', 'radio', 'range']).isRequired,
  }).isRequired,
  invalid: func,
  label: node,
  meta: object.isRequired, // eslint-disable-line react/forbid-prop-types
  valid: func,
}

CustomInputAdapter.defaultProps = {
  children: null,
  customType: null,
  invalid: meta => meta.touched && meta.invalid,
  label: '',
  valid: () => false,
}

function CustomInputAdapter({
  children,
  customType,
  input,
  label,
  meta,
  invalid,
  valid,
  ...inputAttributes
}) {
  // peel off type for custom switch detection
  const { type, ...inputRest } = input
  return (
    <CustomInput
      {...inputRest}
      {...inputAttributes}
      type={customType === 'switch' ? customType : type}
      label={label}
      invalid={invalid(meta)}
      valid={valid(meta)}
    >
      {children}
    </CustomInput>
  )
}

export default CustomInputAdapter
