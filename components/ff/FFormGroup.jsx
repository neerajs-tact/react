import React, { useMemo } from 'react'
import {
  arrayOf,
  bool,
  func,
  node,
  number,
  object,
  oneOf,
  oneOfType,
  shape,
  string,
} from 'prop-types'
import { FormGroup, FormText, Label } from 'reactstrap'
import { Field } from 'react-final-form'
import { composeValidators } from '../../lib/ff'
import ConditionalWrapper from '../ConditionalWrapper'
import InputAdapter from './InputAdapter'
import CustomInputAdapter from './CustomInputAdapter'
import SelectAdapter from './SelectAdapter'
import FormFeedbackAdapter from './FormFeedbackAdapter'

FFormGroup.propTypes = {
  // FormGroup
  formGroupClassNames: string,

  // label
  hiddenLabel: bool,
  label: node.isRequired,
  labelClassNames: string,
  customLabel: string,

  // field/input
  bsSize: string,
  fieldWrap: func,
  name: string.isRequired,
  seed: func.isRequired,

  // type & layout
  custom: bool,
  inline: bool,
  multiSelect: bool,
  type: oneOf([
    // supported prop options noted next to each
    'text',
    'email',
    'password',
    'url',
    'number',
    'tel',
    'datetime',
    'date',
    'time',
    'color',
    'search',
    'select', // custom, multiSelect
    'textarea',
    'checkbox', // custom, inline, options
    'radio', // custom, inline, options
    'file', // custom, customLabel if custom is used
    'range', // custom
    'switch', // custom (must include), inline, options
  ]).isRequired,

  // data for select or multiple radios/checkboxes/switches
  options: arrayOf(
    shape({
      label: node.isRequired,
      value: oneOfType([number, string]),
      groupOptions: arrayOf(
        shape({
          label: node.isRequired,
          value: oneOfType([number, string]).isRequired,
        })
      ),
    })
  ),
  value: string,

  // assistance/UX
  autoComplete: string,
  formText: node,
  placeholder: string,

  // validation and error highlighting/messages
  errorMsgPriority: func,
  // eslint-disable-next-line react/forbid-prop-types,no-undef
  errorSubscription: object,
  invalid: func,
  valid: func,
  validators: arrayOf(func),
}

FFormGroup.defaultProps = {
  // FormGroup
  formGroupClassNames: undefined,

  // label
  hiddenLabel: false,
  customLabel: undefined,
  labelClassNames: undefined,

  // field/input
  bsSize: undefined,
  fieldWrap: undefined,

  // type & layout
  custom: false,
  inline: false,
  multiSelect: undefined,

  // data for select or multiple radios/checkboxes/switches
  options: undefined,
  value: undefined,

  // assistance/UX
  autoComplete: undefined,
  formText: undefined,
  placeholder: undefined,

  // validation and error highlighting/messages
  errorMsgPriority: undefined,
  errorSubscription: undefined,
  invalid: undefined,
  valid: undefined,
  validators: [],
}

/**
 * Final-Form / ReactStrap FormGroup Adapter
 *
 * @see above for param types and defaults
 * @see below for descriptions of what params are/do
 * @see implementation /components/styleguide/FormGroupElementsForm.jsx
 * @see what output we adapt to match https://reactstrap.github.io/components/form/
 * @see if you need to do something more custom /components/styleguide/FormElementsForm.jsx
 *      uses the smaller components.
 *
 * // FormGroup
 * @param formGroupClassNames - Optional CSS classes for <FormGroup/> ReactStrap component.
 *
 * // Label
 * @param hiddenLabel         - Optional bool to hide the label, UX-wise not recommended.
 * @param label               - Content for <Label/>, can be string or JSX. Required.
 * @param labelClassNames     - Optional CSS classes for <Label/>
 * @param customLabel         - Custom file field text to replace "No file chosen".
 *
 * // field/input
 * @param bsSize              - Use 'sm' or 'lg' to change input size categories.
 * @param fieldWrap           - Optional function to wrap input. See "Disabled" field example in FormGroupElementsForm.jsx
 * @param name                - Name of field, used by FF to store value(s).
 * @param seed                - Const from react-uid useUIDSeed(), ensures unique for/id pairing.
 *
 * // type and layout
 * @param custom              - Enables custom version of input if available.
 * @param inline              - Optional layout for multiple radios, checkboxes, and switches.
 * @param multiSelect         - Select option that allows the selection of multiple values. UX-wise checkboxes might be a better UI.
 * @param type                - Field type.
 *
 * // data for select or multiple radios/checkboxes/switches
 * @param options             - Used for select and multiple radios, checkboxes, and switches. Array of objects with value and label properties.
 * @param value               - Value for radio/checkbox/switch
 *
 * // assistance/UX
 * @param autoComplete        - https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete
 * @param formText            - Helper text shown below field or radio/check/switch group (via options). Can be string or JSX.
 * @param placeholder         - Placeholder attribute. UX-wise recommended as hint, not label due to loss of context when input is filled.
 *
 * // validation and error highlighting/messages
 * @param errorMsgPriority    - Optional function to override which error message takes precedence. @see FormFeedbackAdapter.jsx
 * @param errorSubscription   - Override for <FormFeedbackAdapter/> subscription for when you override invalid. @see FormFeedbackAdapter.jsx
 * @param invalid             - Override of field highlight and error message visibility condition. @see password field in styleguide for example.
 * @param valid               - Override of valid highlighting of field condition.
 * @param validators          - Array of validators. Compose is automatically applied when there is more than one. Order of validators matter.
 *
 * // the rest, HTML attributes, etc
 * @param fieldProps          - Any additional props/attributes are passed to the field. These are usually HTML attributes or Reactstrap props.
 *
 * @returns {*}               - Reactstrap <FormGroup/> using Final-Form <Field/> and Bootstrap/Reactstrap UI
 */
function FFormGroup({
  // FormGroup
  formGroupClassNames,
  // label
  hiddenLabel,
  label,
  labelClassNames,
  customLabel,
  // field/input
  bsSize,
  fieldWrap,
  name,
  seed,
  // type & layout
  custom,
  inline,
  multiSelect,
  type,
  // data
  options,
  value,
  // assistance
  autoComplete,
  formText,
  placeholder,
  // validation
  errorMsgPriority,
  errorSubscription,
  invalid,
  valid,
  validators,
  // additional field props
  ...fieldProps
}) {
  // determine correct FF adapter to use for field
  const component = useMemo(() => pickFieldAdapter(type, custom), [
    type,
    custom,
  ])

  //
  // logic that determines component layout
  //

  // input is a checkbox, radio, or switch?
  const isCheckRadioSwitch =
    type === 'checkbox' || type === 'radio' || type === 'switch'

  // input is a custom checkbox, radio or switch?
  const isCustomCheckRadioSwitch = isCheckRadioSwitch && custom

  // input is a native checkbox or radio?
  const isCheckRadioNative =
    (type === 'checkbox' || type === 'radio') && !custom

  // options to build multiple checkboxes, radios, or switches?
  const isMultiCheckRadioSwitch = isCheckRadioSwitch && options

  //
  // setup props for sub-components
  //

  const ffLabelProps = {
    hiddenLabel,
    isCheckRadioNative,
    label,
    labelClassNames,
    name,
    seed,
  }

  const ffErrorProps = {
    errorMsgPriority,
    invalid,
    name,
    errorSubscription,
  }

  const ffFieldProps = {
    autoComplete,
    bsSize,
    component,
    custom,
    customLabel,
    inline,
    invalid,
    isCustomCheckRadioSwitch,
    label,
    multiSelect,
    name,
    options,
    placeholder,
    seed,
    type,
    valid,
    validators,
    value,
    ...fieldProps,
  }

  // multiple checkbox, radio, or switch from options form group structure
  if (isMultiCheckRadioSwitch) {
    return (
      <FormGroup tag="fieldset">
        <legend className="legend-label">{label}</legend>
        <ConditionalWrapper
          {...isCustomCheckRadioSwitch && {
            wrapFunc: inside => <div>{inside}</div>,
          }}
        >
          {options.map((option, i) => {
            // peel off props we don't need passed
            const { options, validators, ...ffFieldPropsRest } = ffFieldProps
            // adjust see function to concat with value
            const optionSeed = name => seed(`${name}-${option.value}`)
            // logic for component structure
            const isLast = i === options.length - 1

            //
            // setup props for option sub-components
            //

            const ffFieldOptionProps = {
              ...ffFieldPropsRest,
              value: option.value,
              label: option.label,
              // validators should only be added to first of series
              ...(validators && i === 0 && { validators }),
              // update seed function to include value to keep unique
              seed: optionSeed,
              // additional properties from options, add as needed instead of spreading ...option
              disabled: !!option.disabled,
            }

            const ffLabelOptionProps = {
              ...ffLabelProps,
              seed: optionSeed,
              label: option.label,
            }

            return (
              <ConditionalWrapper
                key={optionSeed(name)}
                {...!isCustomCheckRadioSwitch && {
                  wrapFunc: inside => (
                    <FormGroup check {...inline && { inline }}>
                      {inside}
                    </FormGroup>
                  ),
                }}
              >
                <FFField {...ffFieldOptionProps}>
                  {isCustomCheckRadioSwitch ? (
                    <>
                      {formText && isLast && <FormText>{formText}</FormText>}
                      {isLast && !inline && <FFError {...ffErrorProps} />}
                    </>
                  ) : null}
                </FFField>
                {isCheckRadioNative && <FFLabel {...ffLabelOptionProps} />}
                {isLast && formText && !isCustomCheckRadioSwitch && (
                  <FormText>{formText}</FormText>
                )}
                {isLast && !isCustomCheckRadioSwitch && !inline && (
                  <FFError {...ffErrorProps} />
                )}
              </ConditionalWrapper>
            )
          })}
          {inline && <FFError {...ffErrorProps} />}
        </ConditionalWrapper>
      </FormGroup>
    )
  }

  // single field form group structure
  return (
    <FormGroup
      className={formGroupClassNames}
      {...isCheckRadioNative && { check: true }}
    >
      <>
        {isCheckRadioSwitch || <FFLabel {...ffLabelProps} />}
        <ConditionalWrapper wrapFunc={fieldWrap}>
          <FFField {...ffFieldProps}>
            {isCustomCheckRadioSwitch ? (
              <>
                {formText && <FormText>{formText}</FormText>}
                <FFError {...ffErrorProps} />
              </>
            ) : null}
          </FFField>
        </ConditionalWrapper>
        {isCheckRadioNative && <FFLabel {...ffLabelProps} />}
        {formText && !isCustomCheckRadioSwitch && (
          <FormText>{formText}</FormText>
        )}
        {isCustomCheckRadioSwitch || <FFError {...ffErrorProps} />}
      </>
    </FormGroup>
  )
}

/**
 * Return correct Final-Form/Reactstrap Adapter given field type and custom flag.
 *
 * @param {string} type      - Type of field element.
 * @param {boolean} custom   - True if field type should use available custom version.
 * @returns {function(*): *} - Component adapter function.
 */
function pickFieldAdapter(type, custom) {
  switch (true) {
    case type === 'select':
      return SelectAdapter
    case ['checkbox', 'radio', 'switch', 'file', 'range'].includes(type) &&
      custom:
      return CustomInputAdapter
    default:
      return InputAdapter
  }
}

// FFormGroup Sub-Components

function FFField({
  /* eslint-disable react/prop-types */
  autoComplete,
  bsSize,
  children,
  component,
  custom,
  customLabel,
  inline,
  invalid,
  isCustomCheckRadioSwitch,
  label,
  multiSelect,
  name,
  options,
  placeholder,
  seed,
  type,
  value,
  valid,
  validators,
  ...fieldProps
  /* eslint-enable react/prop-types */
}) {
  return (
    <Field
      autoComplete={autoComplete}
      bsSize={bsSize}
      component={component}
      id={seed(name)}
      name={name}
      placeholder={placeholder}
      // FF needs checkbox type to work properly with Reactstrap Switch UI
      type={type === 'switch' ? 'checkbox' : type}
      {...type === 'switch' && { customType: 'switch' }}
      {...type === 'select' && multiSelect && { multiSelect: true }}
      {...isCustomCheckRadioSwitch && { label }}
      {...customLabel && { label: customLabel }}
      {...type === 'select' && { custom }}
      {...inline && custom && { inline }}
      // data
      {...options && { options }}
      {...value && { value }}
      // validation
      invalid={invalid}
      valid={valid}
      {...validators && {
        validate: composeValidators(...validators),
      }}
      // rest of props passed to <Field/> which passes anything extra to HTML input such as native attributes
      {...fieldProps}
    >
      {children}
    </Field>
  )
}

function FFLabel({
  hiddenLabel, // eslint-disable-line react/prop-types
  isCheckRadioNative, // eslint-disable-line react/prop-types
  label, // eslint-disable-line react/prop-types
  labelClassNames, // eslint-disable-line react/prop-types
  name, // eslint-disable-line react/prop-types
  seed, // eslint-disable-line react/prop-types
}) {
  return (
    <Label
      htmlFor={seed(name)}
      hidden={hiddenLabel}
      {...labelClassNames && { className: labelClassNames }}
      {...isCheckRadioNative && { check: true }}
    >
      {label}
    </Label>
  )
}

// eslint-disable-next-line react/prop-types
function FFError({ errorMsgPriority, errorSubscription, invalid, name }) {
  return (
    <FormFeedbackAdapter
      errorMsgPriority={errorMsgPriority}
      invalid={invalid}
      name={name}
      subscription={errorSubscription}
    />
  )
}

export default FFormGroup
