import React from 'react'
import { func, object } from 'prop-types'
import { Input } from 'reactstrap'

/**
 * Final-Form to Reactstrap <Input> component adapter ^
 *
 * ^ For select drop-downs and select[multiple], use <SelectAdapter>.
 *
 * ## Synchronous Field-Level Validation
 * Success validation highlighting is off by default.
 * Invalid validation behavior requires touch (gained and lost focus) by default.
 * Provide your own success/invalid functions into the prop to override.
 *
 * @see Implementation https://github.com/Soundvessel/final-form-reactstrap
 * @see https://github.com/final-form/react-final-form#third-party-components
 * @see https://reactstrap.github.io/components/form/
 */

InputAdapter.propTypes = {
  invalid: func,
  valid: func,
  input: object.isRequired, // eslint-disable-line react/forbid-prop-types
  meta: object.isRequired, // eslint-disable-line react/forbid-prop-types
}

InputAdapter.defaultProps = {
  invalid: meta => meta.touched && meta.invalid,
  valid: () => false,
}

function InputAdapter({ input, meta, invalid, valid, ...inputAttributes }) {
  return (
    <Input
      {...input}
      {...inputAttributes}
      invalid={invalid(meta)}
      valid={valid(meta)}
    />
  )
}

export default InputAdapter
