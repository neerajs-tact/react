import React from 'react'
import { useFormState } from 'react-final-form'

/*
  Final-Form Tool - place in your form to see live output of stored values
 */
function FormValuesTool() {
  const { values } = useFormState({ subscription: { values: true } })

  return (
    <pre className="border mb-0 p-20">{JSON.stringify(values, null, 2)}</pre>
  )
}

export default FormValuesTool
