import React from 'react'
import { arrayOf, func, number, oneOfType, shape, string } from 'prop-types'
import classNames from 'classnames'
import { useField } from 'react-final-form'
// eslint-disable-next-line import/no-extraneous-dependencies
import scss from 'styled-jsx/css'
import { ContextDotIcon } from '../icons'
import { getSelectOptionViaValue } from '../../lib/ff'

// HoC for field, can be used in fieldWrap prop of <FFormGroup type="select"/>
function fieldWrapSelectContextDot(
  fieldName,
  options,
  valueConvertFunc,
  fieldWrapClassName
) {
  const classes = classNames(
    'position-relative',
    fieldWrapClassName && fieldWrapClassName
  )
  return field => (
    // ContextDotIcon absolute positioning attachment point
    <div className={classes}>
      <ContextOverlay
        fieldName={fieldName}
        options={options}
        valueConvertFunc={valueConvertFunc}
      />
      {field}
    </div>
  )
}

ContextOverlay.propTypes = {
  fieldName: string.isRequired,
  options: arrayOf(
    shape({
      label: string.isRequired,
      value: oneOfType([number, string]).isRequired,
      contextColor: string,
    })
  ).isRequired,
  valueConvertFunc: func,
}

ContextOverlay.defaultProps = {
  valueConvertFunc: undefined,
}

/* language=SCSS */
const globalStyles = scss.global`
  @import "jsx-helper";
  
  .context-select-overlay + select {
    padding-left: rems(50); // space for context dot when active
  }
`

/**
 * Checks if selected value has contextColor to display color dot icon
 *
 * @return {node|null}
 */
function ContextOverlay({ fieldName, options, valueConvertFunc }) {
  const {
    input: { value },
  } = useField(fieldName, { subscription: { value: true } })

  if (value === undefined || value === '') return null

  const { label, contextColor } =
    getSelectOptionViaValue(value, options, valueConvertFunc) || {}

  if (!contextColor) return null

  // positioning calculated globally with 'status-select-overlay' in styles/bootstrap-extended/components/_forms.scss

  return (
    <div className="context-select-overlay d-flex align-items-center h-100 pointer-events-none position-absolute pl-20">
      <ContextDotIcon
        title={label}
        className={`text-${contextColor}`}
        ariaHidden
      />
      <style jsx global>
        {globalStyles}
      </style>
    </div>
  )
}

export default fieldWrapSelectContextDot
