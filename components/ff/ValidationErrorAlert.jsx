import React from 'react'
import { string } from 'prop-types'
import { Alert } from 'reactstrap'
import { useFormState } from 'react-final-form'

/**
 * Produces error alert when Final-form has field-level validation errors on submission attempt.
 *
 * Useful with forms where the user is scrolled away from highlighted fields. Likely not needed
 * if form is using final-form-focus.
 */

ValidationErrorAlert.propTypes = {
  msg: string,
}

ValidationErrorAlert.defaultProps = {
  msg: 'Please fix the highlighted errors above.',
}

function ValidationErrorAlert({ msg }) {
  const { hasValidationErrors, submitFailed, submitting } = useFormState({
    subscription: {
      hasValidationErrors: true,
      submitFailed: true,
      submitting: true,
    },
  })
  return (
    <Alert
      isOpen={hasValidationErrors && submitFailed && !submitting}
      color="danger"
    >
      {msg}
    </Alert>
  )
}

export default ValidationErrorAlert
