import React from 'react'
import { bool, func } from 'prop-types'
import { Button, Form } from 'reactstrap'
import { useUIDSeed } from 'react-uid'
import {
  invalidDuringInput,
  subscriptionInvalidDuringInput,
  validateMatch,
  validateMinLength,
  validateRequired,
} from '../../lib/ff'
import { FFormGroup, SubmitErrorAlerts } from '../ff'

AcceptInvitationForm.propTypes = {
  handleSubmit: func.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function AcceptInvitationForm({ handleSubmit, submitSucceeded, submitting }) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <FFormGroup
        label="Password"
        name="password"
        type="password"
        seed={seed}
        validators={[
          validateRequired(),
          validateMinLength(6, 'Use at least 6 characters.'),
        ]}
        autoComplete="new-password"
      />
      <FFormGroup
        label="Confirm Password"
        name="passwordConfirmation"
        type="password"
        seed={seed}
        validators={[
          validateRequired(),
          validateMatch('password', 'Password must match.'),
        ]}
        // validate during user input override
        invalid={invalidDuringInput}
        // error message subscription for new invalid function field states
        errorSubscription={subscriptionInvalidDuringInput}
        autoComplete="new-password"
      />
      <SubmitErrorAlerts />
      <Button
        block
        color="primary"
        className="uaTrack-submit-accept-invitation"
        disabled={submitting || submitSucceeded}
        size="lg"
        type="submit"
      >
        {submitting || submitSucceeded
          ? 'Creating Password...'
          : 'Create Password'}
      </Button>
    </Form>
  )
}

export default AcceptInvitationForm
