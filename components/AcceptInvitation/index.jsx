import React, { useCallback } from 'react'
import { func, string } from 'prop-types'
import { FORM_ERROR } from 'final-form'
import { connect } from 'react-redux'
import { Form } from 'react-final-form'
import { Router } from '../../routes'
import { mapFinalFormErrors } from '../../lib/ff'
import { acceptInvitation } from '../../modules/users'
import AcceptInvitationForm from './AcceptInvitationForm'

const mapDispatch = { acceptInvitation }

const AcceptInvitation = connect(
  null,
  mapDispatch
)(AcceptInvitationFormController)

AcceptInvitation.propTypes = {
  invitationToken: string.isRequired,
}

AcceptInvitationFormController.propTypes = {
  acceptInvitation: func.isRequired,
  invitationToken: string.isRequired,
}

const mapErrors = mapFinalFormErrors('Failed to create password.')

function AcceptInvitationFormController({ acceptInvitation, invitationToken }) {
  const onSubmit = useCallback(
    async function onAcceptInvitationFormSubmit(values) {
      try {
        await acceptInvitation({ invitationToken, ...values })
        Router.pushRoute('login', {
          alertContext: 'success',
          alertMsg: 'Your password and account are set. Please log in below.',
        })
        return undefined
      } catch (error) {
        const formErrors = mapErrors(error)
        if (formErrors.invitationToken) {
          // Use invitation token error as main error if it exists
          formErrors[FORM_ERROR] = formErrors.invitationToken
        }
        return formErrors
      }
    },
    [acceptInvitation, invitationToken]
  )

  return (
    <Form
      component={AcceptInvitationForm}
      onSubmit={onSubmit}
      // restricts which parts of form state the form component needs to render
      subscription={{
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default AcceptInvitation
