import React from 'react'
import { bool, func } from 'prop-types'
import { Alert, Button, Form } from 'reactstrap'
import { useUIDSeed } from 'react-uid'
import {
  invalidDuringInput,
  subscriptionInvalidDuringInput,
  validateMatch,
  validateMinLength,
  validateRequired,
} from '../../lib/ff'
import { FFormGroup, SubmitErrorAlerts } from '../ff'

ChangePasswordForm.propTypes = {
  handleSubmit: func.isRequired,
  pristine: bool.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function ChangePasswordForm({
  handleSubmit,
  pristine,
  submitSucceeded,
  submitting,
}) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <FFormGroup
        label="Current Password"
        name="currentPassword"
        type="password"
        seed={seed}
        validators={[validateRequired()]}
        autoComplete="current-password"
      />
      <FFormGroup
        label="Password"
        name="password"
        type="password"
        seed={seed}
        validators={[
          validateRequired(),
          validateMinLength(6, 'Use at least 6 characters.'),
        ]}
        autoComplete="new-password"
      />
      <FFormGroup
        label="Confirm Password"
        name="passwordConfirmation"
        type="password"
        seed={seed}
        validators={[
          validateRequired(),
          validateMatch('password', 'Password must match.'),
        ]}
        // validate during user input override
        invalid={invalidDuringInput}
        // error message subscription for new invalid function field states
        errorSubscription={subscriptionInvalidDuringInput}
        autoComplete="new-password"
      />
      <SubmitErrorAlerts />
      <Alert color="success" isOpen={submitSucceeded && pristine}>
        Your password has been successfully updated.
      </Alert>
      <Button
        block
        color="primary"
        className="uaTrack-submit-change-password"
        disabled={submitting}
        size="lg"
        type="submit"
      >
        {submitting ? 'Updating Password...' : 'Change Password'}
      </Button>
    </Form>
  )
}
export default ChangePasswordForm
