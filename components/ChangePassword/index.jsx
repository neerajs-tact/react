import React, { useCallback } from 'react'
import { func } from 'prop-types'
import { connect } from 'react-redux'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import { NextAuth } from 'next-auth/client'
import { updatePassword } from '../../modules/users'
import { mapFinalFormErrors } from '../../lib/ff'
import ChangePasswordForm from './ChangePasswordForm'
import { Router } from '../../routes'

const mapDispatch = { updatePassword }

const ChangePassword = connect(
  null,
  mapDispatch
)(ChangePasswordFormController)

ChangePasswordFormController.propTypes = {
  updatePassword: func.isRequired,
}

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors('Failed to change password.')

function ChangePasswordFormController({ updatePassword }) {
  const onSubmit = useCallback(
    async function onForgotPasswordFormSubmit(values, form) {
      try {
        await updatePassword(values)
        await NextAuth.signout()
        Router.pushRoute('auth-callback', {
          alertContext: 'success',
          alertMsg: 'Your new password is set. Please log in below.',
        })
        // clears focus if submit via enter
        document.activeElement.blur()
        // re-initialize form AFTER submitSuccess to prevent focus on first field
        setTimeout(() => form.initialize({}), 20) // resets form without losing submitSucceeded state
        return undefined
      } catch (error) {
        return mapErrors(error)
      }
    },
    [updatePassword]
  )
  return (
    <Form
      component={ChangePasswordForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      // restricts which parts of form state the form component needs to render
      subscription={{
        pristine: true,
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default ChangePassword
