import React from 'react'
// eslint-disable-next-line import/no-extraneous-dependencies
import scss from 'styled-jsx/css'
import {
  FacebookIcon,
  InstagramIcon,
  LinkedInIcon,
  MediumIcon,
  PinterestIcon,
  TwitterIcon,
  YouTubeIcon,
} from '../icons'

const bsUtilityClasses = 'w-100 h-auto d-block'

/* language=SCSS */
const styles = scss`

  @import "jsx-helper";

  // social link config

  $social-link-size: rems(32);
  $social-link-padding: (rems(44) - $social-link-size) / 2; // 44px recommended touch target
  $social-links-max-width: rems(420); // icons spacing will grow no larger than this until desktop

  $social-link-size-desktop: rems(22);
  $social-link-padding-desktop: (rems(44) - $social-link-size-desktop) / 2; // 44px recommended touch target
  $social-link-spacing-desktop: 0;

  $social-link-color: $link-color;
  $social-link-hover-color: $link-hover-color;
  $social-brand-color-hovers: true;

  .social-links {
    justify-content: space-between;
    flex: 0 1 auto;
    width: $social-links-max-width;
    max-width: 100%;

    li {
      width: $social-link-size + ($social-link-padding * 2);
      flex-shrink: 1;
    }
  }

  .social-links__link {
    display: block;
    padding: $social-link-padding;
  }

  @include media-breakpoint-up(fd) {

    .social-links {
      margin-left:  -$social-link-padding-desktop;
      margin-right: -$social-link-padding-desktop;
      width: auto;
      max-width: none;

      li {
        width: $social-link-size-desktop + ($social-link-padding-desktop * 2);
      }
    }

    .social-links__link {
      display: block;
      padding: $social-link-padding-desktop;
    }

    .social-links li + li {
      margin-left: $social-link-spacing-desktop;
    }
  }

  // Brand colors @see https://simpleicons.org/
  $brand-hex-facebook: #3b5998;
  $brand-hex-instagram: #e4405f;
  $brand-hex-linkedin: #0077b5;
  $brand-hex-medium: #1c1b1a;
  $brand-hex-pinterest: #bd081c;
  $brand-hex-twitter: #1da1f2;
  $brand-hex-youtube: #ff0000;

  // mixin for social link hover colors
  @mixin social-links__hover-color($social-hover-color: $link-hover-color) {

    &:hover,
    &:focus,
    &:active {
      color: $social-hover-color;
    }
  }

  @if $social-brand-color-hovers {

    .social-links__link--facebook {
      @include social-links__hover-color($brand-hex-facebook)
    }

    .social-links__link--instagram {
      @include social-links__hover-color($brand-hex-instagram);
    }

    .social-links__link--linkedin {
      @include social-links__hover-color($brand-hex-linkedin);
    }

    .social-links__link--medium {
      @include social-links__hover-color($brand-hex-medium);
    }

    .social-links__link--pinterest {
      @include social-links__hover-color($brand-hex-pinterest);
    }

    .social-links__link--twitter {
      @include social-links__hover-color($brand-hex-twitter);
    }

    .social-links__link--youtube {
      @include social-links__hover-color($brand-hex-youtube);
    }

  } @else {

    .social-links__link {
      color: $social-link-color;

      &:hover,
      &:focus,
      &:active {
        color: $social-link-hover-color;
      }
    }
  }
`

function SocialShareIcons() {
  return (
    <ul className="social-links list-inline flex-shrink-1 d-flex mb-0">
      <li>
        <a
          className="social-links__link social-links__link--facebook"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Follow us on Facebook</span>
          <FacebookIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <li>
        <a
          className="social-links__link social-links__link--instagram"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Follow us on Instagram</span>
          <InstagramIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <li>
        <a
          className="social-links__link social-links__link--linkedin"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Follow us on LinkedIn</span>
          <LinkedInIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <li>
        <a
          className="social-links__link social-links__link--medium"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Follow us on Medium</span>
          <MediumIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <li>
        <a
          className="social-links__link social-links__link--pinterest"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Follow us on Pinterest</span>
          <PinterestIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <li>
        <a
          className="social-links__link social-links__link--twitter"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Follow us on Twitter</span>
          <TwitterIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <li>
        <a
          className="social-links__link social-links__link--youtube"
          href="#"
          target="_blank"
          rel="nofollow noopener"
        >
          <span className="sr-only">Subscribe to our channel on YouTube</span>
          <YouTubeIcon className={bsUtilityClasses} ariaHidden />
        </a>
      </li>
      <style jsx>{styles}</style>
    </ul>
  )
}

export default SocialShareIcons
