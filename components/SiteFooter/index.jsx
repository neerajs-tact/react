import React from 'react'
import { bool, string } from 'prop-types'
import {
  Container,
  Nav,
  Navbar,
  NavItem,
  NavLink,
  NavbarBrand,
} from 'reactstrap'
import { Link } from '../../routes'
import SocialShareIcons from './SocialShareIcons'
import { LogoLg } from '../logos'
import LinkActiveNavLink from '../LinkActiveNavLink'

SiteFooter.propTypes = {
  fnavbarDark: bool,
  fnavbarDarkColor: string,
  fnavbarLightColor: string,
}

SiteFooter.defaultProps = {
  fnavbarLightColor: 'white', // can be white or context names
  fnavbarDark: false,
  fnavbarDarkColor: 'dark', // can be white or context names
}

const year = new Date().getFullYear()

/**
 * It is recommended you leave the expand prop set to 'fd' which is a SCSS
 * configurable breakpoint for this footer's mobile to desktop layout switching.
 *
 * This footer uses a duplicated navbar config called fnavbar. It duplicates
 * most of the features found in navbar except toggle/collapse and adds settings
 * specific to controlling some mobile layout properties.
 */
function SiteFooter({ fnavbarLightColor, fnavbarDark, fnavbarDarkColor }) {
  return (
    <footer className="border-top">
      <Navbar
        expand="fd"
        light={!fnavbarDark}
        dark={fnavbarDark}
        color={fnavbarDark ? fnavbarDarkColor : fnavbarLightColor}
      >
        <Container fluid>
          {/* Linked Brand Logo */}
          <Link route="home" passHref>
            <NavbarBrand className="d-fd-none mr-0 text-primary">
              <LogoLg />
              <span className="sr-only">Home</span>
            </NavbarBrand>
          </Link>
          <hr className="hr-vr ml-auto mx-40 d-none d-fd-block" />
          <Nav navbar className="mx-ncp-sm mx-md-ncp-lg mx-fd-0">
            <NavItem>
              <NavLink href="#">Privacy</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Terms</NavLink>
            </NavItem>
            <NavItem>
              <LinkActiveNavLink route="contact">Contact</LinkActiveNavLink>
            </NavItem>
          </Nav>
          <div className="fnavbar-social-share ml-fd-30 mw-100 d-flex">
            <SocialShareIcons />
          </div>
          <small className="fnavbar-copyright navbar-text text-center order-fd-first">
            &copy;{year} Starter<span className="d-none d-fd-inline">, </span>
            <br className="d-fd-none" />
            All Rights Reserved
          </small>
        </Container>
      </Navbar>
    </footer>
  )
}

export default SiteFooter
