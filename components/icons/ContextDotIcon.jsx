import React, { memo } from 'react'
import { bool, number, string } from 'prop-types'

ContextDotIcon.defaultProps = {
  ariaHidden: false,
  className: undefined,
  size: 18,
  title: 'Dot',
}

ContextDotIcon.propTypes = {
  ariaHidden: bool,
  className: string,
  size: number,
  title: string,
}

function ContextDotIcon({ ariaHidden, className, size, title }) {
  return (
    <svg
      className={className}
      aria-hidden={ariaHidden}
      height={size}
      width={size}
      viewBox={`0 0 ${size} ${size}`}
      xmlns="http://www.w3.org/2000/svg"
      role="img"
    >
      <title>{title}</title>
      <circle fill="currentColor" cx={size / 2} cy={size / 2} r={size / 2} />
    </svg>
  )
}

export default memo(ContextDotIcon)
