/**
 * Icons (SVG) Index
 *
 *
 * Accessibility
 *
 * These icons use <title> tags which is simular to alt text. Add the ariaHidden
 * prop if you are using these with text to prevent doubling the contextual
 * explanation in assistive software.
 *
 *
 * Styling
 *
 * These are single color SVGs that take on the CSS color property around them
 * via the currentColor css property.
 *
 * A className prop is provided if you want to pass a global CSS class. This
 * can include sizing utility classes.
 */

/**
 * https://simpleicons.org
 */
export FacebookIcon from './FacebookIcon'
export InstagramIcon from './InstagramIcon'
export LinkedInIcon from './LinkedInIcon'
export MediumIcon from './MediumIcon'
export PinterestIcon from './PinterestIcon'
export TwitterIcon from './TwitterIcon'
export YouTubeIcon from './YouTubeIcon'

// other icons
export ContextDotIcon from './ContextDotIcon'
