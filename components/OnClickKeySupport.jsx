import { cloneElement, useCallback } from 'react'
import { func, number } from 'prop-types'

OnClickKeySupport.propTypes = {
  onClick: func.isRequired,
  keyCode: number,
}

OnClickKeySupport.defaultProps = {
  keyCode: 32, // space bar
}

/**
 * onClick keyboard support wrapper
 *
 * USE CASE: Non-interactive elements, such as <th>, <div>, <a> without href, etc, that should be
 * focusable via keyboard tab and pressiable via spacebar. Links with href and buttons have this
 * natively.
 *
 * HOW TO USE: Wrap around the element and apply onClick to this element instead. It will pass
 * onClick to the inner element, make it focusable, and duplicate the function for keyboard control.
 */
function OnClickKeySupport({ children, onClick, keyCode }) {
  const onKeyDown = useCallback(
    event => {
      if (event.keyCode === keyCode) {
        event.preventDefault() // prevent screen scrolling
        onClick() // map onClick to onKeyDown
      }
    },
    [keyCode, onClick]
  )

  // merge props into child
  return cloneElement(children, {
    onClick,
    onKeyDown,
    tabIndex: 0, // allows focus on normally non-interactive elements; e.g. <th>
  })
}

export default OnClickKeySupport
