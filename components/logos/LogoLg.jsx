import React, { memo } from 'react'
import { bool, string } from 'prop-types'

LogoLg.defaultProps = {
  ariaHidden: false,
  className: undefined,
  title: 'Starter',
}

LogoLg.propTypes = {
  ariaHidden: bool,
  className: string,
  title: string,
}

function LogoLg({ ariaHidden, className, title }) {
  return (
    <svg
      className={className}
      aria-hidden={ariaHidden}
      xmlns="http://www.w3.org/2000/svg"
      width="133.448"
      height="38"
      viewBox="-161.6 173.8 180 54"
      role="img"
    >
      <title>{title}</title>
      <path fill="currentColor" d="dd" />
    </svg>
  )
}

export default memo(LogoLg)
