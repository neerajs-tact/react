/**
 * Icons (SVG) Index
 *
 *
 * Accessibility
 *
 * These icons use <title> tags which is simular to alt text. Add the ariaHidden
 * prop if you are using these with text to prevent doubling the contextual
 * explanation in assistive software.
 *
 *
 * Styling
 *
 * These are single color SVGs that take on the CSS color property around them
 * via the currentColor css property.
 *
 * A className prop is provided if you want to pass a global CSS class. This
 * can include sizing utility classes.
 */
export LogoLg from './LogoLg'
