import React, { useCallback } from 'react'
import { func } from 'prop-types'
import { connect } from 'react-redux'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import { resetPassword } from '../../modules/users'
import { mapFinalFormErrors } from '../../lib/ff'
import ForgotPasswordForm from './ForgotPasswordForm'

const mapDispatch = { resetPassword }

const ForgotPassword = connect(
  null,
  mapDispatch
)(ForgotPasswordFormController)

ForgotPasswordFormController.propTypes = {
  resetPassword: func.isRequired,
}

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors()

function ForgotPasswordFormController({ resetPassword }) {
  const onSubmit = useCallback(
    async function onForgotPasswordFormSubmit(values) {
      try {
        await resetPassword(values)
        return undefined
      } catch (error) {
        return mapErrors(error)
      }
    },
    [resetPassword]
  )

  return (
    <Form
      component={ForgotPasswordForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      subscription={{
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default ForgotPassword
