import React from 'react'
import { bool, func } from 'prop-types'
import { Alert, Button, Form } from 'reactstrap'
import { useUIDSeed } from 'react-uid'
import { validateEmail, validateRequired } from '../../lib/ff'
import { FFormGroup, SubmitErrorAlerts } from '../ff'

ForgotPasswordForm.propTypes = {
  handleSubmit: func.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function ForgotPasswordForm({ handleSubmit, submitSucceeded, submitting }) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      {submitSucceeded ? (
        <Alert color="success">
          Follow the instructions in the email we have now sent you to complete
          your password reset.
        </Alert>
      ) : (
        <>
          <FFormGroup
            label="Email Address"
            name="email"
            type="email"
            seed={seed}
            validators={[validateRequired(), validateEmail()]}
            formText="Enter the email associated with your account then you will be
              emailed a link to reset your password."
            autoComplete="username email"
            inputMode="email"
          />
          <SubmitErrorAlerts />
          <Button
            block
            color="primary"
            className="uaTrack-submit-forgot-password"
            disabled={submitting || submitSucceeded}
            size="lg"
            type="submit"
          >
            {submitting || submitSucceeded
              ? 'Requesting Reset...'
              : 'Request Reset'}
          </Button>
        </>
      )}
    </Form>
  )
}

export default ForgotPasswordForm
