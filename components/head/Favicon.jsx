/**
 * Favicon
 *
 * We use https://realfavicongenerator.net to generate the icons and markup
 * necessary for favicons.
 *
 * When generating a new favicon, place the markup in the component below,
 * and place the files in the `/static/favicons` directory.
 */

import React from 'react'

function Favicon() {
  return (
    // prettier-ignore
    <>
      <link rel="apple-touch-icon" sizes="152x152" href="/static/favicons/apple-touch-icon.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
      <link rel="manifest" href="/static/favicons/site.webmanifest" />
      <link rel="mask-icon" href="/static/favicons/safari-pinned-tab.svg" color="#32b493" />
      <link rel="shortcut icon" href="/static/favicons/favicon.ico" />
      <meta name="msapplication-TileColor" content="#32b493" />
      <meta name="msapplication-config" content="/static/favicons/browserconfig.xml" />
      <meta name="theme-color" content="#32b493" />
    </>
  )
}

export default Favicon
