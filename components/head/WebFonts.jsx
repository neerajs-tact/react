import React from 'react'

function WebFonts() {
  return (
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat:700|Open+Sans:400,400i,700,700i&display=swap"
      rel="stylesheet"
    />
  )
}

export default WebFonts
