/**
 * Google Tag Manager
 *
 * Use the GTMScript and GTMNoScript components to render the markup necessary
 * for Google Tag Manager. These components load the GTM ID from the Next.js
 * public runtime config.
 *
 * The components render nothing when run in development mode.
 *
 * Note that this file is included from `pages/_document.js` and thus always
 * runs server-side. This means that `process.env` is available.
 */

import React from 'react'
import getConfig from 'next/config'

let gtmId
if (process.env.NODE_ENV === 'production') {
  const { publicRuntimeConfig } = getConfig()
  gtmId = publicRuntimeConfig.gtmId || null
}

export function GTMScript() {
  if (!gtmId) {
    return null
  }

  return (
    <script
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: `
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','${gtmId}');
        `,
      }}
    />
  )
}

export function GTMNoScript() {
  if (!gtmId) {
    return null
  }

  return (
    <noscript
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=${gtmId}" height="0" width="0" style="display:none;visibility:hidden" />`,
      }}
    />
  )
}
