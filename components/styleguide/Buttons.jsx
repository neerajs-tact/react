import React from 'react'
import { Button } from 'reactstrap'

function Buttons() {
  return (
    // prettier-ignore
    <>
      <h6>Default</h6>
      <div className="mb-40">
        <Button color="primary">primary</Button>{' '}
        <Button color="secondary">secondary</Button>{' '}
        <Button color="tertiary">tertiary</Button>{' '}
        <Button color="success">success</Button>{' '}
        <Button color="info">info</Button>{' '}
        <Button color="warning">warning</Button>{' '}
        <Button color="danger">danger</Button>{' '}
        <Button color="light">light</Button>{' '}
        <Button color="dark">dark</Button>{' '}
        <Button color="link">link</Button>
      </div>
      <h6>Outline</h6>
      <div className="mb-40">
        <Button outline color="primary">primary</Button>{' '}
        <Button outline color="secondary">secondary</Button>{' '}
        <Button outline color="tertiary">tertiary</Button>{' '}
        <Button outline color="success">success</Button>{' '}
        <Button outline color="info">info</Button>{' '}
        <Button outline color="warning">warning</Button>{' '}
        <Button outline color="danger">danger</Button>{' '}
        <Button outline color="light">light</Button>{' '}
        <Button outline color="dark">dark</Button>
      </div>
      <h6>Large</h6>
      <div className="mb-40">
        <Button size="lg" color="primary">primary</Button>{' '}
        <Button size="lg" color="secondary">secondary</Button>{' '}
        <Button size="lg" color="tertiary">tertiary</Button>{' '}
        <Button size="lg" color="success">success</Button>{' '}
        <Button size="lg" color="info">info</Button>{' '}
        <Button size="lg" color="warning">warning</Button>{' '}
        <Button size="lg" color="danger">danger</Button>{' '}
        <Button size="lg" color="light">light</Button>{' '}
        <Button size="lg" color="dark">dark</Button>{' '}
        <Button size="lg" color="link">link</Button>
      </div>
      <h6>Large Outline</h6>
      <div className="mb-40">
        <Button outline size="lg" color="primary">primary</Button>{' '}
        <Button outline size="lg" color="secondary">secondary</Button>{' '}
        <Button outline size="lg" color="tertiary">tertiary</Button>{' '}
        <Button outline size="lg" color="success">success</Button>{' '}
        <Button outline size="lg" color="info">info</Button>{' '}
        <Button outline size="lg" color="warning">warning</Button>{' '}
        <Button outline size="lg" color="danger">danger</Button>{' '}
        <Button outline size="lg" color="light">light</Button>{' '}
        <Button outline size="lg" color="dark">dark</Button>{' '}
      </div>
      <h6>Small</h6>
      <div className="mb-40">
        <Button size="sm" color="primary">primary</Button>{' '}
        <Button size="sm" color="secondary">secondary</Button>{' '}
        <Button size="sm" color="tertiary">tertiary</Button>{' '}
        <Button size="sm" color="success">success</Button>{' '}
        <Button size="sm" color="info">info</Button>{' '}
        <Button size="sm" color="warning">warning</Button>{' '}
        <Button size="sm" color="danger">danger</Button>{' '}
        <Button size="sm" color="light">light</Button>{' '}
        <Button size="sm" color="dark">dark</Button>{' '}
        <Button size="sm" color="link">link</Button>
      </div>
      <h6>Small Outline</h6>
      <div className="mb-40">
        <Button outline size="sm" color="primary">primary</Button>{' '}
        <Button outline size="sm" color="secondary">secondary</Button>{' '}
        <Button outline size="sm" color="tertiary">tertiary</Button>{' '}
        <Button outline size="sm" color="success">success</Button>{' '}
        <Button outline size="sm" color="info">info</Button>{' '}
        <Button outline size="sm" color="warning">warning</Button>{' '}
        <Button outline size="sm" color="danger">danger</Button>{' '}
        <Button outline size="sm" color="light">light</Button>{' '}
        <Button outline size="sm" color="dark">dark</Button>
      </div>
      <h6>Square</h6>
      <div>
        <Button className="w-btn-height px-0" color="primary">D</Button>{' '}
        <Button className="w-btn-height-lg px-0" size="lg" color="primary">LG</Button>{' '}
        <Button className="w-btn-height-sm px-0" size="sm" color="primary">SM</Button>{' '}
        <Button outline className="w-btn-height px-0" color="secondary">D</Button>{' '}
        <Button outline className="w-btn-height-lg px-0" size="lg" color="secondary">LG</Button>{' '}
        <Button outline className="w-btn-height-sm px-0" size="sm" color="secondary">SM</Button>
        <p className="mt-10">Note you may need to combine the btn height utility with a padding utility to remove x-axis padding.</p>
      </div>
    </>
  )
}

export default Buttons
