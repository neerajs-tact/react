import React from 'react'

function Typography() {
  return (
    <>
      <h1 className="text-primary">h1 responsive heading</h1>
      <h2>h2 responsive heading</h2>
      <h3>h3 responsive heading</h3>
      <h4>h4 responsive heading</h4>
      <h5>h5 responsive heading</h5>
      <h6>
        h6 responsive heading with an example of line wrapping to see heading
        line-height
      </h6>
      <p>
        The headings above are responsive for screen widths smaller than the
        widest breakpoint while the ones below are static via a{' '}
        <code>.h#-static-size</code> utility class.
      </p>
      <h1 className="text-primary h1-static-size">h1 static heading</h1>
      <h2 className="h2-static-size">h2 static heading</h2>
      <h3 className="h3-static-size">h3 static heading</h3>
      <h4 className="h4-static-size">h4 static heading</h4>
      <h5 className="h5-static-size">h5 static heading</h5>
      <h6 className="h6-static-size">
        h6 static heading with an example of line wrapping to see heading
        line-height
      </h6>
      <p>
        Body Copy
        <br />
        Lipsum dolor sit amet, consectetur adipiscing elit. Fusce lectus purus,
        porttitor vitae enim id, bibendum. <strong>Bold copy, </strong>
        <em>italic copy, </em>
        <strong>
          <em>bold and italic copy.</em>
        </strong>
      </p>
      <p>
        <small>
          Body Copy Small
          <br />
          Lipsum dolor sit amet, consectetur adipiscing elit. Fusce lectus
          purus, porttitor vitae enim id, bibendum. <strong>Bold copy, </strong>
          <em>italic copy, </em>
          <strong>
            <em>bold and italic copy.</em>
          </strong>
        </small>
      </p>
      <p className="lead">
        Lead Body Copy
        <br />
        Lipsum dolor sit amet, consectetur adipiscing elit. Fusce lectus purus,
        porttitor vitae enim id, bibendum. <strong>Bold copy, </strong>
        <em>italic copy, </em>
        <strong>
          <em>bold and italic copy.</em>
        </strong>
      </p>
      <p>
        <a
          href="https://www.example.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          This is a text link
        </a>
        .
      </p>
    </>
  )
}

export default Typography
