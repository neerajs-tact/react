import React from 'react'
import { Button } from 'reactstrap'

function VerticalDividers() {
  return (
    <>
      <h6>Default</h6>
      <div className="mb-40">
        Some divided <hr className="hr-vr" />
        text.
      </div>
      <h6>Large</h6>
      <div className="mb-40">
        Some divided <hr className="hr-vr hr-vr-lg" />
        text.
      </div>
      <h6>Match Default button</h6>
      <div className="mb-40">
        <Button color="primary">Button</Button>
        <hr className="hr-vr hr-vr-btn mx-30" />
        <Button color="primary">Button</Button>
      </div>
      <h6>Match Small button</h6>
      <div className="mb-40">
        <Button size="sm" color="primary">
          Button
        </Button>
        <hr className="hr-vr hr-vr-btn-sm" />
        <Button size="sm" color="primary">
          Button
        </Button>
      </div>
      <h6>Match Large button</h6>
      <div className="mb-40">
        <Button size="lg" color="primary">
          Button
        </Button>
        <hr className="hr-vr hr-vr-btn-lg" />
        <Button size="lg" color="primary">
          Button
        </Button>
      </div>
    </>
  )
}

export default VerticalDividers
