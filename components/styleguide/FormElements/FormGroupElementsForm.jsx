import React from 'react'
import { func, bool } from 'prop-types'
import { useUIDSeed } from 'react-uid'
import { Alert, Button, Col, Form, Row } from 'reactstrap'
import {
  formatBoolToString,
  formatCentsToUSDString,
  formatNumToStr,
  formatTelUS,
  invalidDuringInput,
  parseStringToBool,
  parseStrToNum,
  parseTelUS,
  parseUSDStringToCents,
  subscriptionInvalidDuringInput,
  validateEmail,
  validateMinLength,
  validateRequired,
  validateRequiredBool,
  validateTelUS,
} from '../../../lib/ff'
import {
  FFormGroup,
  FieldValue,
  fieldWrapSelectContextDot,
  FormValuesTool,
  SubmitErrorAlerts,
  ValidationErrorAlert,
} from '../../ff'
import { moveCursorToEnd } from '../../../lib/utils'
import {
  groupedSelectOptions,
  idNumsAndContextOptions,
  multiCheckboxInlineOptions,
  multiCheckboxOptions,
  multiRadioInlineOptions,
  multiRadioOptions,
  multiSwitchOptions,
  selectOptions,
  yesNoRadioOptions,
} from '../../../lib/data/styleguide'
import FormValuesControls from './FormValuesControls'

// start - for validation testing and theming, not for production code
const requireOne = value =>
  value === 'one' || (Array.isArray(value) && value.includes('one'))
    ? undefined
    : 'Select the first option to pass validation.'
// end - for validation testing and theming, not for production code

FormGroupElementsForm.propTypes = {
  handleSubmit: func.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function FormGroupElementsForm({ handleSubmit, submitSucceeded, submitting }) {
  // unique for/id pairs and keys per form instance
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <p>
        This panel uses our <code>FFormGroup</code> adapter to reduce the amount
        of code needed for these examples. Compare and contrast this form
        against the other to understand the differences and limitations.
      </p>
      <p>
        Multiple checkboxes, radios and switches can be generated using the
        options prop much in the same manner as selects.
      </p>
      <p>
        Both forms use the same container index.
        <br />
        <br />
      </p>

      {/* Email */}
      <FFormGroup
        label="Email"
        name="email"
        type="email"
        seed={seed}
        autoComplete="email"
        formText="We'll never share your email with anyone else."
        placeholder="This is a placeholder"
        validators={[validateRequired(), validateEmail()]}
        inputMode="email" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
      />

      {/* Password with overrides to show while typing */}
      <FFormGroup
        label="Password"
        name="password"
        type="password"
        seed={seed}
        autoComplete="password"
        formText="This example shows how to show validation messages as you type and success (valid) highlighting."
        validators={[
          validateRequired(),
          validateMinLength(6, 'Use at least 6 characters.'),
        ]}
        // validate during user input override
        invalid={invalidDuringInput}
        valid={meta => !meta.invalid} // show valid highlight
        // error message subscription for new invalid function field states
        errorSubscription={subscriptionInvalidDuringInput}
      />

      {/* US Tel E.164 Standard (+11234567890) */}
      <FFormGroup
        label="Phone (US 10-digit)"
        name="phoneNumber"
        type="tel"
        seed={seed}
        parse={parseTelUS}
        format={formatTelUS}
        validators={[validateRequired(), validateTelUS()]}
        onClick={event => moveCursorToEnd(event.target)}
        onFocus={event => moveCursorToEnd(event.target)}
        inputMode="tel" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
        formText="Note that parse/format is setup for 10-digit US numbers only parsing into E.164 standard."
      />

      {/* Text */}
      <FFormGroup
        label="Text (with Initial Value)"
        name="text"
        type="text"
        seed={seed}
        placeholder="This is a placeholder"
        validators={[validateRequired()]}
      />

      {/* Text (Disabled) */}
      <FFormGroup
        label="Disabled"
        name="textDisabled"
        type="text"
        seed={seed}
        // option for JSX to wrap field component
        fieldWrap={field => (
          <div className="d-flex">
            {field}
            <Button className="ml-20" color="primary" type="submit">
              Submit
            </Button>
          </div>
        )}
        disabled
      />

      {/* Select */}
      <FFormGroup
        label="Select"
        name="select"
        type="select"
        seed={seed}
        options={selectOptions}
        placeholder="Select your State"
        validators={[validateRequired()]}
      />

      {/* Custom Select */}
      <FFormGroup
        label="Custom Select"
        name="customSelect"
        type="select"
        custom
        seed={seed}
        options={selectOptions}
        placeholder="Select your State"
        validators={[validateRequired()]}
      />

      {/* Multi Select w/<optgroup> */}
      <FFormGroup
        label="Multi Select with Option Groups"
        name="multiSelectGroup"
        type="select"
        seed={seed}
        multiSelect // add to allow selection of multiple items
        size="6" // height in number of text lines, prevents cutoff text
        options={groupedSelectOptions}
        placeholder="Select Food"
        validators={[validateRequired()]}
      />

      {/* Custom Multi Select w/<optgroup> */}
      <FFormGroup
        label="Custom Multi Select with Option Groups"
        name="customMultiSelectGroup"
        type="select"
        custom
        seed={seed}
        multiSelect // add to allow selection of multiple items
        size="6" // height in number of text lines, prevents cutoff text
        options={groupedSelectOptions}
        placeholder="Select Food"
        validators={[validateRequired()]}
      />

      {/* Textarea */}
      <FFormGroup
        label="Textarea"
        name="textarea"
        type="textarea"
        seed={seed}
        placeholder="This is a placeholder"
        rows={3} // html attribute that sets height by number of lines to prevent cutoff
        validators={[validateRequired()]}
      />

      {/* File */}
      <FFormGroup
        label="File Field"
        name="file"
        type="file"
        accept=".jpg" // attribute that affects file picker, is not secure user can override
        seed={seed}
      />

      {/* Custom File */}
      <FFormGroup
        label="Custom File Field"
        name="customFile"
        customLabel="You can use a custom label here"
        type="file"
        custom
        accept=".jpg" // attribute that affects file picker, is not secure user can override
        seed={seed}
      />

      {/* Range */}
      <FFormGroup
        label="Range (0 to 100, 10 step)"
        name="range"
        type="range"
        seed={seed}
        formText={
          // can be JSX
          <>
            You have selected <FieldValue name="range" />.
          </>
        }
        // range attributes
        min="0"
        max="100"
        step="10"
        validators={[validateRequired()]}
      />

      {/* Custom Range */}
      <FFormGroup
        label="Custom Range"
        name="customRange"
        type="range"
        custom
        seed={seed}
        formText={
          // can be JSX
          <>
            You have selected <FieldValue name="customRange" />.
          </>
        }
        // range attributes
        min="0"
        max="100"
        step="10"
        validators={[validateRequired()]}
      />

      {/* Single Checkbox */}
      <FFormGroup
        formGroupClassNames="mt-30 mb-20"
        label="Single checkbox with a quite a bit of text that will likely wrap so
          you can check that out on your screen."
        name="checkbox"
        type="checkbox"
        seed={seed}
        validators={[validateRequired()]}
      />

      <hr className="border-top-dashed my-40" />

      <Row>
        <Col md={6}>
          {/* Checkboxes */}
          <FFormGroup
            label="Checkboxes"
            name="checkboxes"
            seed={seed}
            type="checkbox"
            options={multiCheckboxOptions}
            validators={[requireOne]}
          />
        </Col>
        <Col md={6}>
          {/* Radios */}
          <FFormGroup
            label="Radios"
            name="radios"
            seed={seed}
            type="radio"
            options={multiRadioOptions}
            validators={[requireOne]}
          />
        </Col>
      </Row>

      <hr className="border-top-dashed my-40" />

      {/* Checkboxes (inline) */}
      <FFormGroup
        label="Checkboxes (inline)"
        name="checkboxesInline"
        seed={seed}
        type="checkbox"
        inline
        options={multiCheckboxInlineOptions}
        validators={[requireOne]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Radios (inline) */}
      <FFormGroup
        label="Radios (inline)"
        name="radiosInline"
        seed={seed}
        type="radio"
        inline
        options={multiRadioInlineOptions}
        validators={[requireOne]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Custom Single Checkbox */}
      <FFormGroup
        label={
          <>
            Single <strong>custom</strong> checkbox with a quite a bit of text
            that will likely wrap so you can check that out on your screen.
          </>
        }
        name="customCheckbox"
        type="checkbox"
        custom
        seed={seed}
        validators={[validateRequired()]}
      />

      <hr className="border-top-dashed my-40" />

      <Row>
        <Col md={6}>
          {/* Custom Checkboxes */}
          <FFormGroup
            label="Custom Checkboxes"
            name="customCheckboxes"
            seed={seed}
            type="checkbox"
            custom
            options={multiCheckboxOptions}
            validators={[requireOne]}
          />
        </Col>
        <Col md={6}>
          {/* Custom Radios */}
          <FFormGroup
            label="Custom Radios"
            name="customRadios"
            seed={seed}
            type="radio"
            custom
            options={multiRadioOptions}
            validators={[requireOne]}
          />
        </Col>
      </Row>

      <hr className="border-top-dashed my-40" />

      {/* Custom Checkboxes (inline) */}
      <FFormGroup
        label="Custom Checkboxes (inline)"
        name="customCheckboxesInline"
        seed={seed}
        type="checkbox"
        inline
        custom
        options={multiCheckboxInlineOptions}
        validators={[requireOne]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Custom Radios (inline) */}
      <FFormGroup
        label="Custom Radios (inline)"
        name="customRadiosInline"
        seed={seed}
        type="radio"
        inline
        custom
        options={multiRadioInlineOptions}
        validators={[requireOne]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Single Switch */}
      <FFormGroup
        label="Single Toggle with a quite a bit of text that will likely wrap so
          you can check that out on your screen."
        name="customSwitch"
        seed={seed}
        type="switch"
        custom
        validators={[validateRequired()]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Custom Switches */}
      <FFormGroup
        label="Custom Switches (Checkbox Toggles)"
        name="customSwitches"
        seed={seed}
        type="switch"
        custom
        options={multiSwitchOptions}
        validators={[requireOne]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Custom Switches (inline) */}
      <FFormGroup
        label="Custom Switches (inline)"
        name="customSwitchesInline"
        seed={seed}
        type="switch"
        custom
        inline
        options={multiRadioInlineOptions}
        validators={[requireOne]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Advanced Select with number ids and context colors */}
      <FFormGroup
        label="Advanced Select Options"
        name="customSelectAdvanced"
        type="select"
        custom
        seed={seed}
        options={idNumsAndContextOptions}
        placeholder="Select a Bootstrap Context"
        validators={[validateRequired()]}
        fieldWrap={fieldWrapSelectContextDot(
          'customSelectAdvanced',
          idNumsAndContextOptions
        )}
        // keep ids as numbers
        parse={parseStrToNum()}
        format={formatNumToStr()}
        formText={
          <ul className="mb-0">
            <li>Parse/Format keeps id data as numbers.</li>
            <li>
              Bootstrap context color data as <code>contextColor</code> property
              in option data.
            </li>
            <li>Context colored indicator using fieldWrap prop.</li>
            <li>
              Icon has configurable size while spacing can be controlled in{' '}
              <code>fieldWrapSelectContextDot</code> component.
            </li>
          </ul>
        }
      />

      <hr className="border-top-dashed my-40" />

      {/* USD Amount with number-only entry */}
      <FFormGroup
        label="Enter Amount"
        name="amountUSDCents"
        seed={seed}
        type="text"
        placeholder="$0.00"
        validators={[validateRequired()]}
        formText="This input uses parse/format and some additional onClick/Focus cursor moving functions emulate the money entry UI for Paypal and others. Parse function has option to allow zero."
        format={formatCentsToUSDString}
        parse={parseUSDStringToCents()}
        onClick={event => moveCursorToEnd(event.target)}
        onFocus={event => moveCursorToEnd(event.target)}
      />

      <hr className="border-top-dashed my-40" />

      {/* Custom Yes/No Radios */}
      <FFormGroup
        label="Trigger Submit Error?"
        name="triggerSubmitError"
        seed={seed}
        type="radio"
        custom
        options={yesNoRadioOptions}
        // use yes/no values but store in FF as true/false via parse/format
        parse={parseStringToBool()}
        format={formatBoolToString()}
        formText="This example shows how to make yes/no radios that use true/false as as their data even though values have to strings. Take note of the validation and use of parse/format."
        validators={[validateRequiredBool()]}
      />

      <hr className="border-top-dashed my-40" />

      {/* Alert for client-side errors */}
      <ValidationErrorAlert msg="This is a general alert that shows when submit is attempted but is prevented by field-level validation errors. For long forms this helps prompt the user that there are errors above the top fold. Usually not needed if you are using final-form-focus. Default message is: Please fix the highlighted errors above." />

      {/* Alerts for Submit Errors */}
      <SubmitErrorAlerts />

      {/* submitSucceeded can be used for success logic including replacing the form with content */}
      <Alert color="success" isOpen={submitSucceeded}>
        Form successfully submitted
      </Alert>

      <Button
        block
        size="lg"
        color="success"
        disabled={submitting}
        type="submit"
      >
        Submit and View Values
      </Button>

      <FormValuesControls className="mt-20" />

      <h6 className="mt-40">FormSpy of Values</h6>
      <FormValuesTool />
    </Form>
  )
}

export default FormGroupElementsForm
