import React, { useCallback } from 'react'
import { bool } from 'prop-types'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import { FORM_ERROR } from 'final-form'
import { mapFinalFormErrors } from '../../../lib/ff'
import { sleep } from '../../../lib/utils'
import FormElementsForm from './FormElementsForm'
import FormGroupElementsForm from './FormGroupElementsForm'

FormElements.propTypes = {
  isFFGroup: bool, // determines which form is used; form element fragments vs FFormGroup helper
}

FormElements.defaultProps = {
  isFFGroup: false,
}

const focusOnError = createDecorator()

const mapErrors = mapFinalFormErrors('Failed to submit test form.')

function FormElements({ isFFGroup }) {
  const onSubmit = useCallback(async function onFinalFormSubmit(values) {
    try {
      await sleep(1000) // simulated API delay, don't copy into production

      /*
        Ideally you should use field-level validation on your fields to prompt the user during
        form entry. Server validation from the submission into the API is handled with the submit
        function try/catch with mapErrors.

        In the rare case you need to do client-side validation during the submit this following
        technique allows for the building of an error object instead of immediately returning on
        first error. The error object let's you return form-level errors as well as field-level.
      */

      // setup formErrors object
      const formSubmitErrors = { [FORM_ERROR]: [] }

      // submit error condition
      if (values.triggerSubmitError) {
        // add form-level error messages that show up as alerts
        formSubmitErrors[FORM_ERROR].push(
          'This is a simulated client-side submit error.'
        )
        formSubmitErrors[FORM_ERROR].push(
          'Multiple form-level error messages are supported.'
        )
        // add field-level error messages for specific fields
        formSubmitErrors.triggerSubmitError =
          'Change this option if you wish to submit successfully.'

        // validation fails when error object is returned instead of undefined
        return formSubmitErrors
      }

      return undefined // triggers Final-Form submitSucceeded state
    } catch (error) {
      return mapErrors(error) // maps API errors to FORM_ERROR object and returns it
    }
  }, [])

  return (
    <Form
      component={isFFGroup ? FormGroupElementsForm : FormElementsForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      /*
        It is recommended to initialize checkbox/switch groups with an empty
        array to prevent it being undefined and requiring condition checking
        before use of map and array methods.
       */
      initialValues={{
        text: 'This is a text value',
        textDisabled: 'I am a disabled field.',
        checkboxes: ['two'],
        radios: 'two',
        checkboxesInline: ['two'],
        radiosInline: 'two',
        customCheckboxes: ['two'],
        customRadios: 'two',
        customCheckboxesInline: ['two'],
        customRadiosInline: 'two',
        customSwitches: ['two'],
        customSwitchesInline: ['two'],
      }}
      /*
        Subscribe to only the form state props you use in the form component.
        FormSpy can be used to build components for the form that subscribe themselves rather than
        relying on subscriptions here.
       */
      subscription={{
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default FormElements
