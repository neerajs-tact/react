import React from 'react'
import { string } from 'prop-types'
import { Button } from 'reactstrap'
import { useForm } from 'react-final-form'

FormValuesControls.propTypes = {
  className: string,
}

FormValuesControls.defaultProps = {
  className: undefined,
}

const validValues = {
  email: 'test@example.com',
  password: 'password',
  phoneNumber: '+11234567890',
  text: 'text',
  select: 'AZ',
  customSelect: 'AZ',
  multiSelectGroup: ['icecream'],
  customMultiSelectGroup: ['icecream'],
  textarea: 'Here is some text.',
  range: 90,
  customRange: 90,
  checkbox: true,
  checkboxes: ['one'],
  radios: 'one',
  checkboxesInline: ['one'],
  radiosInline: 'one',
  customCheckbox: true,
  customCheckboxes: ['one'],
  customRadios: 'one',
  customCheckboxesInline: ['one'],
  customRadiosInline: 'one',
  customSwitch: true,
  customSwitches: ['one'],
  customSwitchesInline: ['one'],
  customSelectAdvanced: 0,
  amountUSDCents: 15000000,
  triggerSubmitError: false,
}

function FormValuesControls({ className }) {
  // get FF API, only works if this component is inside a Final-Form <Form/>
  const formApi = useForm() // https://final-form.org/docs/final-form/types/FormApi

  const setValidValues = () =>
    // batch ensures all listeners are updated at the same time
    formApi.batch(() => {
      Object.keys(validValues).forEach(fieldName => {
        // change field value
        formApi.change(fieldName, validValues[fieldName])
        // blur or focus triggers field "touched" state triggering validation
        formApi.blur(fieldName)
      })
    })

  return (
    <div className={`d-flex justify-content-between ${className}`}>
      <Button color="light" size="sm" onClick={() => formApi.reset()}>
        Reset Form
      </Button>
      <Button color="light" size="sm" onClick={setValidValues}>
        Set Valid Values
      </Button>
    </div>
  )
}

export default FormValuesControls
