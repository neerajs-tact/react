import React from 'react'
import { func, bool } from 'prop-types'
import { Field } from 'react-final-form'
import { useUIDSeed } from 'react-uid'
import {
  Alert,
  Button,
  Col,
  Form,
  FormGroup,
  FormText,
  Label,
  Row,
} from 'reactstrap'
import {
  composeValidators,
  formatBoolToString,
  formatCentsToUSDString,
  formatNumToStr,
  formatTelUS,
  invalidDuringInput,
  parseStringToBool,
  parseStrToNum,
  parseTelUS,
  parseUSDStringToCents,
  subscriptionInvalidDuringInput,
  validateEmail,
  validateMinLength,
  validateRequired,
  validateRequiredBool,
  validateTelUS,
} from '../../../lib/ff'
import {
  CustomInputAdapter,
  FieldValue,
  fieldWrapSelectContextDot,
  FormFeedbackAdapter,
  FormValuesTool,
  InputAdapter,
  SelectAdapter,
  SubmitErrorAlerts,
  ValidationErrorAlert,
} from '../../ff'
import {
  groupedSelectOptions,
  idNumsAndContextOptions,
  selectOptions,
} from '../../../lib/data/styleguide'
import FormValuesControls from './FormValuesControls'
import { moveCursorToEnd } from '../../../lib/utils'

// start - for validation testing and theming, not for production code
const requireOne = value =>
  value === 'one' || (Array.isArray(value) && value.includes('one'))
    ? undefined
    : 'Select the first option to pass validation.'
// end - for validation testing and theming, not for production code

FormElementsForm.propTypes = {
  handleSubmit: func.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function FormElementsForm({ handleSubmit, submitSucceeded, submitting }) {
  // unique for/id pairs and keys per form instance
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <p>
        This form demos all the Reactstrap adapters in our kit for Final-Form in
        all the common Bootstrap 4 patterns.
      </p>
      <p>
        Final-Form-Focus is implemented in the container while in the form
        react-uid is used to create unique label/field pairs and keys.
      </p>
      <p>
        Pay careful attention to the implementation of each field type and
        configuration. You should copy and edit these examples into forms for
        production.
      </p>

      {/* Email */}
      <FormGroup>
        <Label htmlFor={seed('email')}>Email</Label>
        <Field
          autoComplete="email"
          id={seed('email')}
          name="email"
          type="email"
          component={InputAdapter}
          placeholder="This is a placeholder"
          validate={composeValidators(validateRequired(), validateEmail())}
          inputMode="email" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
        />
        <FormText>We&apos;ll never share your email with anyone else.</FormText>
        <FormFeedbackAdapter name="email" />
      </FormGroup>

      {/* Password with overrides to show while typing */}
      <FormGroup>
        <Label htmlFor={seed('password')}>Password</Label>
        <Field
          autoComplete="password"
          id={seed('password')}
          name="password"
          type="password"
          component={InputAdapter}
          validate={composeValidators(
            validateRequired(),
            validateMinLength(6, 'Use at least 6 characters.')
          )}
          // show validation status during user input
          invalid={invalidDuringInput}
          valid={meta => !meta.invalid} // show valid highlight
        />
        <FormText>
          This example shows how to show validation messages as you type and
          success (valid) highlighting.
        </FormText>
        <FormFeedbackAdapter
          // show validation errors during user input
          invalid={invalidDuringInput}
          name="password"
          // error message subscription for new invalid function FF field states
          subscription={subscriptionInvalidDuringInput}
        />
      </FormGroup>

      {/* US Tel E.164 Standard (+11234567890) */}
      <FormGroup>
        <Label htmlFor={seed('phoneNumber')}>Phone (US 10-digit)</Label>
        <Field
          id={seed('phoneNumber')}
          name="phoneNumber"
          type="tel"
          component={InputAdapter}
          parse={parseTelUS}
          format={formatTelUS}
          validate={composeValidators(validateRequired(), validateTelUS())}
          onClick={event => moveCursorToEnd(event.target)}
          onFocus={event => moveCursorToEnd(event.target)}
          inputMode="tel" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
        />
        <FormText>
          Note that parse/format is setup for 10-digit US numbers only parsing
          into E.164 standard.
        </FormText>
        <FormFeedbackAdapter name="phoneNumber" />
      </FormGroup>

      {/* Text */}
      <FormGroup>
        <Label htmlFor={seed('text')}>Text (with Initial Value)</Label>
        <Field
          id={seed('text')}
          name="text"
          type="text"
          component={InputAdapter}
          placeholder="This is a placeholder"
          validate={validateRequired()}
        />
        <FormFeedbackAdapter name="text" />
      </FormGroup>

      {/* Text (Disabled) */}
      <FormGroup>
        <Label htmlFor={seed('textDisabled')}>Disabled</Label>
        <div className="d-flex">
          <Field
            id={seed('textDisabled')}
            name="textDisabled"
            disabled
            type="text"
            component={InputAdapter}
          />
          <Button className="ml-20" color="primary" type="submit">
            Submit
          </Button>
        </div>
        <FormFeedbackAdapter name="textDisabled" />
      </FormGroup>

      {/* Select */}
      <FormGroup>
        <Label htmlFor={seed('select')}>Select</Label>
        <Field
          id={seed('select')}
          name="select"
          type="select"
          component={SelectAdapter}
          options={selectOptions}
          placeholder="Select your State"
          validate={validateRequired()}
        />
        <FormFeedbackAdapter name="select" />
      </FormGroup>

      {/* Custom Select */}
      <FormGroup>
        <Label htmlFor={seed('customSelect')}>Custom Select</Label>
        <Field
          custom
          id={seed('customSelect')}
          name="customSelect"
          type="select"
          component={SelectAdapter}
          options={selectOptions}
          placeholder="Select your State"
          validate={validateRequired()}
        />
        <FormFeedbackAdapter name="customSelect" />
      </FormGroup>

      {/* Multi Select w/<optgroup> */}
      <FormGroup>
        <Label htmlFor={seed('multiSelectGroup')}>
          Multi Select with Option Groups
        </Label>
        <Field
          id={seed('multiSelectGroup')}
          name="multiSelectGroup"
          type="select"
          component={SelectAdapter}
          options={groupedSelectOptions}
          placeholder="Select Food"
          validate={validateRequired()}
          multiSelect // add to allow selection of multiple items
          size="6" // height in number of text lines, prevents cutoff text
        />
        <FormFeedbackAdapter name="multiSelectGroup" />
      </FormGroup>

      {/* Custom Multi Select w/<optgroup> */}
      <FormGroup>
        <Label htmlFor={seed('customMultiSelectGroup')}>
          Custom Multi Select with Option Groups
        </Label>
        <Field
          custom
          id={seed('customMultiSelectGroup')}
          name="customMultiSelectGroup"
          type="select"
          component={SelectAdapter}
          options={groupedSelectOptions}
          placeholder="Select Food"
          validate={validateRequired()}
          multiSelect // add to allow selection of multiple items
          size="6" // height in number of text lines, prevents cutoff text
        />
        <FormFeedbackAdapter name="customMultiSelectGroup" />
      </FormGroup>

      {/* Textarea */}
      <FormGroup>
        <Label htmlFor={seed('textarea')}>Textarea</Label>
        <Field
          id={seed('textarea')}
          name="textarea"
          type="textarea"
          component={InputAdapter}
          placeholder="This is a placeholder"
          rows={3} // html attribute that sets height by number of lines to prevent cutoff
          validate={validateRequired()}
        />
        <FormFeedbackAdapter name="textarea" />
      </FormGroup>

      {/* File */}
      <FormGroup>
        <Label htmlFor={seed('file')}>File Field</Label>
        <Field
          id={seed('file')}
          name="file"
          type="file"
          accept=".jpg" // attribute that affects file picker, is not secure user can override
          component={InputAdapter}
        />
        <FormFeedbackAdapter name="file" />
      </FormGroup>

      {/* Custom File */}
      <FormGroup>
        <Label htmlFor={seed('customFile')}>Custom File Field</Label>
        <Field
          label="You can use a custom label here"
          id={seed('customFile')}
          name="customFile"
          type="file"
          accept=".jpg" // attribute that affects file picker, is not secure user can override
          component={CustomInputAdapter}
        />
        <FormFeedbackAdapter name="customFile" />
      </FormGroup>

      {/* Range */}
      <FormGroup>
        <Label htmlFor={seed('range')}>Range (0 to 100, 10 step)</Label>
        <Field
          id={seed('range')}
          name="range"
          type="range"
          min="0"
          max="100"
          step="10"
          component={InputAdapter}
          validate={validateRequired()}
        />
        <FormText>
          You have selected <FieldValue name="range" />.
        </FormText>
        <FormFeedbackAdapter name="range" />
      </FormGroup>

      {/* Custom Range */}
      <FormGroup>
        <Label htmlFor={seed('customRange')}>Custom Range</Label>
        <Field
          label="You can use a custom label here"
          id={seed('customRange')}
          name="customRange"
          type="range"
          min="0"
          max="100"
          step="10"
          component={CustomInputAdapter}
          validate={validateRequired()}
        />
        <FormText>
          You have selected <FieldValue name="customRange" />.
        </FormText>
        <FormFeedbackAdapter name="customRange" />
      </FormGroup>

      {/* Single Checkbox */}
      <FormGroup check className="mt-30 mb-20">
        <Field
          id={seed('checkbox')}
          name="checkbox"
          type="checkbox"
          component={InputAdapter}
          validate={validateRequired()}
        />
        <Label check htmlFor={seed('checkbox')}>
          Single checkbox with a quite a bit of text that will likely wrap so
          you can check that out on your screen.
        </Label>
        <FormFeedbackAdapter name="checkbox" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      <Row>
        <Col md={6}>
          {/* Checkboxes */}
          <FormGroup tag="fieldset">
            <legend className="legend-label">Checkboxes</legend>
            <FormGroup check>
              <Field
                id={seed('checkboxes-one')}
                name="checkboxes"
                value="one"
                type="checkbox"
                component={InputAdapter}
                // checkbox/radio groups only need validation in first field of group
                validate={requireOne}
              />
              <Label check htmlFor={seed('checkboxes-one')}>
                Checkbox Unchecked
              </Label>
            </FormGroup>
            <FormGroup check>
              <Field
                id={seed('checkboxes-two')}
                name="checkboxes"
                value="two"
                type="checkbox"
                component={InputAdapter}
              />
              <Label check htmlFor={seed('checkboxes-two')}>
                Checkbox Checked
              </Label>
            </FormGroup>
            <FormGroup check>
              <Field
                id={seed('checkboxes-disabled')}
                name="checkboxes"
                value="disabled"
                disabled
                type="checkbox"
                component={InputAdapter}
              />
              <Label check htmlFor={seed('checkboxes-three')}>
                Checkbox Disabled
              </Label>
              {/* in groups of checkboxes/radio place feedback after last label */}
              <FormFeedbackAdapter name="checkboxes" />
            </FormGroup>
          </FormGroup>
        </Col>
        <Col md={6}>
          {/* Radios */}
          <FormGroup tag="fieldset">
            <legend className="legend-label">Radios</legend>
            <FormGroup check>
              <Field
                id={seed('radios-one')}
                name="radios"
                value="one"
                type="radio"
                component={InputAdapter}
                // checkbox/radio groups only need validation in first field of group
                validate={requireOne}
              />
              <Label check htmlFor={seed('radios-one')}>
                Radio Unselected
              </Label>
            </FormGroup>
            <FormGroup check>
              <Field
                id={seed('radios-two')}
                name="radios"
                value="two"
                type="radio"
                component={InputAdapter}
              />
              <Label check htmlFor={seed('radios-two')}>
                Radio Selected
              </Label>
            </FormGroup>
            <FormGroup check>
              <Field
                id={seed('radios-disabled')}
                name="radios"
                value="disabled"
                disabled
                type="radio"
                component={InputAdapter}
              />
              <Label check htmlFor={seed('radios-three')}>
                Radio Disabled
              </Label>
              {/* in groups of checkboxes/radio place feedback after last label */}
              <FormFeedbackAdapter name="radios" />
            </FormGroup>
          </FormGroup>
        </Col>
      </Row>

      <hr className="border-top-dashed my-40" />

      {/* Checkboxes (inline) */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">Checkboxes (inline)</legend>
        <FormGroup check inline>
          <Field
            id={seed('checkboxesInline-one')}
            name="checkboxesInline"
            value="one"
            type="checkbox"
            component={InputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={requireOne}
          />
          <Label check htmlFor={seed('checkboxesInline-one')}>
            Unchecked
          </Label>
        </FormGroup>
        <FormGroup check inline>
          <Field
            id={seed('checkboxesInline-two')}
            name="checkboxesInline"
            value="two"
            type="checkbox"
            component={InputAdapter}
          />
          <Label check htmlFor={seed('checkboxesInline-two')}>
            Checked
          </Label>
        </FormGroup>
        <FormGroup check inline>
          <Field
            id={seed('checkboxesInline-disabled')}
            name="checkboxesInline"
            value="disabled"
            disabled
            type="checkbox"
            component={InputAdapter}
          />
          <Label check htmlFor={seed('checkboxesInline-three')}>
            Disabled
          </Label>
        </FormGroup>
        <FormFeedbackAdapter name="checkboxesInline" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Radios (inline) */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">Radios (inline)</legend>
        <FormGroup check inline>
          <Field
            id={seed('radiosInline-one')}
            name="radiosInline"
            value="one"
            type="radio"
            component={InputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={requireOne}
          />
          <Label check htmlFor={seed('radiosInline-one')}>
            Unselected
          </Label>
        </FormGroup>
        <FormGroup check inline>
          <Field
            id={seed('radiosInline-two')}
            name="radiosInline"
            value="two"
            type="radio"
            component={InputAdapter}
          />
          <Label check htmlFor={seed('radiosInline-two')}>
            Selected
          </Label>
        </FormGroup>
        <FormGroup check inline>
          <Field
            id={seed('radiosInline-disabled')}
            name="radiosInline"
            value="disabled"
            disabled
            type="radio"
            component={InputAdapter}
          />
          <Label check htmlFor={seed('radiosInline-disabled')}>
            Disabled
          </Label>
        </FormGroup>
        <FormFeedbackAdapter name="radiosInline" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Custom Single Checkbox */}
      <FormGroup>
        <Field
          label={
            <>
              Single <strong>custom</strong> checkbox with a quite a bit of text
              that will likely wrap so you can check that out on your screen.
            </>
          }
          id={seed('customCheckbox')}
          name="customCheckbox"
          type="checkbox"
          component={CustomInputAdapter}
          validate={validateRequired()}
        >
          {/* FormFeedback must be child of single custom checkbox */}
          <FormFeedbackAdapter name="customCheckbox" />
        </Field>
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      <Row>
        <Col md={6}>
          {/* Custom Checkboxes */}
          <FormGroup tag="fieldset">
            <legend className="legend-label">Custom Checkboxes</legend>
            <div>
              <Field
                label="Checkbox Unchecked"
                id={seed('customCheckboxes-one')}
                name="customCheckboxes"
                value="one"
                type="checkbox"
                component={CustomInputAdapter}
                // checkbox/radio groups only need validation in first field of group
                validate={requireOne}
              />
              <Field
                label="Checkbox Checked"
                id={seed('customCheckboxes-two')}
                name="customCheckboxes"
                value="two"
                type="checkbox"
                component={CustomInputAdapter}
              />
              <Field
                label="Checkbox Disabled"
                id={seed('customCheckboxes-three')}
                name="customCheckboxes"
                value="disabled"
                disabled
                type="checkbox"
                component={CustomInputAdapter}
              >
                {/* FormFeedback must be child of last field of stacked custom checkboxes */}
                <FormFeedbackAdapter name="customCheckboxes" />
              </Field>
            </div>
          </FormGroup>
        </Col>
        <Col md={6}>
          {/* Custom Radios */}
          <FormGroup tag="fieldset">
            <legend className="legend-label">Custom Radios</legend>
            <div>
              <Field
                label="Radio Unselected"
                id={seed('customRadios-one')}
                name="customRadios"
                value="one"
                type="radio"
                component={CustomInputAdapter}
                // checkbox/radio groups only need validation in first field of group
                validate={requireOne}
              />
              <Field
                label="Radio Selected"
                id={seed('customRadios-two')}
                name="customRadios"
                value="two"
                type="radio"
                component={CustomInputAdapter}
              />
              <Field
                label="Radio Disabled"
                id={seed('customRadios-disabled')}
                name="customRadios"
                value="disabled"
                disabled
                type="radio"
                component={CustomInputAdapter}
              >
                {/* FormFeedback must be child of last field of stacked custom radios */}
                <FormFeedbackAdapter name="customRadios" />
              </Field>
            </div>
          </FormGroup>
        </Col>
      </Row>

      <hr className="border-top-dashed my-40" />

      {/* Custom Checkboxes (inline) */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">Custom Checkboxes (inline)</legend>
        <div>
          <Field
            inline
            label="Unchecked"
            id={seed('customCheckboxesInline-one')}
            name="customCheckboxesInline"
            value="one"
            type="checkbox"
            component={CustomInputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={requireOne}
          />
          <Field
            inline
            label="Checked"
            id={seed('customCheckboxesInline-two')}
            name="customCheckboxesInline"
            value="two"
            type="checkbox"
            component={CustomInputAdapter}
          />
          <Field
            inline
            label="Disabled"
            id={seed('customCheckboxesInline-disabled')}
            name="customCheckboxesInline"
            value="disabled"
            disabled
            type="checkbox"
            component={CustomInputAdapter}
          />
        </div>
        <FormFeedbackAdapter name="customCheckboxesInline" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Custom Radios (inline) */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">Custom Radios (inline)</legend>
        <div>
          <Field
            inline
            label="Unselected"
            id={seed('customRadiosInline-one')}
            name="customRadiosInline"
            value="one"
            type="radio"
            component={CustomInputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={requireOne}
          />
          <Field
            inline
            label="Selected"
            id={seed('customRadiosInline-two')}
            name="customRadiosInline"
            value="two"
            type="radio"
            component={CustomInputAdapter}
          />
          <Field
            inline
            label="Disabled"
            id={seed('customRadiosInline-disabled')}
            name="customRadiosInline"
            value="disabled"
            disabled
            type="radio"
            component={CustomInputAdapter}
          />
        </div>
        <FormFeedbackAdapter name="customRadiosInline" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Single Switch */}
      <FormGroup>
        <Field
          label="Single Toggle with a quite a bit of text that will likely wrap so
          you can check that out on your screen."
          id={seed('customSwitch')}
          name="customSwitch"
          type="checkbox" // final-form needs to be told switches work as checkboxes
          customType="switch" // additional prop to tell Reactstrap to render switch instead of checkbox
          component={CustomInputAdapter}
          validate={validateRequired()}
        >
          <FormFeedbackAdapter name="customSwitch" />
        </Field>
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Custom Switches */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">
          Custom Switches (Checkbox Toggles)
        </legend>
        <div>
          <Field
            label="Toggle Unselected"
            id={seed('customSwitches-one')}
            name="customSwitches"
            value="one"
            type="checkbox" // final-form needs to be told switches work as checkboxes
            customType="switch" // additional prop to tell Reactstrap to render switch instead of checkbox
            component={CustomInputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={requireOne}
          />
          <Field
            label="Toggle Selected"
            id={seed('customSwitches-two')}
            name="customSwitches"
            value="two"
            type="checkbox"
            customType="switch"
            component={CustomInputAdapter}
          />
          <Field
            label="Toggle Disabled"
            id={seed('customSwitches-disabled')}
            name="customSwitches"
            value="disabled"
            disabled
            type="checkbox"
            customType="switch"
            component={CustomInputAdapter}
          >
            {/* FormFeedback must be child of last field in stack of custom switches */}
            <FormFeedbackAdapter name="customSwitches" />
          </Field>
        </div>
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Custom Switches (inline) */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">Custom Switches (inline)</legend>
        <div>
          <Field
            inline
            label="Unselected"
            id={seed('customSwitchesInline-one')}
            name="customSwitchesInline"
            value="one"
            type="checkbox" // final-form needs to be told switches work as checkboxes
            customType="switch" // additional prop to tell Reactstrap to render switch instead of checkbox
            component={CustomInputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={requireOne}
          />
          <Field
            inline
            label="Selected"
            id={seed('customSwitchesInline-two')}
            name="customSwitchesInline"
            value="two"
            type="checkbox"
            customType="switch"
            component={CustomInputAdapter}
          />
          <Field
            inline
            label="Disabled"
            id={seed('customSwitchesInline-disabled')}
            name="customSwitchesInline"
            value="disabled"
            disabled
            type="checkbox"
            customType="switch"
            component={CustomInputAdapter}
          />
        </div>
        <FormFeedbackAdapter name="customSwitchesInline" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Advanced Select with number ids and context colors */}
      <FormGroup>
        <Label htmlFor={seed('customSelectAdvanced')}>
          Advanced Select Options
        </Label>
        {fieldWrapSelectContextDot(
          'customSelectAdvanced',
          idNumsAndContextOptions
        )(
          <Field
            custom
            id={seed('customSelectAdvanced')}
            name="customSelectAdvanced"
            type="select"
            component={SelectAdapter}
            options={idNumsAndContextOptions}
            placeholder="Select a Bootstrap Context"
            validate={validateRequired()}
            // keep ids as numbers
            parse={parseStrToNum()}
            format={formatNumToStr()}
          />
        )}
        <FormText>
          <ul className="mb-0">
            <li>Parse/Format keeps id data as numbers.</li>
            <li>
              Bootstrap context color data as <code>contextColor</code> property
              in option data.
            </li>
            <li>Context colored indicator using HoC wrapping technique.</li>
            <li>
              Icon has configurable size while spacing can be controlled in{' '}
              <code>fieldWrapSelectContextDot</code> component.
            </li>
          </ul>
        </FormText>
        <FormFeedbackAdapter name="customSelectAdvanced" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* USD Amount with number-only entry */}
      <FormGroup>
        <Label htmlFor={seed('amountUSDCents')}>Enter Amount</Label>
        <Field
          id={seed('amountUSDCents')}
          name="amountUSDCents"
          type="text"
          placeholder="$0.00"
          component={InputAdapter}
          validate={validateRequired()}
          format={formatCentsToUSDString}
          parse={parseUSDStringToCents()}
          onClick={event => moveCursorToEnd(event.target)}
          onFocus={event => moveCursorToEnd(event.target)}
        />
        <FormText>
          This input uses parse/format and some additional onClick/Focus cursor
          moving functions emulate the money entry UI for Paypal and others.
          Parse function has option to allow zero.
        </FormText>
        <FormFeedbackAdapter name="amountUSDCents" />
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Custom Yes/No Radios */}
      <FormGroup tag="fieldset">
        <legend className="legend-label">Trigger Submit Error?</legend>
        <div>
          <Field
            label="Yes"
            id={seed('customRadio-yes')}
            name="triggerSubmitError"
            value="Yes"
            type="radio"
            component={CustomInputAdapter}
            // checkbox/radio groups only need validation in first field of group
            validate={validateRequiredBool()}
            // use yes/no values but store in FF as true/false via parse/format
            parse={parseStringToBool()}
            format={formatBoolToString()}
          />
          <Field
            label="No"
            id={seed('customRadio-no')}
            name="triggerSubmitError"
            value="No"
            type="radio"
            component={CustomInputAdapter}
            // use yes/no values but store in FF as true/false via parse/format
            parse={parseStringToBool()}
            format={formatBoolToString()}
          >
            <FormText>
              This example shows how to make yes/no radios that use true/false
              as as their data even though values have to strings. Take note of
              the validation and use of parse/format.
            </FormText>
            {/* FormFeedback must be child of last field of stacked custom radios */}
            <FormFeedbackAdapter name="triggerSubmitError" />
          </Field>
        </div>
      </FormGroup>

      <hr className="border-top-dashed my-40" />

      {/* Alert for client-side errors */}
      <ValidationErrorAlert msg="This is a general alert that shows when submit is attempted but is prevented by field-level validation errors. For long forms this helps prompt the user that there are errors above the top fold. Usually not needed if you are using final-form-focus. Default message is: Please fix the highlighted errors above." />

      {/* Alerts for Submit Errors */}
      <SubmitErrorAlerts />

      {/* submitSucceeded can be used for success logic including replacing the form with content */}
      <Alert color="success" isOpen={submitSucceeded}>
        Form successfully submitted
      </Alert>

      <Button
        block
        size="lg"
        color="success"
        disabled={submitting}
        type="submit"
      >
        Submit and View Values
      </Button>

      <FormValuesControls className="mt-20" />

      <h6 className="mt-40">FormSpy of Values</h6>
      <FormValuesTool />
    </Form>
  )
}

export default FormElementsForm
