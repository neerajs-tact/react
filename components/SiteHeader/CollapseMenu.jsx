import React from 'react'
import { bool, node } from 'prop-types'
import { Collapse, Nav } from 'reactstrap'

CollapseMenu.propTypes = {
  children: node.isRequired,
  menuIsOpen: bool.isRequired,
}

function CollapseMenu({ children, menuIsOpen }) {
  return (
    <Collapse isOpen={menuIsOpen} navbar>
      {/* Desktop Divider */}
      <hr className="d-none ml-auto d-hd-block hr-vr hr-vr-lg mr-30" />
      {/* Menu (ul) */}
      <Nav
        className="mt-40 border-top pt-20 mt-hd-0 border-hd-top-0 pt-hd-0 align-items-hd-center"
        navbar
      >
        {children}
      </Nav>
    </Collapse>
  )
}

export default CollapseMenu
