import React from 'react'
import { bool, func, node, string } from 'prop-types'
import {
  Container,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  NavItem,
  NavLink,
} from 'reactstrap'
import CollapseMenu from './CollapseMenu'
import SlideMenu from './SlideMenu'

import { Link } from '../../routes'
import LinkActiveNavLink from '../LinkActiveNavLink'
import OnClickKeySupport from '../OnClickKeySupport'
import { LogoLg } from '../logos'

SiteHeaderView.propTypes = {
  isSignedIn: bool.isRequired,
  menuIsOpen: bool.isRequired,
  navbarDark: bool,
  navbarDarkColor: string,
  navbarLightColor: string,
  navbarSlideOut: bool,
  onSignOut: func.isRequired,
  onMenuToggle: func.isRequired,
}

SiteHeaderView.defaultProps = {
  navbarLightColor: 'white', // can be white or context names
  navbarDark: false,
  navbarDarkColor: 'dark', // can be white or context names
  navbarSlideOut: true,
}

/**
 * It is recommended you leave the expand prop set to 'hd' which is a SCSS
 * configurable breakpoint for this header's mobile to desktop layout switching.
 */
function SiteHeaderView({
  isSignedIn,
  onSignOut,
  menuIsOpen,
  navbarLightColor,
  navbarDark,
  navbarDarkColor,
  navbarSlideOut,
  onMenuToggle,
}) {
  const navbarColor = navbarDark ? navbarDarkColor : navbarLightColor

  const isSignedOut = !isSignedIn

  return (
    <header className="border-bottom">
      <Navbar
        expand="hd"
        light={!navbarDark}
        dark={navbarDark}
        color={navbarColor}
      >
        <Container fluid>
          {/* Linked Brand Logo */}
          <Link route={isSignedIn ? 'dashboard' : 'home'} passHref>
            <NavbarBrand className="text-primary mr-cp-sm mr-md-cp-lg">
              <LogoLg />
              <span className="sr-only">
                {isSignedIn ? 'Dashboard' : 'Home'}
              </span>
            </NavbarBrand>
          </Link>
          {/* Mobile Menu Toggle */}
          <NavbarToggler
            onClick={onMenuToggle}
            className="uaTrack-mobile-nav-toggle border-0"
            aria-label="Open Mobile Menu"
          />
          {/* Mobile Menu Mode Wrapper */}
          <CollapseOrSlide
            shouldSlide={navbarSlideOut}
            renderSlide={children => (
              <SlideMenu
                menuIsOpen={menuIsOpen}
                navbarColor={navbarColor}
                onMenuToggle={onMenuToggle}
              >
                {children}
              </SlideMenu>
            )}
            renderCollapse={children => (
              <CollapseMenu menuIsOpen={menuIsOpen} navbarColor={navbarColor}>
                {children}
              </CollapseMenu>
            )}
          >
            {/* Nav Items */}
            {isSignedOut && (
              <>
                <NavItem>
                  <LinkActiveNavLink
                    route="login"
                    className="uaTrack-header-nav-log-in"
                  >
                    Log In
                  </LinkActiveNavLink>
                </NavItem>
                <NavItem>
                  <LinkActiveNavLink
                    route="register"
                    className="uaTrack-header-nav-register"
                  >
                    Register
                  </LinkActiveNavLink>
                </NavItem>
              </>
            )}
            {isSignedIn && (
              <>
                <NavItem>
                  <LinkActiveNavLink
                    route="your-account"
                    className="uaTrack-header-nav-your-account"
                  >
                    Your Account
                  </LinkActiveNavLink>
                </NavItem>
                <NavItem>
                  <OnClickKeySupport onClick={onSignOut}>
                    <NavLink
                      className="cursor-pointer uaTrack-header-nav-logout"
                      role="link"
                    >
                      Log Out
                    </NavLink>
                  </OnClickKeySupport>
                </NavItem>
              </>
            )}
          </CollapseOrSlide>
        </Container>
      </Navbar>
    </header>
  )
}

CollapseOrSlide.propTypes = {
  shouldSlide: bool.isRequired,
  renderSlide: func.isRequired,
  renderCollapse: func.isRequired,
  children: node.isRequired,
}

function CollapseOrSlide({
  shouldSlide,
  renderSlide,
  renderCollapse,
  children,
}) {
  return shouldSlide ? renderSlide(children) : renderCollapse(children)
}

export default SiteHeaderView
