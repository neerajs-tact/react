import React, { useEffect } from 'react'
import { bool, func, node, string } from 'prop-types'
import classNames from 'classnames'
// eslint-disable-next-line import/no-extraneous-dependencies
import scss from 'styled-jsx/css'
import { Nav } from 'reactstrap'
import { CSSTransition } from 'react-transition-group'

SlideMenu.propTypes = {
  children: node.isRequired,
  menuIsOpen: bool.isRequired,
  navbarColor: string.isRequired,
  onMenuToggle: func.isRequired,
}

/* language=SCSS */
const styles = scss`

  @import "jsx-helper";
  
  // design config
  $slide-close-bar-bg: $primary;
  $slide-close-bar-width: rems(64); // 44px or greater is recommended for touch targeting
  
  $slide-close-x-font-family: $font-family-base;
  $slide-close-x-font-size: rems(56);
  $slide-close-x-font-weight: normal;
  $slide-close-x-color: $white;
  
  // mobile only
  @include media-breakpoint-down(md) { // reverse (down to mobile) breakpoint is inclusive so we must use breakpoint BEFORE hd
    
    .slide-menu {
      position: fixed;
      display: flex;
      flex-direction: row;
      align-items: stretch;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      z-index: $zindex-fixed;
      will-change: transform;
      transform: translateX(102%); // a little extra to keep close button focus outline out of view
    }
    
    // open state
    .slide-menu--open {
      transform: translateX(0);
    }
    
    .slide-menu__close-bar {
      background-color: $slide-close-bar-bg;
    }
    
    .slide-menu__close-btn {
      width: $slide-close-bar-width;
      height: $slide-close-bar-width;
      line-height: $slide-close-bar-width;
      padding: 0;
      background-color: transparent;
      border: none;
      font-family: $slide-close-x-font-family;
      font-size: $slide-close-x-font-size;
      font-weight: $slide-close-x-font-weight;
      color: $slide-close-x-color;
      overflow: hidden; // prevents large font size from deforming focus outline
    }
  }
`

/* language=SCSS */
const globalStyles = scss.global`

  @import "jsx-helper";

  // animation config
  $slide-animation-speed: 300ms; // if changed change timeout prop in CSSTransition to match
  $slide-animation-open-easing: ease-out;
  $slide-animation-close-easing: ease-in;
  
  // mobile only
  @include media-breakpoint-down(md) { // breakpoint before hd
    
    // prevent body scrolling when slide menu is open (like Bootstrap modals)
    .bodyLock-slide-menu {
      overflow: hidden;
    }

    // react-transition-group via CSSTransition
    .slide-menu { // match specificity of styled-jsx to override without !important
      
      // opening animation from
      &.SlideMenuAnimate-enter {
        transform: translateX(102%);
      }

      // opening animation to
      &.SlideMenuAnimate-enter-active {
        transition: transform $slide-animation-speed $slide-animation-open-easing;
        transform: translateX(0);
      }

      // closing animation from
      &.SlideMenuAnimate-exit {
        transform: translateX(0);
      }

      // closing animation to
      &.SlideMenuAnimate-exit-active {
        transition: transform $slide-animation-speed $slide-animation-close-easing;
        transform: translateX(102%);
      }
    }
  }
`

function SlideMenu({ children, menuIsOpen, navbarColor, onMenuToggle }) {
  // body locking class when menu is open
  useEffect(() => {
    bodyLock(menuIsOpen)
  }, [menuIsOpen])

  // cleanup body locking class on unmount
  useEffect(() => {
    return () => bodyLock(false)
  }, [])

  return (
    <CSSTransition in={menuIsOpen} timeout={300} classNames="SlideMenuAnimate">
      <div
        className={classNames(
          'navbar-collapse',
          'slide-menu',
          `bg-${navbarColor}`,
          { 'slide-menu--open': menuIsOpen }
        )}
      >
        <div className="slide-menu__close-bar d-block d-hd-none">
          <button
            color="link"
            aria-label="Close Menu"
            type="button"
            className="slide-menu__close-btn d-flex justify-content-center align-items-center"
            onClick={onMenuToggle}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {/* Desktop Divider */}
        <hr className="d-none ml-auto d-hd-block hr-vr hr-vr-lg mr-30" />
        {/* Menu (ul) */}
        <Nav
          className="overflow-auto flex-grow-1 flex-hd-grow-0 align-items-hd-center overflow-hd-visible py-10 px-30 p-hd-0"
          navbar
        >
          {children}
        </Nav>
        <style jsx>{styles}</style>
        <style jsx global>
          {globalStyles}
        </style>
      </div>
    </CSSTransition>
  )
}

/**
 * Applies or Removes body locking CSS class
 *
 * @param {boolean} setLock - Desired state of body lock.
 */
function bodyLock(setLock) {
  const bodyClassList = document.body.classList
  const lockClass = 'bodyLock-slide-menu'
  return setLock
    ? bodyClassList.add(lockClass)
    : bodyClassList.remove(lockClass)
}

export default SlideMenu
