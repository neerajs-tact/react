import { bool, func } from 'prop-types'
import React, { useCallback, useState } from 'react'
import { connect } from 'react-redux'
import { useAuthUser, useIsMounted } from '../../hooks'
import { signOut } from '../../modules/users'
import SiteHeaderView from './SiteHeaderView'

SiteHeader.propTypes = {
  signOut: func.isRequired,
  navbarDark: bool.isRequired,
}

const mapDispatch = { signOut }

function SiteHeader({ navbarDark, signOut }) {
  const authUser = useAuthUser()
  const isMounted = useIsMounted()
  const [menuIsOpen, setMenuIsOpen] = useState(false)

  const handleMenuToggle = useCallback(
    function handleBootstrapMenuToggle() {
      if (isMounted()) setMenuIsOpen(!menuIsOpen)
    },
    [isMounted, menuIsOpen]
  )

  return (
    <SiteHeaderView
      isSignedIn={!!authUser}
      menuIsOpen={menuIsOpen}
      onSignOut={signOut}
      onMenuToggle={handleMenuToggle}
      navbarDark={navbarDark}
    />
  )
}

export default connect(
  null,
  mapDispatch
)(SiteHeader)
