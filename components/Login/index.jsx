import React, { useCallback } from 'react'
import { NextAuth } from 'next-auth/client'
import { FORM_ERROR } from 'final-form'
import { connect } from 'react-redux'
import { Form } from 'react-final-form'
import createDecorator from 'final-form-focus'
import { signIn } from '../../modules/users'
import { Router } from '../../routes'
import LoginForm from './LoginForm'

// focus on first error
const focusOnError = createDecorator()

const mapDispatch = { signIn }

function Login() {
  const onSubmit = useCallback(async function onLoginFormSubmit(values) {
    try {
      await NextAuth.signin(values)
      Router.pushRoute('auth-callback')
      return undefined
    } catch (error) {
      // next-auth can't return server error so we define a catch all for usual cases.
      return { [FORM_ERROR]: 'Invalid email address or password.' }
    }
  }, [])

  return (
    <Form
      component={LoginForm}
      decorators={[focusOnError]}
      onSubmit={onSubmit}
      // restricts which parts of form state the form component needs to render
      subscription={{
        submitSucceeded: true,
        submitting: true,
      }}
    />
  )
}

export default connect(
  null,
  mapDispatch
)(Login)
