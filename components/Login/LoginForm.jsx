import React from 'react'
import { bool, func } from 'prop-types'
import { Button, Form } from 'reactstrap'
import { useUIDSeed } from 'react-uid'
import { validateEmail, validateRequired } from '../../lib/ff'
import { FFormGroup, SubmitErrorAlerts } from '../ff'

LoginForm.propTypes = {
  handleSubmit: func.isRequired,
  submitSucceeded: bool.isRequired,
  submitting: bool.isRequired,
}

function LoginForm({ handleSubmit, submitSucceeded, submitting }) {
  const seed = useUIDSeed()

  return (
    <Form onSubmit={handleSubmit}>
      <FFormGroup
        label="Email Address"
        name="email"
        type="email"
        seed={seed}
        validators={[validateRequired(), validateEmail()]}
        autoComplete="username email"
        inputMode="email" // https://css-tricks.com/everything-you-ever-wanted-to-know-about-inputmode/
      />
      <FFormGroup
        label="Password"
        name="password"
        type="password"
        seed={seed}
        validators={[validateRequired()]}
        autoComplete="current-password"
      />
      <SubmitErrorAlerts />
      <Button
        block
        color="primary"
        className="uaTrack-submit-login"
        disabled={submitting || submitSucceeded}
        size="lg"
        type="submit"
      >
        {submitting || submitSucceeded ? 'Logging In...' : 'Log In'}
      </Button>
    </Form>
  )
}

export default LoginForm
