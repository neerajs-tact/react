import React from 'react'
import { bool, node, oneOf, string } from 'prop-types'
import classNames from 'classnames'
// eslint-disable-next-line import/no-extraneous-dependencies
import scss from 'styled-jsx/css'
import SiteHeader from './SiteHeader'
import SiteFooter from './SiteFooter'

Layout.propTypes = {
  bodyStyle: oneOf(['default', 'center-xy']),
  children: node.isRequired,
  // turns on footer navbar dark mode and applies fnavbarDarkColor default in <SiteFooter/>
  fnavbarDark: bool,
  // use to replace default utilities, good for changing padding on specific pages
  mainClassNames: string,
  // turns on navbar dark mode and applies navbarDarkColor default in <SiteHeader/>
  navbarDark: bool,
  hideFooter: bool,
  hideHeader: bool,
}

Layout.defaultProps = {
  bodyStyle: 'default',
  fnavbarDark: false,
  mainClassNames: 'py-60', // default <main/> y padding
  navbarDark: false,
  hideHeader: false,
  hideFooter: false,
}

/* language=SCSS */
const globalStyles = scss.global`

  // Setup <main> to fill available browser height keeping footer at bottom

  html, body {
    height: 100%;
  }

  #__next {
    height:         100%;
    display:        flex;
    flex-direction: column;
  }
`

/* language=SCSS */
const styles = scss`

  @import "jsx-helper";

  .page__main {
    flex: 1 0 auto; // footer sticks to bottom of short pages
  }

  // 'center-xy' Layout
  // centers content vertically and horizontally in <main>
  .page__main--center-xy {
    display:         flex;
    justify-content: center;
    align-items:     center;
  }
`

function Layout({
  bodyStyle,
  children,
  hideHeader,
  hideFooter,
  fnavbarDark,
  mainClassNames,
  navbarDark,
}) {
  return (
    <>
      {!hideHeader && <SiteHeader navbarDark={navbarDark} />}
      <main
        role="main"
        className={classNames(
          'page__main',
          `page__main--${bodyStyle}`,
          mainClassNames
        )}
      >
        {children}
      </main>
      {!hideFooter && <SiteFooter fnavbarDark={fnavbarDark} />}
      <style jsx global>
        {globalStyles}
      </style>
      <style jsx>{styles}</style>
    </>
  )
}

export default Layout
