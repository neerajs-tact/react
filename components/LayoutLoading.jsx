/**
 * Loading animation screen to use in place of <Layout>
 */

import React from 'react'
// eslint-disable-next-line import/no-extraneous-dependencies
import scss from 'styled-jsx/css'

/* language=SCSS */
const globalStyles = scss.global`

  // Setup <main> to fill available browser height

  html, body {
    height:   100%;
  }

  #__next {
    height:         100%;
    display:        flex;
    flex-direction: column;
  }
`

/* language=SCSS */
const styles = scss`
  @import "jsx-helper";
  
  .page__main {
    flex: 1 0 auto;
  }
  
  // center content vertically and horizontally
  .page__main--loader {
    display:         flex;
    justify-content: center;
    align-items:     center;
  }

  // loading animation (pulse)

  $pulse-color: $primary;

  .loading__pulse {
    background-color:          $pulse-color;
    width:                     100px;
    height:                    100px;
    border-radius:             50%;
    transform:                 translateZ(0); // for animation performance
    animation-name:            loading__pulse;
    animation-duration:        1000ms;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
  }

  @keyframes loading__pulse {

    from {
      opacity:   1;
      transform: scale(0.01);
    }

    to {
      opacity:   0;
      transform: scale(1);
    }
  }
`

function LayoutLoading() {
  return (
    <main role="main" className="page__main page__main--loader py-60">
      <div className="loading__pulse">
        <span className="sr-only">Loading.</span>
      </div>
      <style jsx global>
        {globalStyles}
      </style>
      <style jsx>{styles}</style>
    </main>
  )
}

export default LayoutLoading
