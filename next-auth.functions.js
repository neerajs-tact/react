const { camelizeKeys } = require('humps')
const fetch = require('isomorphic-unfetch')
const url = require('url')
const { requireEnvVar } = require('./lib/nodeUtils')

const API_VERSION = 'v1'
const apiRoot = url.resolve(requireEnvVar('API_SERVER'), `/api/${API_VERSION}/`)

const callAPI = (endpoint, options = {}) => {
  const url = apiRoot + endpoint

  return fetch(url, {
    ...options,
    headers: { 'Content-Type': 'application/json' },
  })
}

module.exports = async () => ({
  serialize: async user => user || null,

  deserialize: async user => user || null,

  signIn: async ({ form }) => {
    if (form.user) {
      // User was provided directly; just update the session data.
      // See `updateAuthUser` in `lib/auth.jsx`
      return camelizeKeys(JSON.parse(form.user))
    }

    const response = await callAPI('users/sign_in', {
      method: 'POST',
      body: JSON.stringify({ user: form }),
    })
    const json = await response.json()
    if (response.ok) return camelizeKeys(json)
    throw json
  },
})
