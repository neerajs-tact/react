# Starter Web App

## Requirements

To run development mode or export the app for hosting, you'll need the following:

- [Node](https://nodejs.org/en/) 10.15.x or later
  (set in [.node-version](.node-version))
- [NPM CLI](https://www.npmjs.com/get-npm) 6.4.1 or later

If installing Node via [nodenv](https://github.com/nodenv/nodenv), you can use
[nodenv/jetbrains-npm](https://github.com/nodenv/jetbrains-npm) for WebStorm support.

## Setup

Install the required Node packages:

```
npm ci
```

### Environment Variables

Required environment variables are listed in [`.env.example`](.env.example).
You can either set these in your environment or your IDE, or you can copy
`.env.example` to a new file named `.env` which will be loaded automatically by
[dotenv](https://github.com/motdotla/dotenv) when running the dev server.

## Development

To run the Next.js dev server:

```
npm run dev
```

## Backend Setup

You can get the [Starter Backend](https://github.com/techelogy/starter-rails-backend)
up and running by following the instructions in its README.

Alternatively, you can point the `API_SERVER` environment variable to a staging
deployment running on Heroku:

```
API_SERVER=https://starter-rails-backend-staging.herokuapp.com
```

## Continuous Integration

Github CI is setup in `.github/workflows/continous-integration.yml` file.
It runs Linter check for pull requests.

### Deploy

We are using Heroku CLI to deploy code from development system to staging/production servers.
Install Heroku CLI
https://devcenter.heroku.com/articles/heroku-cli

`develop` branch to be deployed to staging server.
`master` branch to be deployed to production server.

Set heroku staging in git config
`heroku git:remote -a starter-react-web-staging`
Deploy
`git push heroku develop:master`

Set heroku production in git config
`git remote add heroku-production https://git.heroku.com/starter-react-web.git`
Deploy
`git checkout master`
`git push heroku-production`

Not using automatic deploys and Heroku pipeline intentionally.
To have more control and single interface.
