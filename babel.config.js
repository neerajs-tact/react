/**
 * Custom Babel Config
 * @see https://nextjs.org/docs#customizing-babel-config
 */
module.exports = {
  plugins: ['lodash', '@babel/plugin-proposal-export-default-from'],
  presets: [
    [
      'next/babel',
      {
        'styled-jsx': {
          plugins: [
            [
              'styled-jsx-plugin-sass',
              {
                sassOptions: {
                  includePaths: ['./styles'],
                },
              },
            ],
            [
              'styled-jsx-plugin-postcss',
              {
                path: './postcss.config.js',
              },
            ],
          ],
        },
      },
    ],
  ],
}
