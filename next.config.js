const { withPlugins, optional } = require('next-compose-plugins')
// eslint-disable-next-line import/no-extraneous-dependencies
const { PHASE_PRODUCTION_BUILD } = require('next-server/constants')
const sass = require('@zeit/next-sass')
const { requireEnvVar } = require('./lib/nodeUtils')

/**
 * Custom Next.js Configuration
 * @see https://nextjs.org/docs#custom-configuration
 */
const nextConfig = {
  /**
   * Runtime configuration
   * @see https://nextjs.org/docs#exposing-configuration-to-the-server--client-side
   */
  publicRuntimeConfig: {
    // Will be available on both server and client
    apiUrl: requireEnvVar('API_SERVER'),
    gtmId: requireEnvVar('GTM_ID'),
  },
  /**
   * Disable file-system routing
   * @see https://nextjs.org/docs#disabling-file-system-routing
   */
  useFileSystemPublicRoutes: false,
  /**
   * Custom webpack config
   * @see https://nextjs.org/docs#customizing-webpack-config
   */
  webpack(config, { webpack }) {
    /**
     * Only load specific locales for moment.js
     * @see https://stackoverflow.com/a/25426019/956688
     */
    config.plugins.push(
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/)
    )

    return config
  },
}

/**
 * Load multiple plugins with next-compose-plugins
 * @see https://github.com/cyrilwanner/next-compose-plugins
 */
module.exports = withPlugins(
  [
    [sass],
    /**
     * Analyzing the webpack bundle
     * @see https://github.com/zeit/next-plugins/tree/master/packages/next-bundle-analyzer
     *
     * Load @zeit/next-bundle-analyzer as an optional plugin only during production build
     * @see https://github.com/cyrilwanner/next-compose-plugins#optional-plugins
     * @see https://github.com/cyrilwanner/next-compose-plugins#phases-array
     */
    [
      // eslint-disable-next-line global-require,import/no-extraneous-dependencies
      optional(() => require('@zeit/next-bundle-analyzer')),
      {
        analyzeServer: ['server', 'both'].includes(process.env.BUNDLE_ANALYZE),
        analyzeBrowser: ['browser', 'both'].includes(
          process.env.BUNDLE_ANALYZE
        ),
        bundleAnalyzerConfig: {
          server: {
            analyzerMode: 'static',
            reportFilename: '../../bundles/server.html',
          },
          browser: {
            analyzerMode: 'static',
            reportFilename: '../bundles/client.html',
          },
        },
      },
      [PHASE_PRODUCTION_BUILD],
    ],
  ],
  nextConfig
)
