import NextSeo from 'next-seo'
import React from 'react'
import { string } from 'prop-types'
import { Alert, Container } from 'reactstrap'
import ResetPassword from '../components/ResetPassword'
import Layout from '../components/Layout'

ResetPasswordPage.propTypes = {
  resetToken: string,
}

ResetPasswordPage.defaultProps = {
  resetToken: null,
}

ResetPasswordPage.getInitialProps = ({ query }) => ({
  resetToken: query.reset_password_token,
})

function ResetPasswordPage({ resetToken }) {
  return (
    <Layout bodyStyle="center-xy">
      <NextSeo config={{ title: 'Reset Password' }} />
      <Container fluid>
        <div className="w-form-narrow mw-100 mx-auto">
          <h1 className="text-center h3">Reset Password</h1>
          <hr className="border-top-dashed my-30" />
          {resetToken ? (
            <ResetPassword resetToken={resetToken} />
          ) : (
            <Alert color="danger">
              <strong>Missing reset token.</strong> Please click the link from
              the last password reset email you received to reset your password.
            </Alert>
          )}
        </div>
      </Container>
    </Layout>
  )
}

export default ResetPasswordPage
