/**
 * Custom Next.js Document
 *
 * _document is only rendered on the server side and not on the client side.
 * @see https://nextjs.org/docs#custom-document
 */

import Document, { Html, Head, Main, NextScript } from 'next/document'
import React from 'react'
import { GTMNoScript } from '../components/head'

class CustomDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="en">
        {
          // to keep <Head/> items de-duped we should use next/head in _app.jsx
          // @see https://github.com/zeit/next.js/issues/6919
        }
        <Head />
        <body>
          <GTMNoScript />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default CustomDocument
