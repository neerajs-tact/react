import React from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { func, string } from 'prop-types'
import NextSeo from 'next-seo'
import { Button, Alert, Container } from 'reactstrap'
import { retrieveCurrentUser, signOut } from '../../modules/users'
import { requireAuth } from '../../lib/auth'
import PersonalInfo from '../../components/PersonalInfo'
import ChangePassword from '../../components/ChangePassword'
import Layout from '../../components/Layout'
import UrlQueryAlert from '../../components/UrlQueryAlert'

YourAccountPage.propTypes = {
  personalInfoError: string,
  signOut: func.isRequired,
}

YourAccountPage.defaultProps = {
  personalInfoError: null,
}

YourAccountPage.getInitialProps = async ({ store }) => {
  // load content before page is displayed
  try {
    await store.dispatch(retrieveCurrentUser())
  } catch (error) {
    return {
      personalInfoError: `Error fetching personal info. ${error.error}.`,
    }
  }
  return {}
}

const mapDispatch = { signOut }

function YourAccountPage({ personalInfoError, signOut }) {
  return (
    <Layout>
      <NextSeo config={{ title: 'Your Account' }} />
      <Container fluid>
        <h1 className="sr-only">Your Account</h1>
        <div className="w-form mw-100 mx-auto mb-70">
          <UrlQueryAlert className="text-center" />
          <h2 className="h3 text-center text-sm-left">Personal Info</h2>
          <hr className="border-top-dashed" />
          {personalInfoError && (
            <Alert color="danger">{personalInfoError}</Alert>
          )}
          <PersonalInfo />
        </div>
        <div className="w-form mw-100 mx-auto">
          <h2 className="h3 text-center text-sm-left">Change Password</h2>
          <hr className="border-top-dashed" />
          <ChangePassword />
          <hr className="border-top-dashed mt-40 mb-10" />
          <Button
            color="link"
            block
            size="lg"
            className="uaTrack-sign-out"
            onClick={signOut}
          >
            Log Out
          </Button>
        </div>
      </Container>
    </Layout>
  )
}

export default compose(
  requireAuth,
  connect(
    null,
    mapDispatch
  )
)(YourAccountPage)
