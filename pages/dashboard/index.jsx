import NextSeo from 'next-seo'
import { func, shape, string } from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { Button, Container } from 'reactstrap'
import Layout from '../../components/Layout'
import { requireAuth } from '../../lib/auth'
import { retrieveCurrentUser } from '../../modules/users'

const mapDispatch = { retrieveCurrentUser }

DashboardPage.propTypes = {
  authUser: shape({
    firstName: string.isRequired,
  }).isRequired,
  retrieveCurrentUser: func.isRequired,
}

function DashboardPage({ authUser, retrieveCurrentUser }) {
  return (
    <Layout>
      <NextSeo config={{ title: 'Dashboard' }} />
      <Container>
        <h1 className="text-center">
          Hey, {authUser.firstName}! You are logged in.
        </h1>
        <p className="text-center">
          <Button color="primary" onClick={retrieveCurrentUser}>
            Retrieve User
          </Button>
        </p>
      </Container>
    </Layout>
  )
}

export default requireAuth(
  connect(
    null,
    mapDispatch
  )(DashboardPage)
)
