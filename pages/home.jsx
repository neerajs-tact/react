import React from 'react'
import NextSeo from 'next-seo'
import Layout from '../components/Layout'

function Home() {
  return (
    <Layout>
      <NextSeo
        // We customize `titleTemplate` here so that the app name is not appended
        config={{
          titleTemplate: '%s',
          title: 'Starter | Catchy Catchphrase Here',
        }}
      />
      <h1 className="text-center">Hello, startup!</h1>
    </Layout>
  )
}

export default Home
