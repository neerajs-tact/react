import React from 'react'
import NextSeo from 'next-seo'
import { Container } from 'reactstrap'
import ForgotPassword from '../components/ForgotPassword'
import Layout from '../components/Layout'

function ForgotPasswordPage() {
  return (
    <Layout bodyStyle="center-xy">
      <NextSeo config={{ title: 'Forgot Password' }} />
      <Container fluid>
        <div className="w-form-narrow mw-100 mx-auto">
          <h1 className="text-center h3">Forgot Password</h1>
          <hr className="border-top-dashed my-30" />
          <ForgotPassword />
        </div>
      </Container>
    </Layout>
  )
}

export default ForgotPasswordPage
