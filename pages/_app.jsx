/**
 * Custom Next.js App
 *
 * @see https://nextjs.org/docs#custom-app
 */

import React from 'react'
import Head from 'next/head'
import { NextAuth } from 'next-auth/client'
import withRedux from 'next-redux-wrapper'
import NextSeo from 'next-seo'
import App, { Container as NextContainer } from 'next/app'
import { Provider as ReduxProvider } from 'react-redux'
import { Favicon, GTMScript, WebFonts } from '../components/head'
import { AuthUserProvider } from '../contexts/AuthUserContext'
import makeSEO from '../lib/seo'
import makeStore from '../lib/store'
// Global CSS from SCSS (compiles to style.css in _document)
import '../styles/globals.scss'

class CustomApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    ctx.session = await NextAuth.init({ req: ctx.req })

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return {
      pageProps,
      seo: makeSEO(ctx.req),
      session: ctx.session,
    }
  }

  componentDidMount() {
    // picturefill for <picture> and srcset support in older browsers
    // eslint-disable-next-line global-require
    if (process.browser) require('picturefill')
  }

  render() {
    const { Component, pageProps, seo, session, store } = this.props

    return (
      <NextContainer>
        {
          // to keep <Head/> items de-duped we should use next/head in _app.jsx
          // @see https://github.com/zeit/next.js/issues/6919
        }
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, minimal-ui"
          />
          <WebFonts />
          <Favicon />
          <GTMScript />
        </Head>
        <ReduxProvider store={store}>
          <AuthUserProvider session={session}>
            <NextSeo config={seo} />
            <Component {...pageProps} />
          </AuthUserProvider>
        </ReduxProvider>
      </NextContainer>
    )
  }
}

export default withRedux(makeStore)(CustomApp)
