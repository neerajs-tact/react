import React, { Component } from 'react'
import { objectOf, string } from 'prop-types'
import { NextAuth } from 'next-auth/client'
import {
  Router,
  UNAUTHORIZED_REDIRECT_ROUTE,
  USER_HOME_ROUTE,
} from '../../routes'
import LayoutLoading from '../../components/LayoutLoading'

export default class extends Component {
  static async getInitialProps({ query, req }) {
    return {
      query,
      session: await NextAuth.init({ force: true, req }),
    }
  }

  static propTypes = {
    query: objectOf(string).isRequired,
  }

  async componentDidMount() {
    // Get latest session data after rendering on client then redirect.
    // This ensures client state is always updated after signing in or out.
    const session = await NextAuth.init({ force: true })
    const { query } = this.props
    if (session.user) {
      Router.pushRoute(USER_HOME_ROUTE)
    } else {
      Router.pushRoute(UNAUTHORIZED_REDIRECT_ROUTE, query)
    }
  }

  render() {
    return <LayoutLoading />
  }
}
