import NextSeo from 'next-seo'
import { shape, string } from 'prop-types'
import React from 'react'
import { Alert, Container } from 'reactstrap'
import { forwardAuth } from '../../lib/auth'
import AcceptInvitation from '../../components/AcceptInvitation'
import Layout from '../../components/Layout'

AcceptInvitePage.getInitialProps = ({ query }) => ({
  query,
})

AcceptInvitePage.propTypes = {
  query: shape({
    token: string,
  }).isRequired,
}

function AcceptInvitePage({ query: { token } }) {
  return (
    <Layout>
      <NextSeo config={{ title: 'Accept Invitation' }} />
      <Container fluid>
        <div className="w-form-narrow mw-100 mx-auto">
          <h1 className="text-center h3 mb-20">Create Password</h1>
          <p className="text-center">
            Accept your invitation by creating a password to complete your
            account.
          </p>
          <hr className="border-top-dashed my-30" />
          {token && <AcceptInvitation invitationToken={token} />}
          {!token && (
            <Alert color="danger">
              <strong>Missing invitation token.</strong>
              <p className="mb-0">
                Please click the link from your invite email to finish your
                invitation.
              </p>
            </Alert>
          )}
        </div>
      </Container>
    </Layout>
  )
}

export default forwardAuth(AcceptInvitePage)
