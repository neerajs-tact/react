import React from 'react'
import { string } from 'prop-types'
import Layout from '../../components/Layout'
import { Link } from '../../routes'

AuthErrorPage.propTypes = {
  action: string.isRequired,
  type: string.isRequired,
}

AuthErrorPage.getInitialProps = async function getInitialProps({ query }) {
  return {
    action: query.action || null,
    type: query.type || null,
    service: query.service || null,
  }
}

function AuthErrorPage({ action, type }) {
  return (
    <Layout bodyStyle="center-xy">
      <section className="w-form mw-100 px-hg py-40">
        <h1 className="text-danger mt-0 mb-20">Error</h1>
        <ErrorMsg action={action} type={type} />
      </section>
    </Layout>
  )
}

function ErrorMsg(action, type) {
  if (action === 'signin' && type === 'oauth') return <ErrorMsgOauth />
  if (action === 'signin' && type === 'token-invalid') return <ErrorMsgToken />
  return <ErrorMsgDefault />
}

function ErrorMsgOauth() {
  return (
    <>
      <h2 className="mb-20">Unable to log in.</h2>
      <p className="lead">
        An account associated with your email address already exists.
      </p>
      <p>
        <small>
          It looks like you might have already signed up using another service
          to sign in.
        </small>
      </p>
      <p>
        <small>
          If you have previously signed up using another service you must link
          accounts before you can use a different service to sign in.
        </small>
      </p>
      <p>
        <small>
          This is to prevent people from signing up to another service using
          your email address to try and access your account.
        </small>
      </p>
      <p>
        <small>
          First sign in using your email address then link your account to the
          service you want to use to sign in with in future. You only need to do
          this once.
        </small>
      </p>
      <hr className="border-top-dashed" />
      <p>
        <Link route="login">
          <a>Log in with email or another service.</a>
        </Link>
      </p>
    </>
  )
}

function ErrorMsgToken() {
  return (
    <>
      <h2 className="mb-20">Link not valid.</h2>
      <p className="lead">This log in link is no longer valid.</p>
      <hr className="border-top-dashed" />
      <p>
        <Link route="login">
          <a>Get a new log in link.</a>
        </Link>
      </p>
    </>
  )
}

function ErrorMsgDefault() {
  return (
    <>
      <h2 className="mb-20">Unable to log in.</h2>
      <p className="lead">An error occurred while trying to log in.</p>
      <hr className="border-top-dashed" />
      <p>
        <Link route="login">
          <a>Try to log in again.</a>
        </Link>
      </p>
    </>
  )
}

export default AuthErrorPage
