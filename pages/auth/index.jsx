import NextSeo from 'next-seo'
import React from 'react'
import { Container } from 'reactstrap'

// Containers
import Login from '../../components/Login'

// Components
import Layout from '../../components/Layout'
import { forwardAuth } from '../../lib/auth'
import { Link } from '../../routes'

import UrlQueryAlert from '../../components/UrlQueryAlert'
import LogoLg from '../../components/logos/LogoLg'

function LogInPage() {
  return (
    <Layout bodyStyle="center-xy" hideHeader>
      <NextSeo config={{ title: 'Log In' }} />
      <Container fluid>
        <h1 className="sr-only">Log In</h1>
        <Link route="home">
          <a className="mw-100 w-logo mx-auto d-block mb-40 px-60 px-sm-0">
            <LogoLg className="w-100 h-auto" />
          </a>
        </Link>
        <div className="w-form-narrow mw-100 mx-auto">
          <UrlQueryAlert className="text-center" />
          <Login />
          <hr className="border-top-dashed mt-40 mb-30" />
          <Link route="forgot-password">
            <a className="uaTrack-link-forgot-password text-body d-inline-block px-10">
              Forgot password?
            </a>
          </Link>
          <hr className="border-top-dashed my-30" />
          <span className="px-10 d-inline-block">
            Don&apos;t have an account?{' '}
            <Link route="register">
              <a className="uaTrack-link-register text-body text-nowrap">
                Sign up free!
              </a>
            </Link>
          </span>
        </div>
      </Container>
    </Layout>
  )
}

export default forwardAuth(LogInPage)
