import NextSeo from 'next-seo'
import React from 'react'
import { Container } from 'reactstrap'
import { forwardAuth } from '../../lib/auth'

// Containers
import Register from '../../components/Register'

// Components
import Layout from '../../components/Layout'
import UrlQueryAlert from '../../components/UrlQueryAlert'
import { Link } from '../../routes'
import LogoLg from '../../components/logos/LogoLg'

function RegisterPage() {
  return (
    <Layout bodyStyle="center-xy" hideHeader>
      <NextSeo config={{ title: 'Register' }} />
      <Container fluid>
        <h1 className="sr-only">Register</h1>
        <Link route="home">
          <a className="mw-100 w-logo mx-auto d-block mb-40 px-60 px-sm-0">
            <LogoLg className="w-100 h-auto" />
          </a>
        </Link>
        <div className="w-form-narrow mw-100 mx-auto">
          <UrlQueryAlert className="text-center" />
          <Register />
          <hr className="border-top-dashed mt-40 mb-30" />
          <span className="px-10 d-inline-block">
            Already have an account?{' '}
            <Link route="login">
              <a className="uaTrack-link-login text-body text-nowrap">
                Log in here!
              </a>
            </Link>
          </span>
        </div>
      </Container>
    </Layout>
  )
}

export default forwardAuth(RegisterPage)
