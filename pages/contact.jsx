import NextSeo from 'next-seo'
import React from 'react'
import { Container } from 'reactstrap'
import Contact from '../components/Contact'
import Layout from '../components/Layout'

function ContactPage() {
  return (
    <Layout bodyStyle="center-xy">
      <NextSeo config={{ title: 'Contact' }} />
      <Container fluid>
        <div className="w-form mw-100 mx-auto">
          <h1 className="text-center">Contact</h1>
          <hr className="border-top-dashed my-30" />
          <Contact />
        </div>
      </Container>
    </Layout>
  )
}

export default ContactPage
