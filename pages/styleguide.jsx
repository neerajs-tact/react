import NextSeo from 'next-seo'
import { node } from 'prop-types'
import React from 'react'

// components
import {
  Badge,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Row,
} from 'reactstrap'
import {
  Alerts,
  Buttons,
  FormElements,
  Typography,
  VerticalDividers,
} from '../components/styleguide'
import Layout from '../components/Layout'

function StyleGuidePage() {
  return (
    <Layout>
      <NextSeo config={{ title: 'Styleguide' }} />
      <Container>
        <Row>
          <Col lg={7}>
            <StyleCard title="Typography">
              <Typography />
            </StyleCard>
          </Col>
          <Col lg={5}>
            <StyleCard title="Vertical Dividers via &lt;hr/&gt;">
              <VerticalDividers />
            </StyleCard>
          </Col>
          <Col lg={6}>
            <StyleCard title="Form Elements (Reactstrap + Final Form)">
              <div className="w-form mw-100">
                <FormElements />
              </div>
            </StyleCard>
          </Col>
          <Col lg={6}>
            <StyleCard title="Form Group Elements (Reactstrap + Final Form)">
              <div className="w-form mw-100">
                <FormElements isFFGroup />
              </div>
            </StyleCard>
          </Col>
          <Col lg={6}>
            <StyleCard title="Alerts">
              <Alerts />
            </StyleCard>
          </Col>
          <Col xs={12}>
            <StyleCard title="Buttons">
              <Buttons />
            </StyleCard>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

StyleCard.propTypes = {
  title: node.isRequired,
  children: node.isRequired,
}

function StyleCard({ title, children }) {
  return (
    <Card className="mb-50">
      <CardBody>
        <CardTitle>
          {title} <BreakpointBadge />
        </CardTitle>
        <hr className="mb-40" />
        {children}
      </CardBody>
    </Card>
  )
}

function BreakpointBadge() {
  return (
    <span className="ml-10">
      <Badge className="d-inline-block d-sm-none" color="dark">
        xs
      </Badge>
      <Badge className="d-none d-sm-inline-block d-md-none" color="dark">
        sm
      </Badge>
      <Badge className="d-none d-md-inline-block d-lg-none" color="dark">
        md
      </Badge>
      <Badge className="d-none d-lg-inline-block d-xl-none" color="dark">
        lg
      </Badge>
      <Badge className="d-none d-xl-inline-block" color="dark">
        xl
      </Badge>
    </span>
  )
}

export default StyleGuidePage
