import { node, object, shape } from 'prop-types'
import React, { createContext } from 'react'
import { getAuthUser } from '../lib/auth'

const AuthUserContext = createContext(null)

AuthUserProvider.propTypes = {
  children: node.isRequired,
  session: shape({
    user: object,
  }),
}

AuthUserProvider.defaultProps = {
  session: null,
}

export function AuthUserProvider({ children, session }) {
  const authUser = getAuthUser(session)
  return (
    <AuthUserContext.Provider value={authUser}>
      {children}
    </AuthUserContext.Provider>
  )
}

export default AuthUserContext
