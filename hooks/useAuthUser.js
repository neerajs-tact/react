import { useContext } from 'react'
import { AuthUserContext } from '../contexts'

/**
 * Return the auth user provided by AuthUserContext. Only updated at Sign In/Out.
 * If you are working with data that might change between those moments then
 * use the Redux user module instead.
 * @type {object|null}
 */
const useAuthUser = () => useContext(AuthUserContext)

export default useAuthUser
