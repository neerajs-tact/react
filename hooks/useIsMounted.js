import { useCallback, useEffect, useRef } from 'react'

/**
 * Use this hook to determine if a component is still mounted or not. This is
 * probably an anti-pattern but can be useful when trying to use promises inside
 * a hook to not risk setting state on an unmounted component.
 *
 * @returns {function(): boolean} A function that tells if a component is mounted or not
 *
 * @see https://github.com/adambrgmn/hooks/blob/master/src/use-is-mounted/index.ts
 *
 * @example
 *   function Example({ retrieveSomething }) {
 *     const isMounted = useIsMounted()
 *     const [something, setSomething] = useState(null)
 *
 *     useEffect(() => {
 *       try {
 *         const newSomething = await retrieveSomething()
 *         if (isMounted()) setSomething(newSomething);
 *       } catch (error) {
 *         console.error(error)
 *       }
 *     }, [isMounted, retrieveSomething])
 *
 *     return <p>Example: {something}</p>
 *   }
 */
function useIsMounted() {
  const isMounted = useRef(false)

  const callback = useCallback(() => isMounted.current, [])

  useEffect(() => {
    isMounted.current = true
    return () => {
      isMounted.current = false
    }
  }, [])

  return callback
}

export default useIsMounted
