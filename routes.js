// eslint-disable-next-line no-multi-assign
const routes = (module.exports = require('next-routes')())

// prettier-ignore
routes
  // route name,            url pattern,                page inside ./pages
  .add('home',              '/',                        'home')
  .add('login',             '/login',                   'auth/index')
  .add('register',          '/register',                'auth/register')
  .add('accept-invitation', '/accept-invitation',       'auth/accept-invitation')
  .add('auth-callback',     '/auth/callback',           'auth/callback')
  .add('auth-error',        '/auth/error',              'auth/error')
  .add('forgot-password',   '/forgot-password',         'forgot-password')
  .add('reset-password',    '/reset-password',          'reset-password')
  .add('contact',           '/contact',                 'contact')
  // Logged in
  .add('dashboard',         '/dashboard',               'dashboard/index')
  .add('your-account',      '/dashboard/your-account',  'dashboard/your-account')

if (process.env.NODE_ENV !== 'production' || process.env.SHOW_STYLEGUIDE) {
  // prettier-ignore
  routes
    .add('styleguide',      '/_styleguide',             'styleguide')
}

routes.USER_HOME_ROUTE = 'dashboard'
routes.UNAUTHORIZED_REDIRECT_ROUTE = 'login'
