/* eslint-disable no-console */
const dev = process.env.NODE_ENV !== 'production'
if (dev) {
  // Load environment variables from .env
  // eslint-disable-next-line global-require,import/no-extraneous-dependencies
  require('dotenv').config()
}

const compression = require('compression')
const express = require('express')
const next = require('next')
const nextAuth = require('next-auth')
const nextAuthConfig = require('./next-auth.config')
const routes = require('./routes')

const port = process.env.PORT || 3000

// Initialize Next.js
const nextApp = next({ dir: '.', dev })
const handler = routes.getRequestHandler(nextApp)

/**
 * Middleware to force HTTPS on Heroku
 * @see https://help.heroku.com/J2R1S4T8/can-heroku-force-an-application-to-use-ssl-tls
 */
const forceHTTPS = (req, res, next) => {
  if (
    Object.prototype.hasOwnProperty.call(req.headers, 'x-forwarded-proto') &&
    req.headers['x-forwarded-proto'] !== 'https'
  ) {
    res.redirect(`https://${req.hostname}${req.originalUrl}`)
  } else {
    next()
  }
}

// Initialize Express server
const expressApp = express()

if (!dev) {
  // Force HTTPS when run in production
  expressApp.use(forceHTTPS)
  // Enable gzip compression
  // See https://github.com/zeit/next.js/wiki/Getting-ready-for-production
  expressApp.use(compression())
}

// Add NextAuth to Next.js app
nextApp
  .prepare()
  .then(async () => {
    // Load configuration and return config object
    const nextAuthOptions = await nextAuthConfig()
    // Pass our own Express instance to NextAuth - and don't pass a port!
    // See https://github.com/iaincollins/next-auth/wiki/Using-NextAuth-with-Express-and-Custom-Routes
    if (nextAuthOptions.port) delete nextAuthOptions.port
    nextAuthOptions.expressApp = expressApp
    // Initialize NextAuth app
    await nextAuth(nextApp, nextAuthOptions)

    // Use next-routes handler
    expressApp.use(handler)

    expressApp.listen(port, err => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
  .catch(err => {
    console.error(err.stack)
    process.exit(1)
  })
